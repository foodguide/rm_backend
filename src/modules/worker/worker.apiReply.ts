import { Inject, Injectable } from '@nestjs/common'
import { BaseWorker } from './worker.base'
import { ReviewService } from '../review/review.service'
import { Database } from '../database/database.baseProvider'
import { ConfigService } from '../../config/config.service'

@Injectable()
export class ApiReplyWorker extends BaseWorker {

  constructor(
    @Inject('Database') private readonly database: Database,
    private readonly reviewService: ReviewService,
    protected config: ConfigService) {
    super(database, config)
  }

  get queueReference() {
    return 'apiReplyQueue'
  }

  process(data, progress, resolve, reject) {
    console.log(`Processing reply task for review ${data.id}`)
    this.reviewService.sendReplyToService(data.id, data.userId, data.text)
      .then(() => resolve())
      .catch(error => reject(error))
  }
}
