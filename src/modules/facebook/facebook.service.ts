import { Injectable } from '@nestjs/common'
import { FacebookApi } from '../api/facebook/api.facebook'

@Injectable()
export class FacebookService {
  constructor(private readonly _facebookApi: FacebookApi) {}
  get facebookApi() {
    return this._facebookApi
  }

  async sendReply(user, review, representation, text) {
    return this.facebookApi.send(`/${review.externalId}/comments`, { access_token: representation.accessToken , message: text }, 'POST')
  }

}
