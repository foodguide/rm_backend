import { Injectable } from '@nestjs/common'
import { Database } from '../database/database.baseProvider'
import { LocationHelper } from '../helper/helper.locationHelper'
import { ReviewRepository } from '../review/review.repository'

@Injectable()
export class HealthService {
  protected loginCheckQueue: any

  protected constructor(
    protected database: Database,
    private readonly locationHelper: LocationHelper,
    private readonly reviewRepository: ReviewRepository,
  ) {
    this.loginCheckQueue = this.database.firebase.ref('loginCheckQueue/tasks')
  }

  async pushLoginCheckTasks() {
    const platforms = ['tripadvisor', 'yelp', 'opentable']
    platforms.forEach((platform) => {
      this.loginCheckQueue.push({
        platform,
        date: Date(),
      })
    })
  }

  async correctGoogleReviewIds() {
    await this.locationHelper.forAllUsers(async (locations, userId) => {
      await Promise.all(locations.map(async (location) => {
        await this.correctGoogleReviewIdsForLocation(location, userId)
      }))
    })
  }

  async correctGoogleReviewIdsForLocation(location, userId) {
    const representations = await this.locationHelper.getPlatformRepresentationsForLocation(location)
    const googleRepresentation = representations.find(representation => representation.platform === 'google')
    if (!googleRepresentation) return
    const reviews = await this.reviewRepository.findByRepresentationId(googleRepresentation.id)
    await Promise.all(reviews.map(async (review) => {
      await this.changeReviewId(review)
    }))
    console.log(`${reviews.length} Google reviews for location ${location.id} ${location.name} of user ${userId} updated`)
  }

  async changeReviewId(review) {
    await this.reviewRepository.delete(review.id)

    const newReview = { ...review }
    newReview.id = `${newReview.representationId}_${newReview.externalId}`
    await this.reviewRepository.create(newReview)
  }

}
