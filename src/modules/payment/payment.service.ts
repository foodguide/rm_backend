import { Inject, Injectable } from '@nestjs/common'
import { PaymentApiInterface } from './payment.api.interface'
import { SlackApi } from '../api/slack/api.slack'
import { ExternalApiError } from '../api/errors'

@Injectable()
export class PaymentService {
  constructor(@Inject('PaymentApiInterface') private paymentApi: PaymentApiInterface,
              protected readonly slackApi: SlackApi) {
  }

  getNewSubscriptionUrl(customerId: string, planId: string, userEmail?: string) {
    return this.paymentApi.getCheckoutUrlForNewSubscription(customerId, planId, userEmail)
  }

  getExistingSubscriptionUrl(subscriptionId: string) {
    return this.paymentApi.getCheckoutUrlForExistingSubscription(subscriptionId)
  }

  getUpdatePaymentUrl(customerId: string) {
    return this.paymentApi.getUpdatePaymentUrl(customerId)
  }

  getPortalSession(customerId: string) {
    return this.paymentApi.getPortalSession(customerId)
  }

  hasValidSubscriptions(customerId: string) {
    return this.paymentApi.hasValidSubscriptions(customerId)
  }

  hasCancelledSubscriptions(customerId: string) {
    return this.paymentApi.hasCancelledSubscriptions(customerId)
  }

  async upgradeSubscriptionToBasicPlan(subscriptionId: string) {
    return this.paymentApi.updateSubscription(subscriptionId, 'respondo-basic', true, 1)
  }

  async updateSubscription(subscriptionId: string, numberOfLocations: number) {
    /**
     * Basic explanation:
     *
     * The Respondo subscription model is based on the quantity of managed locations.
     *
     * If a location is added, the subscription's quantity is increased immediately
     * and stays at least on this level until the next renewal.
     *
     * If a location is deleted, the subscription's quantity stays on the current level,
     * but on next renewal the quantity will be decreased.
     *
     * On renewal date a new subscription term starts with the following configuration:
     * a) no scheduled changes - the quantity remains the same as in the last subscription term
     * b) scheduled changes - the quantity will be automatically adjusted according to the changes
     */

    try {
      /*
      In the Basic plan up to ONE location is included, therefore the minimum quantity always has to be ONE
      even the user has zero locations in Respondo.
       */
      const newNumberOfLocations = numberOfLocations === 0 ? 1 : numberOfLocations
      const currentSubscription = await this.paymentApi.retrieveSubscription(subscriptionId)
      const currentQuantity = currentSubscription.subscription.plan_quantity
      const scheduledChanges = await this.paymentApi.retrieveScheduledChanges(subscriptionId)
      const planId = currentSubscription.subscription.plan_id

      if (planId.startsWith('start')) {
        await this.removeScheduledChanges(subscriptionId, scheduledChanges)
        // User is in test plan, therefore all changes are scheduled at end of term
        return this.paymentApi.updateSubscription(subscriptionId, 'respondo-basic', true, newNumberOfLocations)
      } else {
        if (newNumberOfLocations === currentQuantity) {
          // Quantity stays on the same level, all scheduled changes have to be removed
          return this.removeScheduledChanges(subscriptionId, scheduledChanges)
        } else if (newNumberOfLocations < currentQuantity) {
          // Quantity has decreased, therefore scheduled changes have to be removed and a the new quantity has to be scheduled for the end of the term
          await this.removeScheduledChanges(subscriptionId, scheduledChanges)
          return this.paymentApi.updateSubscription(subscriptionId, planId, true, newNumberOfLocations)
        } else {
          // Quantity has increased, update subscription immediately
          return this.paymentApi.updateSubscription(subscriptionId, planId, false, newNumberOfLocations)
        }
      }
    } catch (error) {
      const message = `Problem while syncing ChargeBee. SubscriptionId: ${subscriptionId}, locations: ${numberOfLocations}`
      this.slackApi.sendMessageToChannel('review-manager-errors', message, [
        this.slackApi.createErrorAttachement(new ExternalApiError(null, error.message))])
    }
  }

  removeScheduledChanges(subscriptionId: string, scheduledChanges) {
    if (!!scheduledChanges.subscription) {
      return this.paymentApi.removeScheduledChanges(subscriptionId)
    }
    return Promise.resolve()
  }

  getValidSubscriptions(customerId: string) {
    return this.paymentApi.getValidSubscriptions(customerId)
  }

  getHostedPage(hostedPageId: string) {
    return this.paymentApi.getHostedPage(hostedPageId)
  }

  cancelSubscriptions(customerId: string) {
    return this.paymentApi.cancelSubscriptions(customerId)
  }

  getCustomerIdsWithActiveSubscriptions() {
    return this.paymentApi.getCustomerIdsWithActiveSubscriptions()
  }
}
