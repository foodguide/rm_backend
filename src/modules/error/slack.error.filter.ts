import {
  ExceptionFilter,
  Catch,
  HttpException,
  ArgumentsHost,
  HttpStatus,
  Injectable,
  Logger,
} from '@nestjs/common'
import { SlackApi } from '../api/slack/api.slack'

@Injectable()
@Catch()
export class SlackErrorFilter implements ExceptionFilter {

  constructor(
    private readonly slackApi: SlackApi) {
  }

  async catch(error: Error, host: ArgumentsHost) {
    const response = host.switchToHttp().getResponse()
    const status = (error instanceof HttpException) ? error.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR

    const requestUrl = (host.args && host.args[0]) ? host.args[0].url : ''
    const headers = (host.args && host.args[0]) ? host.args[0].headers : ''

    if (process.env.NODE_ENV !== 'development') {
      if (requestUrl.startsWith('/status/authorization')) {
        this.specialLoggingForAuthRequest(requestUrl)
      } else {
        this.slackApi.logErrors([
          this.slackApi.createErrorAttachement(
            {
              status,
              headers,
              name: error.name,
              stacktrace: error.stack,
              message: error.message,
              url: requestUrl,
            },
          )],
        )
      }
    }
    Logger.error(error.message, error.stack)
    return response.status(status).send({
      status,
      message: error.message.message ? error.message.message : 'Internal Server Error',
    })
  }

  /**
   * Failing requests to authorization endpoint should be logged as warning.
   *
   * @param requestUrl
   */
  specialLoggingForAuthRequest(requestUrl) {
    this.slackApi.logCatchedError(`Authorization for: ${requestUrl.split('=').pop()} failed`)
  }
}
