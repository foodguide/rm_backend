import { Injectable } from '@nestjs/common'
import { BaseRepository } from '../database/database.baseRepository'

@Injectable()
export class RepresentationStatisticRepository extends BaseRepository {

  managedCollection = 'representationStatistics'

  findForRepresentationByDate(representationId: string, date: Date) {
    const id = date.getFullYear() + '_' + (date.getUTCMonth() + 1) + '_' + date.getDate() + '_' + representationId
    return this.commitRead(
      this.collection
        .where('id', '==', id)
        .get(),
    )
  }

  async hasStatistics(representationId: string): Promise<Boolean> {
    const snapshot = await this.collection.where('representationId', '==', representationId).limit(1).get()
    return snapshot.docs.length > 0
  }

  findForUserBetweenDates(userId: string, start: Date, end: Date) {
    return this.commitRead(
      this.collection
        .where('userId', '==', userId)
        .where('date', '>=', start)
        .where('date', '<=', end)
        .get(),
    )
  }

  findByRepresentationId(representationId: string) {
    return this.commitRead(
      this.collection.where('representationId', '==', representationId).limit(1).get(),
    )
  }
}
