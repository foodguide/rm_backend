import { emailTemplate } from '../email.template'
import { MessageName } from '../../../message.name'
import { UnsubscribableTransactionalMail } from '../transactional.mail'
import { MessageSubscription } from '../../../message.model'
import { MessageType } from '../../../message.type'
import { classToPlain } from 'class-transformer'
import moment from 'moment'
import 'moment/locale/de'
import { StringHelper } from '../../../../helper/helper.stringHelper'

export class SummaryEmail extends UnsubscribableTransactionalMail {

  constructor(locale: string) {
    super()
    moment.locale(locale)
    this.subscription = new MessageSubscription()
            .setType(MessageType.Email)
            .setName(MessageName.Summary)

    this.setTemplate(emailTemplate.summary, locale)
  }

  setLocationSummaries(locationSummaries: Array<LocationSummary>) {
    this.setTemplateVariable('locationSummaries', classToPlain(locationSummaries))
    return this
  }

  setStartDate(start: Date) {
    this.setTemplateVariable('startDate', moment(start).format('D. MMMM'))
    return this
  }

  setEndDate(end: Date) {
    this.setTemplateVariable('endDate', moment(end).format('D. MMMM'))
    return this
  }

}

abstract class Summary {

  totalReviewAmount: number
  totalReviewAmountDelta: number

  goodReviewAmount: number
  goodReviewAmountDelta: number

  badReviewAmount: number
  badReviewAmountDelta: number

  answeredReviewAmount: number
  answeredReviewAmountDelta: number

  averageRating: number
  averageRatingDelta: number
  averageRatingDeltaSign: string

  unansweredReviewAmount: number

  setTotalReviewAmount(amount: number) {
    this.totalReviewAmount = amount
    if (this.answeredReviewAmount)
      this.unansweredReviewAmount = amount - this.answeredReviewAmount
    return this
  }

  setTotalReviewAmountDelta(delta: number) {
    this.totalReviewAmountDelta = delta
    return this
  }

  setGoodReviewAmount(amount: number) {
    this.goodReviewAmount = amount
    return this
  }

  setGoodReviewAmountDelta(delta: number) {
    this.goodReviewAmountDelta = delta
    return this
  }

  setBadReviewAmount(amount: number) {
    this.badReviewAmount = amount
    return this
  }

  setBadReviewAmountDelta(delta: number) {
    this.badReviewAmountDelta = delta
    return this
  }

  setAnsweredReviewAmount(amount: number) {
    this.answeredReviewAmount = amount
    if (this.totalReviewAmount)
      this.unansweredReviewAmount = this.totalReviewAmount - amount
    return this
  }

  setAnsweredReviewAmountDelta(delta: number) {
    this.answeredReviewAmountDelta = delta
    return this
  }

  setAverageRating(rating: number) {
    this.averageRating = rating
    return this
  }

  setAverageRatingDelta(delta: number) {
    this.averageRatingDelta = delta
    this.averageRatingDeltaSign = delta < 0 ? 'negative' : 'positive'
    return this
  }

}

export class LocationSummary extends Summary {
  name: string
  goToReviewsUrl: string
  representationSummaries: Array<RepresentationSummary> = []

  setName(name: string) {
    this.name = name
    return this
  }

  setRepresentationSummaries(representationSummaries: Array<RepresentationSummary>) {
    this.representationSummaries = representationSummaries
    return this
  }

  setGoToReviewsUrl(locationId: string, baseUrl: string) {
    this.goToReviewsUrl = `${baseUrl}/#/app/reviews/${locationId}`
    return this
  }

}

export class RepresentationSummary extends Summary {
  platform: string

  setPlatform(platform: string) {
    this.platform = StringHelper.capitalize(platform)
    return this
  }
}
