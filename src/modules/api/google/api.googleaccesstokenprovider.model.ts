import { Credential, User } from '../../user/user.model'
import { MissingRequiredPropertyException } from '../../database/database.Errors'

export class GoogleAccessTokenProvider {
  user?: User
  credentialId?: string
  credential?: Credential

  constructor(user?: User, credentialId?: string, credential?: Credential) {
    if (!!credential) {
      this.credential = credential
    } else if (!!user) {
      if (!credentialId) {
        throw new MissingRequiredPropertyException('credentialId')
      }
      this.user = user
      this.credentialId = credentialId
    } else {
      throw new MissingRequiredPropertyException('user or credentialId or accessToken')
    }
  }
}
