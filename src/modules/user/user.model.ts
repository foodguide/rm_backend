import { BaseModel } from '../database/database.baseModel'
import { Type } from 'class-transformer'
import { IsNotEmpty, IsOptional, IsString } from 'class-validator'

export class User extends BaseModel {
  @Type(() => Credential)
    credentials?: Credential[]
  subscriptions?: any[]
  locale?: string
}

export class Credential {
  token_type?: string | null
  refresh_token?: string | null
  platform: string
  expiry_date?: number
  scope?: string | null
  accessToken?: string | null
  access_token?: string | null
  id_token?: string | null
  id?: string | null
  account?: object | null
}

export class GoogleRequestDto {
  @IsString()
  @IsNotEmpty()
  userId: string

  @IsString()
  @IsNotEmpty()
  credentialId: string
}

export class NextPageTokenDto {
  @IsOptional()
    @IsString()
    nextPageToken: string
}
