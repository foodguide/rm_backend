import { TransactionalMail } from '../transactional.mail'
import { MessageSubscription } from '../../../message.model'
import { MessageType } from '../../../message.type'
import { MessageName } from '../../../message.name'
import { emailTemplate } from '../email.template'


export class RetentionMail extends TransactionalMail {
  constructor(locale: string) {
    super()
    this.subscription = new MessageSubscription()
      .setType(MessageType.Email)
      .setName(MessageName.Reminder)

    this.setTemplate(emailTemplate.retention, locale)
  }

  setNewReviewsCount(count: string) {
    this.setTemplateVariable('newReviewsCount', count)
    return this
  }
}
