import { forwardRef, Module } from '@nestjs/common'
import { ApiModule } from '../api/api.module'
import { GmbController } from './gmb.controller'
import { HelperModule } from '../helper/helper.module'
import { GmbService } from './gmb.service'
import { ReviewModule } from '../review/review.module'

@Module({
  imports: [
    ApiModule,
    HelperModule,
    forwardRef(() => ReviewModule)],
  providers: [GmbService],
  controllers: [GmbController],
  exports: [GmbService],
})
export class GmbModule {}
