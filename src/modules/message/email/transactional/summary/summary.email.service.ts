import { Injectable, Logger } from '@nestjs/common'
import { RepresentationStatisticRepository } from '../../../../representationStatistic/representationStatistic.repository'
import { LocationSummary, RepresentationSummary, SummaryEmail } from './summary.email'
import { User } from '../../../../user/user.model'
import { LocationRepository } from '../../../../location/location.repository'
import { ConfigService } from '../../../../../config/config.service'
import { EmailRecipient } from '../../email.model'
import moment from 'moment'
import { UserRepository } from '../../../../user/user.repository'
import { MessagePublisher } from '../../../../publisher/publisher.message'
import { LocationHelper } from '../../../../helper/helper.locationHelper'
import { SlackApi } from '../../../../api/slack/api.slack'
import { CurrentUserHelper } from '../../../../helper/helper.currentUserHelper'

@Injectable()
export class SummaryEmailService {
  constructor(
        private readonly config: ConfigService,
        private readonly locationRepository: LocationRepository,
        private readonly userRepository: UserRepository,
        private readonly representationStatisticRepository: RepresentationStatisticRepository,
        private readonly messagePublisher: MessagePublisher,
        private readonly locationHelper: LocationHelper,
        private readonly currentUserHelper: CurrentUserHelper,
        private readonly slackApi: SlackApi) {
  }

  public async sendSummaryMails() {
    const users = await this.locationHelper.findAllUsersWithActiveSubscriptions()
    const end = moment().add(-1, 'days').endOf('day').toDate()
    const start = moment().add(-8, 'days').startOf('day').toDate()
    for (const user of users) {
      const mail = await this.createSummaryEmail(user, start, end)
      if (mail && user.email) {
        const pipedriveBcc = this.currentUserHelper.createPipedriveBcc(user.pipedriveId)
        if (!!pipedriveBcc) mail.setBccRecipients([new EmailRecipient().setEmail(pipedriveBcc)])
        mail.setRecipients([new EmailRecipient().setEmail(user.email)])
        this.messagePublisher.push(mail, user)
      } else {
        this.slackApi.sendMessageToChannel(
                  'review-manager-errors',
                  `No summary mail has been created for user ${user.id} with mail: ${user.email}`,
                  null,
                )
      }
    }
  }

  public async createSummaryEmail(user: User, start: Date, end: Date) {
    const locations = await this.locationRepository.findByUserId(user.id)
    const locationSummaries: Array<LocationSummary> = []

    if (!locations || locations.length === 0) return

    for (const location of locations) {

      const representations = await this.locationRepository.getRepresentationsByLocationId(location.id)

      if (!representations || representations.length === 0) continue

      const representationSummaries: Array<RepresentationSummary> = []
      for (const representation of representations) {

        if (representation.platform === 'instagram') continue

        const startStatistic = (await this.representationStatisticRepository.findForRepresentationByDate(representation.id, start))[0]
        const endStatistic = (await this.representationStatisticRepository.findForRepresentationByDate(representation.id, end))[0]

        if (!startStatistic || !endStatistic) continue

        const representationSummary = SummaryEmailService.createRepresentationSummary(representation, startStatistic, endStatistic)

        if (
                  !representationSummary
                  || representationSummary.totalReviewAmount === 0
                  || isNaN(representationSummary.totalReviewAmount)
                )
          continue

        representationSummaries.push(representationSummary)
      }
      if (representationSummaries.length === 0) continue

      const locationSummary = SummaryEmailService.createLocationSummary(location, representationSummaries, this.config.get('BASE_URL'))

      if (!locationSummary || locationSummary.totalReviewAmount === 0 || isNaN(locationSummary.totalReviewAmount)) continue

      locationSummaries.push(locationSummary)
    }
    if (locationSummaries.length === 0) return

    return new SummaryEmail(user.locale)
            .setLocationSummaries(locationSummaries)
            .setStartDate(start)
            .setEndDate(end)
            .setUnsubscribeUrlByUserId(user.id, this.config.get('BASE_URL'))
  }

  public static createLocationSummary(location, representationSummaries: Array<RepresentationSummary>, baseUrl: string) {
    let goodReviewAmountSum = 0
    let goodReviewAmountDeltaSum = 0

    let badReviewAmountSum = 0
    let badReviewAmountDeltaSum = 0

    let averageRatingSum = 0
    let averageRatingDeltaSum = 0

    let totalReviewsAmountSum = 0
    let totalReviewsAmountDeltaSum = 0

    let answeredReviewsAmountSum = 0
    let answeredReviewsAmountDeltaSum = 0

    for (const representationSummary of representationSummaries) {

      goodReviewAmountSum += representationSummary.goodReviewAmount
      goodReviewAmountDeltaSum += representationSummary.goodReviewAmountDelta

      badReviewAmountSum += representationSummary.badReviewAmount
      badReviewAmountDeltaSum += representationSummary.badReviewAmountDelta

      averageRatingSum += representationSummary.averageRating * representationSummary.totalReviewAmount
      averageRatingDeltaSum += representationSummary.averageRatingDelta * representationSummary.totalReviewAmount

      totalReviewsAmountSum += representationSummary.totalReviewAmount
      totalReviewsAmountDeltaSum += representationSummary.totalReviewAmountDelta

      answeredReviewsAmountSum += representationSummary.answeredReviewAmount
      answeredReviewsAmountDeltaSum += representationSummary.answeredReviewAmountDelta

    }
    return new LocationSummary().setRepresentationSummaries(representationSummaries)
            .setName(location.name)
            .setGoToReviewsUrl(location.id, baseUrl)
            .setGoodReviewAmount(goodReviewAmountSum)
            .setGoodReviewAmountDelta(goodReviewAmountDeltaSum)
            .setBadReviewAmount(badReviewAmountSum)
            .setBadReviewAmountDelta(badReviewAmountDeltaSum)
            .setAverageRating(Number((averageRatingSum / totalReviewsAmountSum).toFixed(2)))
            .setAverageRatingDelta(Number((averageRatingDeltaSum / totalReviewsAmountSum).toFixed(2)))
            .setTotalReviewAmount(totalReviewsAmountSum)
            .setTotalReviewAmountDelta(totalReviewsAmountDeltaSum)
            .setAnsweredReviewAmount(answeredReviewsAmountSum)
            .setAnsweredReviewAmountDelta(answeredReviewsAmountDeltaSum)

  }

  public static createRepresentationSummary(representation, startStatistic, endStatistic): RepresentationSummary {
    try {
      const goodReviewAmount = this.countGoodReviews(endStatistic)
      const goodReviewAmountDelta = goodReviewAmount - this.countGoodReviews(startStatistic)

      const badReviewAmount = this.countBadReviews(endStatistic)
      const badReviewAmountDelta = badReviewAmount - this.countBadReviews(startStatistic)

      return new RepresentationSummary()
              .setPlatform(representation.platform)
              .setAnsweredReviewAmount(endStatistic.answered)
              .setAnsweredReviewAmountDelta(endStatistic.answered - startStatistic.answered)
              .setTotalReviewAmount(endStatistic.totalReviews)
              .setTotalReviewAmountDelta(endStatistic.totalReviews - startStatistic.totalReviews)
              .setGoodReviewAmount(goodReviewAmount)
              .setGoodReviewAmountDelta(goodReviewAmountDelta)
              .setBadReviewAmount(badReviewAmount)
              .setBadReviewAmountDelta(badReviewAmountDelta)
              .setAverageRating(Number(endStatistic.avgRating.toFixed(2)))
              .setAverageRatingDelta(Number((endStatistic.avgRating - startStatistic.avgRating).toFixed(2)))
    } catch (e) {
      Logger.error(e.message, e.stack)
      return null
    }
  }

  private static countBadReviews(statistic) {
    return statistic.representationId.startsWith('fb_') ?
          statistic.recommendationNegative : statistic.ratingThree + statistic.ratingTwo + statistic.ratingOne
  }

  private static countGoodReviews(statistic) {
    return statistic.representationId.startsWith('fb_') ? statistic.recommendationPositive : statistic.ratingFive + statistic.ratingFour
  }
}
