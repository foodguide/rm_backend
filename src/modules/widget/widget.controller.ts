import { Controller, Get, Query, ValidationPipe } from '@nestjs/common'
import { WidgetRepository } from './widget.repository'
import { WidgetDto } from './widget.model'
import { LocationStatisticRepository } from '../locationStatistic/locationStatistic.repository'

@Controller('widget')
export class WidgetController {

  constructor(
    private readonly widgetRepository: WidgetRepository,
    private readonly locationStatisticRepository: LocationStatisticRepository) {
  }

  @Get('data')
  async getWidgetData(@Query(new ValidationPipe()) dto: WidgetDto) {
    const widget = await this.widgetRepository.findDoc(dto.widgetId)
    const locationStatistic = await this.locationStatisticRepository.findByLocationId(widget.locationId)
    if (!!widget && locationStatistic.length >= 1) {
      const widgetData = locationStatistic[0]
      widgetData.position = widget.position
      widgetData.color = widget.color
      return widgetData
    } else {
      throw Error(`Can't find data for widgetId: ${dto.widgetId}`)
    }
  }
}
