import { IsDateString, IsNotEmpty } from 'class-validator'

export class NewStatisticsDto {
  @IsNotEmpty()
  @IsDateString()
  readonly from: string
}
