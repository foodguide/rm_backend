import { Module } from '@nestjs/common'
import { ReviewRepository } from './review.repository'
import { Review } from './review.model'
import { ReviewController } from './review.controller'
import { ReviewService } from './review.service'
import { LocationModule } from '../location/location.module'
import { GmbModule } from '../googlemybusiness/gmb.module'
import { FacebookModule } from '../facebook/facebook.module'

/**
 * The module for everything related to reviews.
 */
@Module({
  imports: [LocationModule, GmbModule, FacebookModule],
  providers: [Review, ReviewRepository, ReviewService],
  exports: [ReviewRepository, ReviewService],
  controllers: [ReviewController],
})
export class ReviewModule{}
