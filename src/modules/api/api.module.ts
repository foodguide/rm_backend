import { HttpModule, Module } from '@nestjs/common'
import { FacebookApi } from './facebook/api.facebook'
import { SlackApi } from './slack/api.slack'
import { GoogleAuthApi } from './google/api.googleauth'
import { GoogleMyBusinessApi } from './google/api.googlemybusiness'
import { LocationModule } from '../location/location.module'
import { ChargebeeApi } from './chargebee/api.chargebee'
import { MailjetApi } from './mailjet/api.mailjet'
import { StatusApi } from './status/api.status'

const paymentApi = { provide: 'PaymentApiInterface', useClass: ChargebeeApi }
const statusApi = { provide: 'StatusApiInterface', useClass: StatusApi }

@Module({
  imports: [
    HttpModule,
    LocationModule,
  ],
  providers: [
    MailjetApi,
    FacebookApi,
    GoogleAuthApi,
    GoogleMyBusinessApi,
    SlackApi,
    paymentApi,
    statusApi,
  ],
  exports: [
    MailjetApi,
    FacebookApi,
    GoogleAuthApi,
    GoogleMyBusinessApi,
    SlackApi,
    paymentApi,
    statusApi,
  ],
})

export class ApiModule {
}
