export class MissingRequiredPropertyException extends Error {
  constructor(propertyName : string) {
    super()
    this.message = 'Error while saving entity, missing required property ' + propertyName
    this.name = 'MissingRequiredPropertyException'
    this.stack = (<any> new Error()).stack
  }
}
