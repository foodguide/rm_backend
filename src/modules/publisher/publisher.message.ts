import { BasePublisher } from './publisher.base'
import { classToPlain } from 'class-transformer'
import { MessageQueueItem } from '../message/message.interface'
import { MessageValidator, NotSubscribedError } from '../message/message.validator'
import { User } from '../user/user.model'

export class MessagePublisher extends BasePublisher {

  get queueReference() {
    return 'messageQueue/tasks'
  }

  push(message: MessageQueueItem, user: User) {
    try {
      if (MessageValidator.messageIsValid(message)) {
        if (!MessageValidator.userHasSubscription(user, message.subscription))
          throw new NotSubscribedError(user, message.subscription)

        const plainMessageItem = classToPlain(message)
        super.push(plainMessageItem)
      }
    } catch (e) {
      console.log(e.message)
    }
    
  }
}
