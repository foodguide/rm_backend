import { Body, Controller, Get, Put, Query, UseGuards, ValidationPipe } from '@nestjs/common'
import { SyncFacebookService } from './facebook/sync.facebook.service'
import { UserRepository } from '../user/user.repository'
import { plainToClass } from 'class-transformer'
import { LocationRepository } from '../location/location.repository'
import { Location } from '../location/location.model'
import { SyncGoogleService } from './google/sync.google.service'
import { SyncCrawlerService } from './crawler/sync.crawler.service'
import { AuthGuard } from '../auth/auth.guard'
import { BearerToken } from '../auth/auth.bearerdecorator'
import { CurrentUserHelper } from '../helper/helper.currentUserHelper'
import { DateHelper } from '../helper/helper.dateHelper'
import {
    SyncLocationCrawlerDto,
    SyncRepresentationCrawlerDto,
    SyncLocationDto,
    PlatformsDto,
    PlatformNotSupportedException,
    SyncAdminRepresentationCrawlerDto,
    SyncPlatformDto,
} from './sync.model'
import { SyncBaseService } from './sync.base.service'
import { SyncInstagramService } from './instagram/sync.instagram.service'
import { PaymentService } from '../payment/payment.service'
import { AdminGuard } from '../auth/admin.guard'
import { DirectUserRepository } from '../direct/directUser.repository'
import { TaskPriority } from './crawler/sync.crawler.service'

@Controller('sync')
export class SyncController {

  constructor(
        private readonly userRepository: UserRepository,
        private readonly directUserRepository: DirectUserRepository,
        private readonly locationRepository: LocationRepository,
        private readonly syncFacebookService: SyncFacebookService,
        private readonly syncGoogleService: SyncGoogleService,
        private readonly syncCrawlerService: SyncCrawlerService,
        private readonly syncInstagramService: SyncInstagramService,
        private readonly currentUserHelper: CurrentUserHelper,
        private readonly paymentService: PaymentService,
    ) {
  }

  @Get('api')
    async syncUserApiPlatforms(@Query(new ValidationPipe()) dto: SyncPlatformDto) {
    const startTime = new Date()
    await this.syncApiReviewsAndStatistics(dto.platform)
    return {
      message: 'Update finished',
      elapsedTime: `${DateHelper.getDifference(startTime)}s`,
    }
  }

  @Put('api/user')
  @UseGuards(AuthGuard)
  async syncApisForUser(@BearerToken() token: string, @Body(new ValidationPipe()) dto: PlatformsDto) {
    console.log(`start api user sync: ${Date().toLocaleString()}`)
    return this.syncLocationsByToken(token, async (locations, userId) => {
      console.log('updating api reviews for user ' + userId)
      return await Promise.all(await this.getSyncPromises(locations, userId, dto.platforms))
    })
  }


  @Put('api/location')
  @UseGuards(AuthGuard)
  async syncApisForLocation(@BearerToken() token: string, @Body(new ValidationPipe()) dto: SyncLocationDto) {
    return await this.syncLocationByToken(token, dto.locationId, async (location, userId) => {
      return await Promise.all(this.getSyncPromises([location], userId, dto.platforms))
    })
  }

  @Get('direct-user')
  async syncDirectUser() {
    const directUsers = await this.directUserRepository.findAll()

    directUsers.forEach(({ userId }) => {
      // Start api sync
      this.syncLocationsByUserId(userId, async (locations, userId) => {
        return await Promise.all(await this.getSyncPromises(locations, userId, ['instagram', 'google', 'facebook']))
      })
      // Start crawler sync
      return this.syncLocationsByUserId(userId, async (locations, userId) => {
        return await this.syncCrawlerService.syncReviewsForLocations(locations, userId, TaskPriority.HIGH)
      })
    })
  }

  @Get('crawler')
  async syncUserCrawlerPlaforms() {
    const startTime = new Date()
    await this.syncCrawlerReviewsAndStatistics()
    return {
      message: 'Queued all crawler representations',
      elapsedTime: `${DateHelper.getDifference(startTime)}s`,
    }
  }

  @Put('crawler/location')
  @UseGuards(AuthGuard)
  async syncCrawlerForLocation(@BearerToken() token: string, @Body(new ValidationPipe()) dto: SyncLocationCrawlerDto) {
    return this.syncLocationByToken(token, dto.locationId, async (location, userId) => {
      return await this.syncCrawlerService.syncReviewsForLocations([location], userId)
    })
  }

  @Get('crawler/retry')
  @UseGuards(AdminGuard)
  async retryCrawlerTasks() {
    return await this.syncCrawlerService.retryCrawlerTasks(['yelp'])
  }

  @Put('crawler/user')
  @UseGuards(AuthGuard)
  async syncCrawlerForUser(@BearerToken() token: string) {
    return this.syncLocationsByToken(token, async (locations, userId) => {
      return await this.syncCrawlerService.syncReviewsForLocations(locations, userId)
    })
  }

  @Put('crawler/representation')
  @UseGuards(AuthGuard)
  async syncCrawlerForRepresentation(@BearerToken() token: string, @Body(new ValidationPipe()) dto: SyncRepresentationCrawlerDto) {
    const representation = await this.locationRepository.getRepresentationById(dto.locationId, dto.representationId)
    const userId = await this.currentUserHelper.getUserIdFromVerification(token)
    return await this.syncCrawlerService.syncReviewsForRepresentation(representation, dto.locationId, userId)
  }

  @Put('admin/representation')
  @UseGuards(AdminGuard)
  async adminSyncCrawlerForRepresentation(@Body(new ValidationPipe()) dto: SyncAdminRepresentationCrawlerDto) {
    const representation = await this.locationRepository.getRepresentationById(dto.locationId, dto.representationId)
    if (!representation)
      return {
        success: false,
        message: `Representation with id: ${dto.representationId} not found`,
      }
    if (representation.type === 'crawler')
      return await this.syncCrawlerService.syncReviewsForRepresentation(representation, dto.locationId, dto.userId)
    const service = this.getSyncServiceByPlatform(representation.platform)
    if (!service)
      return {
        success: false,
        message: `Platform not supported: ${representation.platform}`,
      }
    const location = await this.locationRepository.findDoc(dto.locationId)
    return await service.syncReviewsForLocations([location], dto.userId)
  }


  // Helper methods

  async syncLocationByToken(token: string, locationId: string, onLocation: (location: Location, userId: string) => {}) {
    const startTime = new Date()
    const userId = await this.currentUserHelper.getUserIdFromVerification(token)
    const locations = await this.getLocationsByUserId(userId)
    const location = locations.find(location1 => location1.id === locationId)
    if (!location)
      return { message: `Location ${locationId} not found for user ${userId}` }
    try {
      await onLocation(location, userId)
    } catch (e) {
      return {
        message: `update canceled`,
        error: e.message,
      }
    }
    return {
      message: 'update finished',
      elapsedTime: `${DateHelper.getDifference(startTime)}s`,
    }
  }

  async syncLocationsByToken(token: string, onLocations: (locations: Location[], userId: string) => {}) {
    const userId = await this.currentUserHelper.getUserIdFromVerification(token)
    return this.syncLocationsByUserId(userId, onLocations)
  }

  @Put('test')
    async test(@Body() options) {
    if (process.env.NODE_ENV === 'development') {
      console.log(options)
      return await this.syncCrawlerService.queueCrawlerTask(options)
    }
  }

  async syncLocationsByUserId(userId: string, onLocations: (locations: Location[], userId: string) => {}) {
    const startTime = new Date()
    const locations = await this.getLocationsByUserId(userId)
    if (!locations) {
      const message = 'no locations found for user ' + userId
      console.log(message)
      return { message }
    }
    try {
      await onLocations(locations, userId)
    } catch (e) {
      return {
        message: `update canceled`,
        error: e.message,
      }
    }
    return {
      message: 'update finished',
      elapsedTime: `${DateHelper.getDifference(startTime)}s`,
    }
  }

  getSyncPromises(locations: Location[], userId: string, platforms?: string[]) {
    const promises = []
    for (const platform of platforms) {
      const syncService = this.getSyncServiceByPlatform(platform)
      if (syncService)
        promises.push(syncService.syncReviewsForLocations(locations, userId))
    }
    return promises
  }

  getSyncServiceByPlatform(platform: string): SyncBaseService {
    switch (platform) {
      case 'google':
        return this.syncGoogleService
      case 'facebook':
        return this.syncFacebookService
      case 'crawler':
        return this.syncCrawlerService
      case 'instagram':
        return this.syncInstagramService
      default:
        throw new PlatformNotSupportedException(platform)
    }
  }

  async syncApiReviewsAndStatistics(platform: string) {
    await this.forAllUsersWithActiveSubscription(async (locations, userId) => {
      console.log('updating api reviews for user ' + userId)
      await this.getSyncServiceByPlatform(platform).syncReviewsForLocations(locations, userId)
    })
  }

  async syncCrawlerReviewsAndStatistics() {
    await this.forAllUsersWithActiveSubscription((async (locations, userId) => {
      console.log('updating crawler reviews for user ' + userId)
      await this.syncCrawlerService.syncReviewsForLocations(locations, userId)
    }))
  }

  async forAllUsersWithActiveSubscription(onLocations: (locations: Location[], userId: string) => void) {
    const userIds = await this.paymentService.getCustomerIdsWithActiveSubscriptions()
    console.log(`having user ids: ${userIds.length}`)
    await Promise.all(userIds.map(async (userId) => {
      const exists = await this.userRepository.doesExists(userId)
      if (!exists) {
        console.log(`Found a customer at chargebee which doesnt exists in the firestore database: ${userId}`)
        return Promise.resolve()
      }
      const locations = await this.getLocationsByUserId(userId)
      if (!locations)
        return Promise.resolve()
      return onLocations(locations, userId)
    }))
  }

  async getLocationsByUserId(userId): Promise<Location[]> {
    const plainLocations = await this.locationRepository.findByUserId(userId)
    if (!plainLocations || plainLocations.length === 0) {
      console.log('no locations found for user ' + userId)
      return null
    }
    return await plainToClass(Location, plainLocations) as Location[]
  }

}
