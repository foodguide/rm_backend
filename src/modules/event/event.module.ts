import { Module } from '@nestjs/common'
import { EventRepository } from './event.repository'
import { EventController } from './event.controller'

@Module({
  providers: [EventRepository],
  controllers: [EventController],
  exports: [EventRepository],
})

export class EventModule {}
