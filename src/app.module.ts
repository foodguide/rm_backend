import { forwardRef, Module } from '@nestjs/common'
import { SyncModule } from './modules/sync/sync.module'
import { DatabaseModule } from './modules/database/database.module'
import { ConfigModule } from './config/config.module'
import { AuthModule } from './modules/auth/auth.module'
import { GmbModule } from './modules/googlemybusiness/gmb.module'
import { ErrorModule } from './modules/error/error.module'
import { WorkerModule } from './modules/worker/worker.module'
import { PaymentModule } from './modules/payment/payment.module'
import { MessageModule } from './modules/message/message.module'
import { UserModule } from './modules/user/user.module'
import { StatusModule } from './modules/status/status.module'
import { HealthModule } from './modules/health/health.module'
import { FacebookModule } from './modules/facebook/facebook.module'
import { WidgetModule } from './modules/widget/widget.module'
import { DirectModule } from './modules/direct/direct.module'
import { QuickreplyModule } from './modules/quickreply/quickreply.module'
import { ReviewModule } from './modules/review/review.module'

@Module({
  imports: [
    forwardRef(() => UserModule),
    SyncModule,
    DatabaseModule,
    ConfigModule,
    ErrorModule,
    AuthModule,
    GmbModule,
    WorkerModule,
    PaymentModule,
    MessageModule,
    StatusModule,
    HealthModule,
    FacebookModule,
    WidgetModule,
    DirectModule,
    QuickreplyModule,
    forwardRef(() => ReviewModule),
  ],
})

export class AppModule {
}
