import moment from 'moment'

/**
 * This simple cache allows to store entries and their storage date.
 */
export class TimedCache {
  cache: Map<string, CacheEntry>
  maxAgeInSeconds: number

  constructor(maxAgeInSeconds: number) {
    this.cache = new Map<string, CacheEntry>()
    this.maxAgeInSeconds = maxAgeInSeconds
  }

  /**
   * Return entry from cache if not outdated or retrieve from original function and store to cache.
   *
   * @param key
   * @param func
   */
  async getEntryOrCreate(key, func) {
    const cacheEntry = this.cache.get(key)
    if (cacheEntry) {
      const entryAgeInSeconds = moment().diff(moment(cacheEntry.created), 'seconds')
      if (entryAgeInSeconds < this.maxAgeInSeconds) {
        return cacheEntry.value
      } else {
        this.cache.delete(key)
      }
    }
    const entry = await func()
    if (entry) {
      this.cache.set(key, new CacheEntry(entry))
    }

    return entry
  }

}

class CacheEntry {
  value: Object
  created: Date

  constructor(value) {
    this.value = value
    this.created = new Date()
  }
}
