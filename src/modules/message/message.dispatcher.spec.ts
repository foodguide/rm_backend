import 'reflect-metadata'
import { BaseMessageQueueItem, MessageSubscription } from './message.model'
import { MessageName } from './message.name'
import { EmailService } from './email/email.service'
import { MessageDispatcher, MessageDispatcherUnsupportedTypeError } from './message.dispatcher'
import { Test } from '@nestjs/testing'
import { MessageType } from './message.type'

describe('MessageDispatcher', () => {


  let emailService: EmailService
  let messageDispatcher: MessageDispatcher
  let message: BaseMessageQueueItem

  beforeEach(async () => {
    // @ts-ignore
      message = new BaseMessageQueueItem()
    message.subscription = new MessageSubscription()

    const testmodule = await Test.createTestingModule({
      providers: [
        {
          provide: EmailService,
          useValue: {
            send: () => {
              return 'emailService'
            }
          }
        },
        MessageDispatcher
      ]
    }).compile()

    emailService = testmodule.get<EmailService>(EmailService)
    messageDispatcher = testmodule.get<MessageDispatcher>(MessageDispatcher)

  })

  describe('dispatch', () => {

    it('Message with type Email should be dispatched to emailService ', async () => {
      message.subscription.name = MessageName.Basic
      message.subscription.type = MessageType.Email
      message.payload = {}

      const result = 'emailService'

      expect(await messageDispatcher.dispatch(message)).toBe(result)

    })

    it('Dispatching message with unsupported type should throw MessageDispatcherUnsupportedTypeError', async () => {
      message.subscription.name = MessageName.Basic
      message.subscription.type = MessageType.UNSUPPORTED
      message.payload = {}

      try {
        await messageDispatcher.dispatch(message)
      } catch (e) {
        expect(e).toEqual((new MessageDispatcherUnsupportedTypeError(message.subscription.type)))
      }
    })
  })
})