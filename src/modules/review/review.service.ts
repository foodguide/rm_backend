import { Inject, Injectable } from '@nestjs/common'
import { ReviewRepository } from './review.repository'
import { LocationRepository } from '../location/location.repository'
import { BulkTimeRange, DeleteReviewsDto, Review, UpdateReviewsDto } from './review.model'
import { GmbService } from '../googlemybusiness/gmb.service'
import { FacebookService } from '../facebook/facebook.service'
import { InstagramService } from '../facebook/instagram.service'
import { Database } from '../database/database.baseProvider'
import { classToPlain } from 'class-transformer'
import { UserRepository } from '../user/user.repository'

/**
 * This is the service class for all review related things.
 */
@Injectable()
export class ReviewService {
  /**
   * The constructor function of the review service
   *
   * @param reviewRepository
   * @param locationRepository
   * @param userRepository
   * @param gmbService
   * @param facebookService
   * @param instagramService
   * @param database
   */
  constructor(
    private readonly reviewRepository: ReviewRepository,
    private readonly locationRepository: LocationRepository,
    private readonly userRepository: UserRepository,
    private readonly gmbService: GmbService,
    private readonly facebookService: FacebookService,
    private readonly instagramService: InstagramService,
    @Inject('Database') database: Database,
  ) {
    this.crawlerReplyQueue = database.firebase.ref('crawlerReplyQueue/tasks')
    this.apiReplyQueue = database.firebase.ref('apiReplyQueue/tasks')
  }

  private readonly crawlerReplyQueue
  private readonly apiReplyQueue

  /**
   * This method checks if at least one of the properties from the time range is set
   *
   * @param {BulkTimeRange} dto
   * @return {Boolean}
   */
  static hasValidTimeInformation(dto: BulkTimeRange): Boolean {
    return !!dto.after || !!dto.before
  }

  /**
   * This function finds all reviews for a specific user in the given time range
   *
   * @param {string} userId
   * @param {BulkTimeRange} dto
   * @return {Promise<any[]>}
   */
  findAllBetweenDates(userId: string, dto: BulkTimeRange): Promise<any[]> {
    const after = dto.after ? new Date(dto.after) : null
    const before = dto.before ? new Date(dto.before) : null
    return this.reviewRepository.findAllBetweenDates(userId, before, after)
  }

  /**
   * This function updates the state for all reviews for a specific user in the
   * given time range.
   *
   * @param {UpdateReviewsDto} dto
   * @return {Promise<any>}
   */
  async updateStateForAllBetweenDates(dto: UpdateReviewsDto): Promise<any> {
    const reviews = await this.findAllBetweenDates(dto.userId, dto)
    const promises = []
    reviews.forEach((review) => {
      if (review.state !== dto.state) {
        promises.push(this.reviewRepository.update(review.id, {
          state: dto.state,
        }))
      }
    })
    return Promise.all(promises)
  }

  /**
   * This function deletes all the reviews for a specific user in the given time range.
   *
   * @param {DeleteReviewsDto} dto
   * @return {Promise<any>}
   */
  deleteAllBetweenDates(dto: DeleteReviewsDto) {
    const after = dto.after ? new Date(dto.after) : null
    const before = dto.before ? new Date(dto.before) : null
    const query = this.reviewRepository.getAllBetweenDatesQuery(dto.userId, before, after)
    return this.reviewRepository.deleteQuery(query)
  }

  /**
   * This function sends a reply to be queued.
   *
   * @param user
   * @param reviewId
   * @param text
   */
  async sendReply(user, reviewId, text) {
    this.queueReplyTask(reviewId, user.id, text)
  }

  /**
   * This function decides where to queue a reply and pushes it to the queue.
   *
   * @param reviewId
   * @param userId
   * @param text
   */
  async queueReplyTask(reviewId: string, userId: string, text: string) {
    const review = await this.reviewRepository.findById(reviewId)
    const representation = await this.locationRepository.getRepresentationById(review.locationId, review.representationId)
    const queue = (apiPlatforms.find(platform => representation.platform === platform))
      ? this.apiReplyQueue
      : this.crawlerReplyQueue

    const replyTask = new ReplyTask({
      text,
      userId,
      id: reviewId,
    })
    try {
      const task = await new Promise((resolve) => {
        queue.child(replyTask.id).once('value', (snapshot) => {
          resolve(snapshot.val())
        })
      }) as any
      if (!!task && task.state === TaskState.RUNNING) {
        console.log(`Reply task for review ${reviewId} is already running`)
      } else {
        queue.child(replyTask.id).set(classToPlain(replyTask)).then(() => {
          console.log(`Queued reply task for review ${reviewId}`)
        })
      }
    } catch (e) {
      console.error(e)
    }
  }

  async sendReplyToService(reviewId, userId, text) {
    const review = await this.reviewRepository.findById(reviewId)
    const representation = await this.locationRepository.getRepresentationById(review.locationId, review.representationId)
    const user = await this.userRepository.findById(userId)
    this.getReplyPromise(review, representation, user, text).then(() => {
      this.reviewRepository.replyToReview(reviewId, {
        text,
        id: `reply_${reviewId}`,
      })
    })
  }

  async getReplyPromise(review, representation, user, text) {
    switch (representation.platform) {
      case 'google': return this.gmbService.sendReply(user, review, representation, text)
      case 'facebook': return this.facebookService.sendReply(user, review, representation, text)
      case 'instagram': return this.instagramService.sendReply(user, review, representation, text)
      default: return Promise.reject('Platform not available')
    }
  }

}

class ReplyTask {

  id: string
  userId: string
  text: string
  state: number

  constructor(options) {
    this.id = options.id
    this.userId = options.userId
    this.text = options.text
    this.state = options.state ? options.state : TaskState.WAITING
  }

}

enum TaskState {
  WAITING = 'WAITING',
  RUNNING = 'RUNNING',
  DONE = 'DONE',
  FAILED = 'FAILED',
}

const apiPlatforms = ['facebook', 'instagram', 'google']
const crawlerPlatforms = ['yelp', 'tripadvisor', 'opentable']
