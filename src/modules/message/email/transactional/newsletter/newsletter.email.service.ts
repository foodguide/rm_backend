import { Injectable } from '@nestjs/common'
import { LocationHelper } from '../../../../helper/helper.locationHelper'
import { NewsletterEmail } from './newsletter.email'
import { MessagePublisher } from '../../../../publisher/publisher.message'
import { CurrentUserHelper } from '../../../../helper/helper.currentUserHelper'
import { EmailRecipient } from '../../email.model'
import { QuestionnaireEmail } from './questionnaire.email'
import moment from 'moment'

@Injectable()
export class NewsletterEmailService {
  constructor(
    private readonly locationHelper: LocationHelper,
    private readonly messagePublisher: MessagePublisher,
    private readonly currentUserHelper: CurrentUserHelper,
  ) {}

  public async sendNewsletterMails(templateId: string) {
    const users = await this.locationHelper.findAllUsersWithActiveSubscriptions()
    for (const user of users) {
      if (user.subscriptions && user.subscriptions.find(subscription => subscription.name === 'basic')) {
        const newsletterMail = new NewsletterEmail(templateId)
        newsletterMail.setRecipients([new EmailRecipient().setEmail(user.email)])
        const pipedriveBcc = await this.currentUserHelper.getPipedriveBcc(user.uid)
        if (!!pipedriveBcc) newsletterMail.setBccRecipients([new EmailRecipient().setEmail(pipedriveBcc)])
        this.messagePublisher.push(newsletterMail, user)
      }
    }
  }

  public async sendQuestionnaireMails() {
    const users = await this.locationHelper.findAllUsersWithActiveSubscriptions()
    const recipients = users.filter(((user) => {
      const monthsSinceCreation = moment.duration(moment().diff(moment(user.metadata.creationTime))).asMonths()
      return monthsSinceCreation >= 2
    }))

    for (const user of recipients) {
      const questionnaireEmail = new QuestionnaireEmail(user.locale)
      questionnaireEmail.setRecipients([new EmailRecipient().setEmail(user.email)])
      const pipedriveBcc = await this.currentUserHelper.getPipedriveBcc(user.uid)
      if (!!pipedriveBcc) questionnaireEmail.setBccRecipients([new EmailRecipient().setEmail(pipedriveBcc)])
      this.messagePublisher.push(questionnaireEmail, user)
    }
  }

}
