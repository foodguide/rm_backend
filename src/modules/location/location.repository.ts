import { BaseRepository } from '../database/database.baseRepository'
import { Injectable } from '@nestjs/common'
import { BaseRepresentation } from './location.model'
import { classToPlain } from 'class-transformer'

@Injectable()
export class LocationRepository extends BaseRepository {
  managedCollection = 'location'

  get representationCollection() {
    return 'representations'
  }

  getRepresentationsByLocationId(id) {
    return this.commitRead(
            this.collection
            .doc(id)
            .collection(this.representationCollection)
            .get(),
        )
  }

  getRepresentationById(locationId, representationId) {
    if (!locationId || !representationId)
      return null
    return this.commitRead(
            this.collection
            .doc(locationId)
            .collection(this.representationCollection)
            .doc(representationId)
            .get(),
        )
  }

  updateRepresentation(locationId: string, representation: BaseRepresentation, useTransformer = true) {
    const data = useTransformer ? this.clean(classToPlain(representation)) : representation
    return this.commitWrite(
            this.collection
            .doc(locationId)
            .collection(this.representationCollection)
            .doc(representation.id))
        .update(data)
  }

  updateExternalReviewCount(locationId: string, representation: BaseRepresentation, externalReviewCount) {
    return this.commitWrite(
          this.collection
            .doc(locationId)
            .collection(this.representationCollection)
            .doc(representation.id))
          .update({ externalReviewCount })
  }

  private clean(obj) {
    Object.keys(obj).forEach((key) => {
      obj[key] === undefined ? delete obj[key] : ''
      if (!!obj[key] && typeof obj[key] === 'object')
        obj[key] = this.clean(obj[key])
    })
    return obj
  }
}
