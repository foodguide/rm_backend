import { Module } from '@nestjs/common'
import { RepresentationStatisticRepository } from './representationStatistic.repository'
import { RepresentationStatistic } from './representationStatistic.model'

@Module({
  providers: [RepresentationStatisticRepository, RepresentationStatistic],
  exports: [RepresentationStatisticRepository, RepresentationStatistic],
})

export class RepresentationStatisticModule{}
