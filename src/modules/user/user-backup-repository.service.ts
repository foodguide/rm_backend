import { Injectable } from '@nestjs/common'
import { BaseRepository } from '../database/database.baseRepository'

/**
 * ONLY FOR BACKUP
 */
@Injectable()
export class UserBackupRepository extends BaseRepository {
  managedCollection = 'userBackup'
}
