import dotenv from 'dotenv'
import fs from 'fs'

export class ConfigService {
  private readonly envConfig: { [prop: string]: string }

  constructor(filePath: string | string[]) {
    if (typeof filePath === 'string') {
      this.envConfig = dotenv.parse(fs.readFileSync(filePath))
    } else {
      let config = {}
      filePath.forEach((file) => {
        config = { ...config, ...dotenv.parse(fs.readFileSync(file)) }
      })
      this.envConfig = config
    }
  }

  get(key: string): string {
    return this.envConfig[key]
  }
}
