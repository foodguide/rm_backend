import { Body, Controller, Delete, Get, Put, Query, UseGuards, ValidationPipe } from '@nestjs/common'
import { UserRepository } from './user.repository'
import { AuthGuard } from '../auth/auth.guard'
import {
    MessageSubscription,
    MessageSubscriptionDto,
    MessageSubscriptionsDto,
    UserIdDto,
    GuestMessageSubscriptionsDto, UserPipedriveDto,
} from '../message/message.model'
import { BearerToken } from '../auth/auth.bearerdecorator'
import { CurrentUserHelper } from '../helper/helper.currentUserHelper'
import { NextPageTokenDto, User } from './user.model'
import { AdminGuard } from '../auth/admin.guard'
import { WelcomeEmail } from '../message/email/transactional/welcome/welcome.email'
import { EmailRecipient } from '../message/email/email.model'
import { MessagePublisher } from '../publisher/publisher.message'
import { EventRepository } from '../event/event.repository'
import { GoogleAccessTokenProvider } from '../api/google/api.googleaccesstokenprovider.model'
import { GmbService } from '../googlemybusiness/gmb.service'
import { LocationRepository } from '../location/location.repository'
import { LocationHelper } from '../helper/helper.locationHelper'
import { FacebookService } from '../facebook/facebook.service'
import { SlackApi } from '../api/slack/api.slack'

@Controller('user')
export class UserController {

  constructor(
      private readonly userRepository: UserRepository,
      private readonly currentUserHelper: CurrentUserHelper,
      private readonly locationHelper: LocationHelper,
      private readonly messagePublisher: MessagePublisher,
      private readonly eventRepository: EventRepository,
      private readonly locationRepository: LocationRepository,
      private readonly gmbService: GmbService,
      private readonly facebookService: FacebookService,
      private readonly slackApi: SlackApi) {
  }

  @UseGuards(AdminGuard)
    @Get()
    async getAll(@Query(new ValidationPipe()) dto: NextPageTokenDto) {
    return this.userRepository.findAllWithDatabase()
  }

  @UseGuards(AuthGuard)
    @Put('subscription')
    async subscribe(
      @BearerToken() token: string,
      @Body(new ValidationPipe()) dto: MessageSubscriptionsDto,
    ) {
    const user = await this.currentUserHelper.getCurrentUser(token)
    if (user) {
      const messages = dto.subscriptions.map(subscriptionDto => new MessageSubscription(subscriptionDto))
      return {
        subscriptions: await this.userRepository.subscribeUserToMessages(user, messages),
      }
    }
  }

  @UseGuards(AuthGuard)
    @Delete('subscription')
    async unsubscribe(
      @BearerToken() token: string,
      @Body(new ValidationPipe()) dto: MessageSubscriptionDto,
    ) {
    const user = await this.currentUserHelper.getCurrentUser(token)
    return this.unsubscribeUser(user, new MessageSubscription(dto))
  }

  async unsubscribeUser(user: User, subscription: MessageSubscription) {
    if (user)
      return {
        subscriptions: await this.userRepository.unsubscribeUserFromMessage(user, subscription),
      }
  }

  @Put('oncreate')
  async onCreate(@Body(new ValidationPipe()) dto: UserIdDto) {
    await this.sendNewUserMessageToSlack(dto)
    await this.sendVerificationMail(dto)
  }

  async sendVerificationMail(dto: UserIdDto) {
    const user = await this.currentUserHelper.getUser(dto.userId)
    if (user.providerData.length === 1 && user.providerData[0].providerId === 'facebook.com')
      return

    if (!user || !user.email) return

    const link = await this.currentUserHelper.getEmailVerificationLink(user.email, dto.userId)
    const welcomeMail = new WelcomeEmail(user.locale)
          .setEmailVerificationLink(link)
          .setRecipients([new EmailRecipient().setEmail(user.email)])

    const pipedriveBcc = await this.currentUserHelper.getPipedriveBcc(user.uid)
    if (!!pipedriveBcc) welcomeMail.setBccRecipients([new EmailRecipient().setEmail(pipedriveBcc)])

    this.messagePublisher.push(welcomeMail, user)
  }

  async sendNewUserMessageToSlack(dto: UserIdDto) {
    const user = await this.currentUserHelper.getFirebaseUser(dto.userId)
    const message = user
      ? `@here User ${user.email} with id '${user.uid}' has created a Respondo account`
      : '@here A Respondo account has been created'
    this.slackApi.sendMessageToChannel('respondo', message, null)
  }

  @Get('notificationsubscriptions')
    async getGuestNotificationSubscriptions(@Query(new ValidationPipe()) dto: UserIdDto) {
    const user = await this.userRepository.findById(dto.userId)
    return user.subscriptions
  }

  @Put('updatenotificationsubscriptions')
  async putGuestNotificationSubscriptions(@Body(new ValidationPipe()) dto: GuestMessageSubscriptionsDto) {
    const user = await this.userRepository.findById(dto.userId)
    const messages = dto.subscriptions.map(subscription => new MessageSubscription(subscription))
    return this.userRepository.updateMessages(user, messages)
  }

  @UseGuards(AdminGuard)
    @Put('updatepipedriveid')
    async updatePipedriveId(@Body(new ValidationPipe()) dto: UserPipedriveDto) {
    const user = await this.userRepository.findById(dto.userId)
    return this.userRepository.updatePipedriveId(user, dto.pipedriveId)
  }

  @Get('activetime')
    async updateLastActiveTime() {
    const users = await this.userRepository.findAllWithDatabase()
    for (const user of users) {
      const lastEvent = await this.eventRepository.findLastForUser(user.uid)
      if (lastEvent.length > 0) {
        user.lastActiveTime = lastEvent[0].timestamp
        this.userRepository.updateLastActiveTime(user)
      }
    }
  }

  @Get('googletokens')
    async clearGoogleTokens() {
    const authResult = await this.userRepository.findAll()
    for (const authUser of authResult.users) {
      try {
                /*
                We need the dbUser because the gmb request could force an update of the user object
                and 'user' from this.userRepository.findAllWithDatabase() contains unnecessary empty fields.
                 */
        const dbUsers = await this.userRepository.findByUserId(authUser.uid)
        if (dbUsers.lentgh === 0) continue
        const dbUser = dbUsers[0]
        if (!dbUser) continue

        const index = dbUser.accessTokens.findIndex(token => token.platform === 'google')
        if (index < 0) continue
        const googleAccessToken = dbUser.accessTokens.find(token => token.platform === 'google')
        if (!googleAccessToken) continue
        const uuidv1 = require('uuid/v1')
        googleAccessToken.id = uuidv1()
        const accounts = await this.gmbService.gmbApi.getAccounts(new GoogleAccessTokenProvider(dbUser))
        accounts.subscribe((result) => {
          googleAccessToken.account = result.accounts[0]
          dbUser.accessTokens[index] = googleAccessToken
          this.userRepository.update(dbUser.id, dbUser)
        })

        const locations = await this.locationRepository.findByUserId(dbUser.id)
        for (const location of locations) {
          const representations = await this.locationRepository.getRepresentationsByLocationId(location.id)
          representations.forEach((representation) => {
            if (representation.platform === 'google') {
              representation.accessTokenId = googleAccessToken.id
              representation.accessToken = null
              this.locationRepository.updateRepresentation(location.id, representation)
            }
          })
        }
      } catch (e) {
        console.log('error while google token clearing')
        console.log('userId: ' + authUser.uid)
      }
    }
  }

  @Get('facebooktokens')
    /**
     * Enrich Facebook accessTokens with account information.
     */
    async clearFacebookTokens() {
    const authResult = await this.userRepository.findAll()
    const uuidv1 = require('uuid/v1')
    for (const authUser of authResult.users) {
      try {
                /*
                We need the dbUser because the gmb request could force an update of the user object
                and 'user' from this.userRepository.findAllWithDatabase() contains unnecessary empty fields.
                 */
        const dbUsers = await this.userRepository.findByUserId(authUser.uid)
        if (dbUsers.lentgh === 0) continue
        const dbUser = dbUsers[0]
        if (!dbUser) continue

        const index = dbUser.accessTokens.findIndex(token => token.platform === 'facebook')
        if (index < 0) continue
        const facebookCredential = dbUser.accessTokens.find(token => token.platform === 'facebook')
        if (!facebookCredential) continue

        facebookCredential.id = uuidv1()
        const account = await this.facebookService.facebookApi.getAccountInformation(facebookCredential.accessToken)
        if (account) {
          facebookCredential.account = account
          dbUser.accessTokens[index] = facebookCredential
          this.userRepository.update(dbUser.id, dbUser)
        }

        const locations = await this.locationRepository.findByUserId(dbUser.id)
        for (const location of locations) {
          const representations = await this.locationRepository.getRepresentationsByLocationId(location.id)
          representations.forEach((representation) => {
            if (representation.platform === 'facebook') {
              representation.accessTokenId = facebookCredential.id
              this.locationRepository.updateRepresentation(location.id, representation)
            }
          })
        }
      } catch (e) {
        this.slackApi.sendMessageToChannel(
                  'review-manager-errors',
                  `User ${authUser.uid} has no valid Facebook authorization for Respondo.`,
                  null,
                )
        console.log('error while facebook token clearing')
        console.log('userId: ' + authUser.uid)
      }
    }
  }
}
