import { IsBoolean, IsInt, IsNotEmpty, IsOptional, IsString } from 'class-validator'

export class ExistingCustomerDto {
  @IsNotEmpty()
  @IsString()
  customerId: string
}

export class NewSubscriptionDto {
  @IsNotEmpty()
  @IsString()
  planId: string
}

export class ExistingSubscriptionDto {
  @IsNotEmpty()
  @IsString()
  subscriptionId: string
}

export class UpdateSubscriptionDto {
  @IsNotEmpty()
  @IsString()
  subscriptionId: string
  @IsNotEmpty()
  @IsInt()
  numberOfLocations: number
}

export class UpgradeSubscriptionDto {
  @IsNotEmpty()
  @IsString()
  subscriptionId: string
}

export class HostedPageDto {
  @IsNotEmpty()
  @IsString()
  hostedPageId: string
}
