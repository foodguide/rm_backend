import { Injectable, CanActivate, ExecutionContext, Inject } from '@nestjs/common'
import { Observable } from 'rxjs'
import { Database } from '../database/database.prodProvider'
import { SlackApi } from '../api/slack/api.slack'

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    @Inject('Database') private readonly database: Database,
    protected readonly slackApi: SlackApi) {
  }

  canActivate(
        context: ExecutionContext,
    ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest()
    return this.validateRequest(request)
  }

  validateRequest(request): boolean | Promise<boolean> {
    if (!request.headers.authorization)
      return false
    const token = request.headers.authorization.split('Bearer ')[1]
    return this.database.auth.verifyIdToken(token).then((claims) => {
      return this.validateClaims(claims)
    }).catch(() => {
      return false
    })
  }

  protected validateClaims(claims): boolean {
    return true
  }
}
