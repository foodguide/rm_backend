import { Module } from '@nestjs/common'
import { StatisticWorker } from './worker.statistic'
import { SyncModule } from '../sync/sync.module'
import { MessageWorker } from './worker.message'
import { ApiReplyWorker } from './worker.apiReply'
import { MessageModule } from '../message/message.module'
import { ErrorWorker } from './worker.error'

// @ts-ignore
@Module({
  imports: [SyncModule, MessageModule],
  providers: [StatisticWorker, MessageWorker, ApiReplyWorker, ErrorWorker],

})

export class WorkerModule {
}
