import { Inject, Injectable } from '@nestjs/common'
import { Database } from '../database/database.baseProvider'

@Injectable()
export abstract class BasePublisher {

  protected queue
  protected _queueReference

  protected constructor(@Inject('Database') protected database: Database) {
    this.queue = this.database.firebase.ref(this.queueReference)
  }

  push(item, user?) {
    this.queue.push(item)
  }

  get queueReference() {
    return this._queueReference
  }
}
