import { Database } from '../database/database.baseProvider'
import firebaseQueue from 'firebase-queue'
import { Inject, Injectable } from '@nestjs/common'
import { ConfigService } from '../../config/config.service'

@Injectable()
export abstract class BaseWorker {

  private _queue: firebaseQueue
  private _options: object = {}
  private _queueReference: string

  protected constructor(@Inject('Database') database: Database,
                        protected config: ConfigService) {

    if (config.get('WORKER_ENABLED') === 'false') return

    this._queue = new firebaseQueue(
      database.firebase.ref(this.queueReference),
      this.options,
      (data, progress, resolve, reject) => {
        this.process(data, progress, resolve, reject)
      },
    )
  }

  abstract process(data, progress, resolve, reject)

  get queueReference() {
    return this._queueReference
  }

  set queueReference(queueName: string) {
    this._queueReference = queueName
  }

  get options() {
    return this._options
  }

  set options(options: object) {
    this._options = options
  }
}
