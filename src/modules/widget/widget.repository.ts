import { Injectable } from '@nestjs/common'
import { BaseRepository } from '../database/database.baseRepository'

@Injectable()
export class WidgetRepository extends BaseRepository {
  managedCollection = 'widget'
}
