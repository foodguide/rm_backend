import { BaseModel } from '../database/database.baseModel'
import { Transform, Type } from 'class-transformer'

export class LocationStatistic extends BaseModel{
  locationId: string
  locationName: string
  @Type(() => Date)
  @Transform((value, obj) => obj.date.toDate(), { toClassOnly: true })
  date: Date
  totalReviews: number
  avgRating: number
  avgRatingOld: number
}
