import { MessageSubscription } from './message.model'

export interface MessageQueueItem {
  subscription: MessageSubscription
  payload: object

  getSubscription()
  getPayload()
}
