import { Module, Global, forwardRef } from '@nestjs/common'
import { DateHelper } from './helper.dateHelper'
import { InstagramHelper } from './helper.instagramHelper'
import { CurrentUserHelper } from './helper.currentUserHelper'
import { UserModule } from '../user/user.module'
import { DatabaseModule } from '../database/database.module'
import { ConfigModule } from '../../config/config.module'
import { LocationHelper } from './helper.locationHelper'
import { LocationModule } from '../location/location.module'
import { FacebookHelper } from './helper.facebookHelper'
import { ArrayHelper } from './helper.arrayHelper'
import { PaymentModule } from '../payment/payment.module'

const helpers = [
  DateHelper,
  InstagramHelper,
  CurrentUserHelper,
  LocationHelper,
  FacebookHelper,
  ArrayHelper,
]

@Global()
// @ts-ignore
@Module({
  imports: [
    forwardRef(() => UserModule),
    DatabaseModule,
    ConfigModule,
    LocationModule,
    forwardRef(() => PaymentModule),
  ],
  providers: helpers,
  exports: helpers,
})
export class HelperModule {
}
