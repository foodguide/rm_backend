import { Module, Global } from '@nestjs/common'
import { BaseModel } from './database.baseModel'
import { BaseRepository } from './database.baseRepository'
import { Database } from './database.baseProvider'
import { DevDatabase } from './database.devProvider'
import { ProdDatabase } from './database.prodProvider'

const database = {
  provide: 'Database',
  useClass: process.env.NODE_ENV === 'development'
    ? DevDatabase
    : ProdDatabase,
}

@Global()
// @ts-ignore
@Module({
  providers: [BaseModel, BaseRepository, database],
  exports: [BaseModel, BaseRepository, 'Database'],
})

export class DatabaseModule {
}
