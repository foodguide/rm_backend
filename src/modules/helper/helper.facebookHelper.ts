import { Injectable } from '@nestjs/common'
import moment from 'moment'
import { Representation } from '../location/location.model'

@Injectable()
export class FacebookHelper {
  static findResponseInComments(review, representation: Representation) {
    if (review.platform !== 'facebook' || representation.platform !== 'facebook') {
      if (!review.open_graph_story.id)
        return null
    }
    if (!review.comments || review.comments.length === 0)
      return null
    const comments = FacebookHelper.getSortedComments(review)
    const answerComment = FacebookHelper.findAnswerComment(comments, representation)
    if (!answerComment)
      return null
    const response = {
      id: answerComment.id,
      responder: null,
      text: answerComment.message,
    }
    return response
  }

  private static findAnswerComment(comments, representation: Representation) {
    return comments.find(comment => !!comment.from ? comment.from.id === representation.externalId : false)
  }

  private static getSortedComments(review: any) {
    return review.comments.sort((comment1, comment2) => {
      const moment1 = moment(comment1.created_time)
      const moment2 = moment(comment2.created_time)
      return moment1.diff(moment2)
    })
  }
}
