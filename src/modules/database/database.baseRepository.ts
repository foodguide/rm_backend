import { BaseModel } from './database.baseModel'
import { Inject, Injectable } from '@nestjs/common'
import { Database } from './database.baseProvider'
import { MissingRequiredPropertyException } from './database.Errors'
import { classToPlain } from 'class-transformer'
import firebaseAdmin from 'firebase-admin'

@Injectable()
export abstract class BaseRepository {
  abstract managedCollection: string

  constructor(readonly database: Database) {
  }

  get collection() {
    return this.database.firestore.collection(this.managedCollection)
  }

  async findAll() {
    return this.commitRead(this.collection.get())
  }

  create(model: BaseModel) {
    if (!model.userId) {
      throw new MissingRequiredPropertyException('userId')
    }
    if (model.id)
      return this.commitWrite(this.collection.doc(model.id).set(classToPlain(model)))
    else
      return this.commitWrite(this.collection.add(classToPlain(model)))
  }

  crupdate(model: BaseModel) {
    return this.commitWrite(this.collection
        .doc(model.id)
        .set(classToPlain(model)))
  }

  commitRead(promise) {
    return promise.then((querySnapshot) => {
      if (!querySnapshot)
        return
      return this.collectDataFromSnapshot(querySnapshot)
    })
  }

  collectDataFromSnapshot(snapshot) {
    if (typeof snapshot.forEach !== 'function')
      return this.collectDataFromDocument(snapshot)
    return snapshot.docs.map(document => this.collectDataFromDocument(document))
  }

  update(id: string, data) {
    if (!('userId' in data)) {
      throw new MissingRequiredPropertyException('userId')
    }
    return this.commitWrite(this.collection.doc(id).update(data))
  }

  commitWrite(promise) {
    return promise
  }

  delete(id) {
    return this.collection.doc(id).delete()
  }

  async deleteAll() {
    const allEntries = await this.findAll()
    if (!allEntries)
      return Promise.resolve()
    return allEntries.forEach((data) => {
      this.collection.doc(data.id).delete()
    })
  }

  collectDataFromDocument(document) {
    if (!document.exists)
      return null
    return {
      ...document.data(),
      id: document.id,
    }
  }

  findByUserId(id) {
    return this.commitRead(this.collection.where('userId', '==', id).get())
  }

  async findById(id) {
    const data = await this.commitRead(this.collection.where('id', '==', id).get())
    if (typeof data.forEach !== 'undefined')
      return data[0]
    return data
  }

  async findDoc(id) {
    const reference = this.getReferenceById(id)
    const snapshot = await reference.get()
    return this.collectDataFromSnapshot(snapshot)
  }

  getReferenceById(id): firebaseAdmin.firestore.DocumentReference {
    return this.collection.doc(id)
  }

  async doesExists(id: string): Promise<Boolean> {
    return this.getReferenceById(id).get().then(doc => doc.exists)
  }

  deleteCollection() {
    // https://firebase.google.com/docs/firestore/manage-data/delete-data#delete_documents
    const query = this.collection
    return this.deleteQuery(query)
  }

  runTransaction(callback) {
    return this.database.firestore.runTransaction(async t => callback(t))
  }

  async runTransactionWithQuery(query, callback) {
    return this.runTransaction(async (t) => {
      const snapshot = await t.get(query)
      const data = this.collectDataFromSnapshot(snapshot)
      return callback(t, data)
    })
  }

  async updateWithTransaction(transaction, id, data) {
    const reference = await this.getReferenceById(id)
    return transaction.update(reference, data)
  }

  async deleteQuery(query, batchSize = 100): Promise<any> {
    query = query.limit(batchSize)
    return new Promise(((resolve, reject) => {
      this.deleteQueryBatch(this.database.firestore, query, batchSize, resolve, reject)
    }))
  }

  private deleteQueryBatch(db, query, batchSize, resolve, reject, counter = 0) {
    query.get().then((snapshot) => {
      console.log(JSON.stringify(snapshot, null, 2))
      // When there are no documents left, we are done
      if (snapshot.size === 0)
        return 0

      // Delete documents in a batch
      const batch = db.batch()
      snapshot.docs.forEach((doc) => {
        batch.delete(doc.ref)
      })

      return batch.commit().then(() => snapshot.size)
    }).then((numDeleted) => {
      if (numDeleted === 0) {
        console.log(`deleted ${counter} database objects.`)
        resolve()
        return
      }
      counter += numDeleted

      // Recurse on the next process tick, to avoid
      // exploding the stack.
      process.nextTick(() => {
        this.deleteQueryBatch(db, query, batchSize, resolve, reject, counter)
      })
    })
    .catch(reject)
  }
}
