import { Injectable } from '@nestjs/common'
import { UserRepository } from '../../../../user/user.repository'
import { EmailRecipient } from '../../email.model'
import { RetentionMail } from './reminder.email'
import { MessagePublisher } from '../../../../publisher/publisher.message'
import moment from 'moment'
import { RepresentationStatisticRepository } from '../../../../representationStatistic/representationStatistic.repository'
import { CurrentUserHelper } from '../../../../helper/helper.currentUserHelper'

@Injectable()
export class RetentionEmailService {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly representationStatisticRepository: RepresentationStatisticRepository,
    private readonly messagePublisher: MessagePublisher,
    private readonly currentUserHelper: CurrentUserHelper,
  ) {}

  public async sendRetentionEmails() {
    const users = await this.userRepository.findAllWithDatabase()
    const oneWeekAgo = moment().add(-7, 'days').startOf('day')
    for (const user of users) {
      if (!user.lastActiveTime
        || (!!user.lastActiveTime && moment(user.lastActiveTime).isSameOrBefore(oneWeekAgo))) {
        // Write retention mail to users who don't have a date of last activity or last activity is older than one week
        const newReviewsCount = await this.calculateNewReviewsCount(user)

        const mail = new RetentionMail().setNewReviewsCount(newReviewsCount.toString())
        mail.setRecipients([new EmailRecipient().setEmail(user.email)])

        const pipedriveBcc = this.currentUserHelper.createPipedriveBcc(user.pipedriveId)
        if (!!pipedriveBcc) mail.setBccRecipients([new EmailRecipient().setEmail(pipedriveBcc)])
        this.messagePublisher.push(mail, user)
      }
    }
  }

  private async calculateNewReviewsCount(user) {
    // Try to find stats from 7 days ago and today. If no stats are available return 'viele' as amount of new reviews
    const oneWeekAgoStart = moment().add(-7, 'days').startOf('day').toDate()
    const oneWeekAgoEnd = moment().add(-7, 'days').endOf('day').toDate()
    const statsOneWeekAgo = await this.representationStatisticRepository.findForUserBetweenDates(user.uid, oneWeekAgoStart, oneWeekAgoEnd)

    if (!statsOneWeekAgo || statsOneWeekAgo.length < 1) return 'viele'
    const countOneWeekAgo = statsOneWeekAgo.reduce((acc, stat) => acc + stat.totalReviews, 0)

    const todayStart = moment().add(-1, 'days').startOf('day').toDate()
    const todayEnd = moment().add(-1, 'days').endOf('day').toDate()
    const statsToday = await this.representationStatisticRepository.findForUserBetweenDates(user.uid, todayStart, todayEnd)

    if (!statsToday || statsToday.length < 1) return 'viele'
    const countToday = statsToday.reduce((acc, stat) => acc + stat.totalReviews, 0)
    const diff = countToday - countOneWeekAgo
    
    return diff > 0 ? diff : 'keine'
  }
}
