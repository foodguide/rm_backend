import { Module } from '@nestjs/common'
import { LocationStatisticRepository } from './locationStatistic.repository'
import { LocationStatistic } from './locationStatistic.model'

@Module({
  providers: [LocationStatisticRepository, LocationStatistic],
  exports: [LocationStatisticRepository, LocationStatistic],
})

export class LocationStatisticModule{}
