import { Injectable } from '@nestjs/common'
import { Representation } from '../location/location.model'

@Injectable()
export class InstagramHelper {

  static getReviewerName(review): string {
    return review.info ? review.info.name ? review.info.name : null : null
  }

  static getUsername(representation: Representation): string {
    return representation.info ? representation.info.name ? representation.info.name : null : null
  }

  static getReplyComment(allPossibleReviews, review) {
    const reviewerName = InstagramHelper.getReviewerName(review)
    if (!allPossibleReviews || !reviewerName)
      return null
    const responseComment = allPossibleReviews.find(comment =>
      comment.id !== review.id && comment.text.includes(`@${reviewerName}`))
    return responseComment ? responseComment : null
  }

  static isReplyComment(allPossibleReviews, possibleReview) {
    const mentionedUsers = InstagramHelper.getMentionedUsernames(possibleReview)
    const commentUsernames = InstagramHelper.getUsernamesFromComments(allPossibleReviews, [possibleReview])
    const intersection = new Set(
      mentionedUsers.filter(x => commentUsernames.includes(x)))
    return intersection.size > 0
  }

  static getUsernamesFromComments(allPossibleReviews, excludedReviews = []) {
    return allPossibleReviews
      .filter(possibleReview => !(possibleReview in excludedReviews))
      .map(possibleReview => possibleReview.reviewer.name)
  }

  static getMentionedUsernames(possibleReview) {
    return possibleReview.text
      .split(' ')
      .filter(word => word.startsWith('@'))
      .map(mentionedUsernameWithAtSign => mentionedUsernameWithAtSign.substr(1))
  }

  static getReply(replyComment) {
    return {
      created_time: replyComment.timestamp ? new Date(replyComment.timestamp) : null,
      from: {
        name: replyComment.username,
      },
      id: replyComment.id,
      message: replyComment.text,
    }
  }

  static arrange(reviews: any[], representation: Representation): any[] {
    const filteredOwnTopComments = InstagramHelper.removeOwnTopComments(reviews, representation)
    return InstagramHelper.findAndUseReplyComments(filteredOwnTopComments, representation)
  }

  // Top in terms of the hierarchy
  static removeOwnTopComments(reviews: any[], representation: Representation): any[] {
    const username = InstagramHelper.getUsername(representation)
    if (!username)
      return reviews
    return reviews.filter(review => review.reviewer.name !== username)
  }

  static findAndUseReplyComments(reviews: any[], representation: Representation): any[] {
    if (!reviews)
      return []
    const restructured = []
    reviews.forEach((review) => {
      if (!InstagramHelper.isReplyComment(reviews, review)) {
        const replyComment = InstagramHelper.getReplyComment(reviews, review)
        if (replyComment) {
          const reply = InstagramHelper.getReply(replyComment)
          if (!InstagramHelper.hasReply(review, reply)) {
            review.comments.push(reply)
          }
        }
        restructured.push(InstagramHelper.useOldestReplyFromUserAsResponse(review, representation))
      }
    })
    return restructured
  }

  static useOldestReplyFromUserAsResponse(review, representation: Representation) {
    const username = InstagramHelper.getUsername(representation)
    if (!review.comments || review.comments.length === 0 || !username)
      return review
    const commentsFromOwner = review.comments.filter(comment => comment.from.name === username)
    if (commentsFromOwner.length === 0)
      return review
    let oldest = commentsFromOwner.shift()
    while (commentsFromOwner.length > 0) {
      const next = commentsFromOwner.shift()
      if (next.created_time < oldest.created_time)
        oldest = next
    }
    if (!review.response)
      review.response = {}
    review.response.text = oldest.message
    review.response.id = `reply_${review.id}`
    review.response.created_time = oldest.created_time
    review.response.responder = oldest.from.name
    return review
  }

  static hasReply(review, reply) {
    if (!review.comments || review.comments.length === 0)
      return false
    return !!review.comments.find(comment => comment.id === reply.id)
  }
}
