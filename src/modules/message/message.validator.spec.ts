import 'reflect-metadata'
import { MessageValidator, NotSubscribedError } from './message.validator'
import { BaseMessageQueueItem, MessageSubscription } from './message.model'
import { MessageName } from './message.name'
import { MessageType } from './message.type'
import { User } from '../user/user.model'

const basicPayload = {
    Variables: {
        locationSummaries: [
            {
                representationSummaries: [
                    {
                        platform: 'Facebook',
                        answeredReviewAmount: 0,
                        answeredReviewAmountDelta: -15,
                        totalReviewAmount: 22,
                        totalReviewAmountDelta: 4,
                        goodReviewAmount: 0,
                        goodReviewAmountDelta: 0,
                        badReviewAmount: 0,
                        badReviewAmountDelta: 0,
                        averageRating: 5,
                        averageRatingDelta: 0,
                        averageRatingDeltaSign: 'positive'
                    },
                    {
                        platform: 'Yelp',
                        answeredReviewAmount: 0,
                        answeredReviewAmountDelta: 0,
                        totalReviewAmount: 4,
                        totalReviewAmountDelta: 1,
                        goodReviewAmount: 2,
                        goodReviewAmountDelta: 1,
                        badReviewAmount: 2,
                        badReviewAmountDelta: 0,
                        averageRating: 5,
                        averageRatingDelta: 0.16666666666666652,
                        averageRatingDeltaSign: 'positive'
                    }
                ],
                name: 'henssler go',
                goToReviewsUrl: 'https://respondo.app/#/main/reviews/Saq3Z22nmgwLyYA4cko4',
                goodReviewAmount: 2,
                goodReviewAmountDelta: 1,
                badReviewAmount: 2,
                badReviewAmountDelta: 0,
                averageRating: 5,
                averageRatingDelta: 0.08,
                averageRatingDeltaSign: 'positive',
                totalReviewAmount: 26,
                totalReviewAmountDelta: 5,
                answeredReviewAmount: 0,
                unansweredReviewAmount: 26,
                answeredReviewAmountDelta: -15
            }
        ],
        startDate: '11.03',
        endDate: '18.03',
        unsubscribe_url: 'https://respondo.app/#/S5CSIeE6zKfQazapATHsBF2TsDE2/summary'
    },
    TemplateErrorReporting: {
        Email: 'mattserudolph@gmail.com'
    },
    From: {
        Name: 'Respondo',
        Email: 'info@respondo.app'
    },
    TemplateLanguage: true,
    TemplateErrorDeliver: true,
    Subject: 'Die letzte Woche im Überblick',
    TemplateId: 606375,
    To: [
        {
            Email: 'matthias@thefoodguide.de'
        }
    ]
}

describe('MessageValidator', () => {

    let message: BaseMessageQueueItem
    let user: User
    let payload: any

    beforeEach(() => {
        // @ts-ignore
        message = new BaseMessageQueueItem()
        user = new User()
        message.subscription = new MessageSubscription()
    })

    describe('messageIsValid', () => {
        it('Valid Message should pass'), () => {
            message.subscription.name = MessageName.Basic
            message.subscription.type = MessageType.Email
            message.payload = {}
            expect(MessageValidator.messageIsValid(message)).toBeTruthy()
        }

        it('should validate the message as positive because it has a valid payload', () => {
            message.subscription.name = MessageName.Basic
            message.subscription.type = MessageType.Email
            message.payload = basicPayload
            expect(MessageValidator.messageIsValid(message)).toBeTruthy()
        })

        it('should throw an error because the payload has some null values', () => {
            message.subscription.name = MessageName.Basic
            message.subscription.type = MessageType.Email
            message.payload = basicPayload
            message.payload.Variables.locationSummaries[0].representationSummaries[0].platform = null
            expect(() => {
                MessageValidator.messageIsValid(message)
            }).toThrowError()
        })

        it('Invalid Message without MessageName should throw error', () => {
            message.subscription.type = MessageType.Email
            message.payload = {}

            expect(() => {
                MessageValidator.messageIsValid(message)
            }).toThrowError()
        })

        it('Invalid Message without MessageType should throw error', () => {
            message.subscription.name = MessageName.Basic
            message.payload = {}

            expect(() => {
                MessageValidator.messageIsValid(message)
            }).toThrowError()
        })

        it('Invalid Message without payload should throw error', () => {
            message.subscription.name = MessageName.Basic
            message.subscription.type = MessageType.Email

            expect(() => {
                MessageValidator.messageIsValid(message)
            }).toThrowError()
        })

    })

    describe('userHasSubscription', () => {

        it('Subscripted user should pass', () => {
                message.subscription.name = MessageName.Basic
                message.subscription.type = MessageType.Email
                message.payload = {}

                const subscription = new MessageSubscription()
                subscription.type = MessageType.Email
                subscription.name = MessageName.Basic
                user.subscriptions = [subscription]

                expect(MessageValidator.userHasSubscription(user, message.subscription)).toBeTruthy()
            }
        )

        it('Subscripted user should throw error', () => {
                message.subscription.name = MessageName.Newsletter
                message.subscription.type = MessageType.Email
                message.payload = {}
                user.subscriptions = []
                expect(
                    MessageValidator.userHasSubscription(user, message.subscription)
                ).toBeFalsy()
            }
        )
    })

})