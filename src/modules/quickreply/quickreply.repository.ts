import { Injectable } from '@nestjs/common'
import { BaseRepository } from '../database/database.baseRepository'

@Injectable()
export class QuickreplyRepository extends BaseRepository {

  managedCollection = 'quickReply'

}
