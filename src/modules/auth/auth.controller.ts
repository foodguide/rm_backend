import { Controller, Post, Body, UsePipes, ValidationPipe, UseGuards, Delete, HttpCode } from '@nestjs/common'
import { GoogleCodeRequestDto } from './auth.GoogleCodeRequestDto'
import { AuthService } from './auth.service'
import { AdminGuard } from './admin.guard'
import { UserIdDto } from './auth.model'

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {
  }

  @Post('google/code')
    @UsePipes(new ValidationPipe())
    async retrieveGoogleAccessToken(@Body() codeDto: GoogleCodeRequestDto) {
    const response = await this.authService.retrieveGoogleAccessToken(codeDto.code)
    return response
  }

  @Post('admin')
    @HttpCode(200)
    @UseGuards(AdminGuard)
    async setAdminClaim(@Body(new ValidationPipe()) dto: UserIdDto) {
    return this.authService.setAdminClaim(dto.userId)
  }

  @Delete('admin')
    @UseGuards(AdminGuard)
    async removeAdminClaim(@Body(new ValidationPipe()) dto: UserIdDto) {

  }

}
