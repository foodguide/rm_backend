import { MailjetApi } from '../../api/mailjet/api.mailjet'
import { ConfigService } from '../../../config/config.service'
import { Test } from '@nestjs/testing'
import { Email, EmailAttachment, EmailRecipient, EmailSender, InvalidEmailConfigurationError } from './email.model'
import { EmailService } from './email.service'
import { EmailTemplate } from './transactional/email.template'

describe('EmailService and MailJetApi', () => {
  let mailjetApi: MailjetApi
  let configService: ConfigService
  let emailService: EmailService
  beforeEach(async () => {

    process.env.NODE_ENV = 'test'
    const envFiles = process.env.NODE_ENV === 'development' || process.env.NODE_ENV === 'test' ? [`./base.env`, `./dev.env`] : `./base.env`
    const configModule = await Test.createTestingModule(
      {
        providers: [{
          provide: ConfigService,
          useValue: new ConfigService(envFiles),
        }],
      },
    ).compile()
    configService = configModule.get<ConfigService>(ConfigService)

    const testModule = await Test.createTestingModule(
      {
        providers: [
          {
            provide: MailjetApi,
            useValue: new MailjetApi(configService),
          },
        ],
      },
    ).compile()
    mailjetApi = testModule.get<MailjetApi>(MailjetApi)

    const emailModule = await Test.createTestingModule(
      {
        providers: [
          {
            provide: EmailService,
            useValue: new EmailService(mailjetApi),
          },
        ],
      },
    ).compile()
    emailService = await emailModule.get<EmailService>(EmailService)
  })

  describe('MailjetApi instance should be defined', () => {
    it('mailjetApi to defined', async () => {
      expect(mailjetApi).toBeDefined()
    })
  })

  describe('MailService instance should be defined', () => {
    it('mailjetService to defined', async () => {
      expect(emailService).toBeDefined()
    })
  })

  describe('getUser', () => {

    it('api is defined', async () => {
      expect(await mailjetApi.getUser()).toBeDefined()
    })

    it('user to be of type object', async () => {
      expect(await mailjetApi.getUser()).toBeInstanceOf(Object)
    })

  })

  describe('validation', async () => {

    it('if text/html part is used, defining template attributes should throw an InvalidEmailConfigurationError', async () => {
      const recipient = new EmailRecipient()
        .setEmail('finn@thefoodguide.de')
        .setName('Finn')

      const sender = new EmailSender()
        .setEmail('support@respondo.app')
        .setName('Testmail')

      const attachment = new EmailAttachment()
        .setBase64Content('VGhpcyBpcyB5b3VyIGF0dGFjaGVkIGZpbGUhISEK')
        .setFileName('test.txt')
        .setContentType('text/plain')

      const baseMail = new Email()
        .setSender(sender)
        .setRecipients([recipient])
        .setHtmlPart('TEST')
        .setTextPart('TEST')
        .setSubject('Test-Email')
        .setAttachments([attachment])

      expect(() => {
        baseMail.setTemplate(EmailTemplate.test)
      }).toThrowError(InvalidEmailConfigurationError)
      expect(() => {
        baseMail.setTemplateVariables({})
      }).toThrowError(InvalidEmailConfigurationError)
      expect(() => {
        baseMail.useTemplateLanguage(true)
      }).toThrowError(InvalidEmailConfigurationError)


    })

    it('if templating is usesd, defining text/html parts should throw an InvalidEmailConfigurationError', async () => {
      const recipient = new EmailRecipient()
        .setEmail('finn@thefoodguide.de')
        .setName('Finn')

      const sender = new EmailSender()
        .setEmail('support@respondo.app')
        .setName('Testmail')

      const attachment = new EmailAttachment()
        .setBase64Content('VGhpcyBpcyB5b3VyIGF0dGFjaGVkIGZpbGUhISEK')
        .setFileName('test.txt')
        .setContentType('text/plain')

      const baseMail = new Email()
        .setSender(sender)
        .setRecipients([recipient])
        .setTemplate(EmailTemplate.Test)
        .setTemplateVariables({})
        .useTemplateLanguage(true)
        .setSubject('Test-Email')
        .setAttachments([attachment])

      expect(() => {
        baseMail.setTextPart('TEST')
      }).toThrowError(InvalidEmailConfigurationError)
      expect(() => {
        baseMail.setHtmlPart('TEST')
      }).toThrowError(InvalidEmailConfigurationError)
    })
  })

  describe('send', async () => {
    it('send email works', async () => {
      const recipient = new EmailRecipient()
        .setEmail('finn@thefoodguide.de')
        .setName('Finn')

      const sender = new EmailSender()
        .setEmail('support@respondo.app')
        .setName('Testmail')

      const attachment = new EmailAttachment()
        .setBase64Content('VGhpcyBpcyB5b3VyIGF0dGFjaGVkIGZpbGUhISEK')
        .setFileName('test.txt')
        .setContentType('text/plain')

      const testMail = new Email()
        .setSender(sender)
        .setRecipients([recipient])
        .setSubject('Test-Email')
        .setAttachments([attachment])
        .setTemplate(EmailTemplate.Test)
        .useTemplateLanguage(true)
        .setTemplateVariables({
          headline: 'TEST',
          time: Date.now().toString(),
        })

      const response = await emailService.send(testMail.payload)
      expect(response).toBeDefined()
      expect(response.response.status).toEqual(200)
      for (const message of response.body.Messages) {
        for (const recipient of message.To) {
          expect(recipient.Email).toEqual(configService.get('MAILJET_TEST_RECIPIENT'))
        }
      }
    })
  })
})