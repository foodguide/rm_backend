import { MessageQueueItem } from './message.interface'
import { MessageType } from './message.type'
import { MessageName } from './message.name'
import { IsEnum, IsNotEmpty, IsString, ValidateNested } from 'class-validator'

export abstract class BaseMessageQueueItem implements MessageQueueItem {
  subscription: MessageSubscription
  payload: any

  getPayload() {
    return this.payload
  }

  getSubscription() {
    return this.subscription
  }
}

export class MessageSubscription {
  type: MessageType
  name: MessageName

  constructor(dto?: MessageSubscriptionDto) {
    if (!!dto) {
      this.type = dto.type
      this.name = dto.name
    }
  }

  setType(type: MessageType) {
    this.type = type
    return this
  }

  setName(name: MessageName) {
    this.name = name
    return this
  }
}

export class MessageSubscriptionDto {
  @IsNotEmpty()
  @IsEnum(MessageName)
  readonly name: MessageName
  @IsNotEmpty()
  @IsEnum(MessageType)
  readonly type: MessageType
}

export class UserIdDto {
  @IsNotEmpty()
  @IsString()
  readonly userId: string
}

export class UserPipedriveDto {
  @IsNotEmpty()
  @IsString()
  readonly userId: string

  @IsNotEmpty()
  @IsString()
  readonly pipedriveId: string
}

export class GuestMessageSubscriptionDto extends MessageSubscriptionDto{
  @IsNotEmpty()
  @IsString()
  readonly userId: string
}

export class MessageSubscriptionsDto {
  @ValidateNested({ each: true })
  subscriptions: MessageSubscriptionDto[]
}

export class GuestMessageSubscriptionsDto extends MessageSubscriptionsDto{
  @IsNotEmpty()
  @IsString()
  readonly userId: string
}
