import { ConfigService } from '../../../config/config.service'
import nodeMailjet from 'node-mailjet'
import { Injectable } from '@nestjs/common'

@Injectable()
export class MailjetApi {
  private client

  constructor(private readonly config: ConfigService) {
    this.client = nodeMailjet.connect(
            config.get('MAILJET_API_KEY'),
            config.get('MAILJET_SECRET'), {
              perform_api_call: true,
            },
        )
  }

  getUser() {
    return this.client.get('user')
  }

  send(emails) {
    if (process.env.NODE_ENV !== 'production') {
      for (const email of emails) {
        for (const index in email.To) {
          email.To[index] = { Email: this.config.get('MAILJET_TEST_RECIPIENT') }
        }
      }
    }
    return this.client.post('send', { version: 'v3.1' }).request({
      Messages: emails,
    })
  }
}
