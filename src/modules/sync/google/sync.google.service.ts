import { Inject, Injectable } from '@nestjs/common'
import { GoogleMyBusinessApi } from '../../api/google/api.googlemybusiness'
import { ReviewRepository } from '../../review/review.repository'
import { LocationRepository } from '../../location/location.repository'
import { GoogleAccessTokenProvider } from '../../api/google/api.googleaccesstokenprovider.model'
import { SyncBaseService } from '../sync.base.service'
import { Database } from '../../database/database.baseProvider'
import { ConfigService } from '../../../config/config.service'
import { SlackApi } from '../../api/slack/api.slack'
import { UserRepository } from '../../user/user.repository'
import { classToPlain } from 'class-transformer'

@Injectable()
export class SyncGoogleService extends SyncBaseService {
  constructor(
        private readonly googleClient: GoogleMyBusinessApi,
        protected config: ConfigService,
        protected locationRepository: LocationRepository,
        protected reviewRepository: ReviewRepository,
        protected userRepository: UserRepository,
        protected readonly slackApi: SlackApi,
        @Inject('Database') protected database: Database,
    ) {
    super(config, reviewRepository, locationRepository, userRepository, database, slackApi)
  }

  get platform() {
    return 'google'
  }

  protected async purgeOutdatedReviews(dbReviews, requestedReviews): Promise<any> {
    const exampleReview = dbReviews[0]
    const promises = []
    let purgingCount = 0
    dbReviews.forEach((dbReview) => {
      const gmbEquivalent = requestedReviews.find(gmbReview => gmbReview.reviewId === dbReview.externalId)
      if (!gmbEquivalent) {
                // This means it was deleted on the platform and we need to delete it in our database
        console.log(`Purging outdated review ${dbReview.id} for location ${exampleReview.locationId} for user ${exampleReview.userId}`)
        promises.push(this.reviewRepository.delete(dbReview.id))
        purgingCount += 1
      }
    })
    console.log(`Purged ${purgingCount} reviews from ${dbReviews.length} reviews for location ${exampleReview.locationId} for user ${exampleReview.userId}`)
    return await Promise.all(promises)
  }

  async requestPlatformReviewsForRepresentation(representation, location, userId, accessToken): Promise<any> {
    const user = await this.userRepository.findById(userId)
    const accessTokenProvider = new GoogleAccessTokenProvider(user, representation.credentialId, null)
    const reviews = await this.googleClient.getReviews({
      accessTokenProvider,
      account: accessToken.account.name,
      location: representation.externalId,
    })
    console.log(`Count: ${reviews.length}, requested google reviews for ${location.id} ${location.name} for user ${userId}`)
    return reviews ? reviews : []
  }

  async getCurrentReviewCount(representation, location, userId, accessToken) {
    const user = await this.userRepository.findById(userId)
    const accessTokenProvider = new GoogleAccessTokenProvider(user, representation.credentialId, null)
    try {
      return await this.googleClient.getReviewCount(accessTokenProvider, representation.externalId)
    } catch (error) {
      if (error.response.status === 404) {
        await this.setCredentialRequired(userId, representation.credentialId)
      }
      return null
    }
  }

  async getCurrentLatestReview(representation, location, userId, accessToken) {
    const user = await this.userRepository.findById(userId)
    const accessTokenProvider = new GoogleAccessTokenProvider(user, representation.credentialId, null)
    try {
      return await this.googleClient.getLatestReview(accessTokenProvider, representation.externalId)
    } catch (error) {
      if (error.response.status === 404) {
        await this.setCredentialRequired(userId, representation.credentialId)
      }
      return null
    }
  }

  private async setCredentialRequired(userId: string, credentialId: string) {
    const user = await this.userRepository.findById(userId)
    const index = user.credentials.findIndex(token => token.id === credentialId)
    user.credentials[index].status = 'accesstoken_required'
    return await this.userRepository.update(user.id, classToPlain(user))
  }

  protected shouldSkipPurging(representation): boolean {
    return this.googleClient.shouldSkipPurging.get(representation.externalId)
  }
}
