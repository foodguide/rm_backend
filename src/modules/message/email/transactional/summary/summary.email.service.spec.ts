import 'reflect-metadata'
import { SummaryEmailService } from './summary.email.service'
import { RepresentationStatistic } from '../../../../representationStatistic/representationStatistic.model'
import { Test } from '@nestjs/testing'
import { LocationSummary, RepresentationSummary, SummaryEmail } from './summary.email'
import { UserRepository } from '../../../../user/user.repository'
import { RepresentationStatisticRepository } from '../../../../representationStatistic/representationStatistic.repository'
import { LocationRepository } from '../../../../location/location.repository'
import { User } from '../../../../user/user.model'
import { ConfigService } from '../../../../../config/config.service'

describe('SummaryEmailService', () => {

    let summaryEmailService: SummaryEmailService
    let startStatisticOne: RepresentationStatistic
    let endStatisticOne: RepresentationStatistic
    let expectedRepresentationSummaryOne: RepresentationSummary
    let startStatisticTwo: RepresentationStatistic
    let endStatisticTwo: RepresentationStatistic
    let expectedRepresentationSummaryTwo: RepresentationSummary
    let expectedLocationSummary: LocationSummary
    let expectedSummaryEmail: SummaryEmail
    let userRepository: UserRepository
    let representationStatisticRepository: RepresentationStatisticRepository
    let locationRepository: LocationRepository

    beforeEach(async () => {

        startStatisticOne = new RepresentationStatistic()
        startStatisticOne.totalReviews = 15
        startStatisticOne.avgRating = 3
        startStatisticOne.date = new Date()
        startStatisticOne.ratingDistribution = [1, 2, 3, 4, 5]
        startStatisticOne.answered = 2

        endStatisticOne = new RepresentationStatistic()
        endStatisticOne.totalReviews = 20
        endStatisticOne.avgRating = 3.7
        endStatisticOne.date = new Date()
        endStatisticOne.ratingDistribution = [1, 2, 3, 10, 4]
        endStatisticOne.answered = 5

        expectedRepresentationSummaryOne = new RepresentationSummary().setPlatform('facebook')
        .setAnsweredReviewAmount(5)
        .setAnsweredReviewAmountDelta(3)
        .setTotalReviewAmount(20)
        .setTotalReviewAmountDelta(5)
        .setAverageRating(3.7)
        .setAverageRatingDelta(0.7000000000000002)
        .setBadReviewAmount(6)
        .setBadReviewAmountDelta(0)
        .setGoodReviewAmount(14)
        .setGoodReviewAmountDelta(5)

        startStatisticTwo = new RepresentationStatistic()
        startStatisticTwo.totalReviews = 15
        startStatisticTwo.avgRating = 4.5
        startStatisticTwo.date = new Date()
        startStatisticTwo.ratingDistribution = [1, 2, 3, 4, 5]
        startStatisticTwo.answered = 2

        endStatisticTwo = new RepresentationStatistic()
        endStatisticTwo.totalReviews = 16
        endStatisticTwo.avgRating = 4
        endStatisticTwo.date = new Date()
        endStatisticTwo.ratingDistribution = [1, 2, 3, 4, 6]
        endStatisticTwo.answered = 0

        expectedRepresentationSummaryTwo = new RepresentationSummary().setPlatform('tripadvisor')
        .setAnsweredReviewAmount(0)
        .setAnsweredReviewAmountDelta(-2)
        .setTotalReviewAmount(16)
        .setTotalReviewAmountDelta(1)
        .setAverageRating(4)
        .setAverageRatingDelta(-0.5)
        .setBadReviewAmount(6)
        .setBadReviewAmountDelta(0)
        .setGoodReviewAmount(10)
        .setGoodReviewAmountDelta(1)

        expectedLocationSummary = new LocationSummary().setName('TEST')
        .setRepresentationSummaries([expectedRepresentationSummaryOne, expectedRepresentationSummaryTwo])
        .setAnsweredReviewAmount(5)
        .setAnsweredReviewAmountDelta(0.5)
        .setTotalReviewAmount(36)
        .setTotalReviewAmountDelta(3)
        .setAverageRating(3.85)
        .setAverageRatingDelta(0.10000000000000009)
        .setBadReviewAmount(12)
        .setBadReviewAmountDelta(0)
        .setGoodReviewAmount(24)
        .setGoodReviewAmountDelta(3)

        expectedSummaryEmail = new SummaryEmail()
        .setStartDate(new Date())
        .setEndDate(new Date())
        .setLocationSummaries([expectedLocationSummary])


        const envFiles = [`./base.env`, `./dev.env`]
        const module = await Test.createTestingModule(
            {

                providers: [
                    SummaryEmailService,
                    {
                        provide: UserRepository,
                        useValue: {
                            findByUserId: () => {
                                {
                                    const user = new User()
                                    user.id = '1235'
                                    return user
                                }
                            }
                        }
                    },
                    {
                        provide: LocationRepository,
                        useValue: {
                            findByUserId: () => {
                            },
                            getRepresentationsByLocationId: () => {
                            }
                        }
                    },
                    {
                        provide: RepresentationStatisticRepository,
                        useValue: {
                            findForRepresentationByDate: () => {
                            }
                        }
                    },
                    {
                        provide: ConfigService,
                        useValue: new ConfigService(envFiles),
                    }
                ]
            }
        ).compile()

        summaryEmailService = module.get<SummaryEmailService>(SummaryEmailService)
        userRepository = module.get<UserRepository>(UserRepository)
        representationStatisticRepository = module.get<RepresentationStatisticRepository>(RepresentationStatisticRepository)
        locationRepository = module.get<LocationRepository>(LocationRepository)

    })

    describe('createSummaryEmail', () => {
        it('should be true', () => {
            expect(true).toBeTruthy()
        })
        //
        //   it('returns correct SummaryMail', async () => {
        //
        //     jest.spyOn(locationRepository, 'findByUserId').mockImplementationOnce(() => [{ name: 'TEST' }])
        //     jest.spyOn(locationRepository, 'getRepresentationsByLocationId').mockImplementationOnce(() => [
        //       { platform: 'facebook' },
        //       { platform: 'tripadvisor' },
        //     ])
        //
        //     jest.spyOn(representationStatisticRepository,
        //       'findForRepresentationByDate').mockImplementationOnce(() => [startStatisticOne],
        //     )
        //     jest.spyOn(representationStatisticRepository,
        //       'findForRepresentationByDate').mockImplementationOnce(() => [endStatisticOne],
        //     )
        //
        //     jest.spyOn(representationStatisticRepository,
        //       'findForRepresentationByDate').mockImplementationOnce(() => [startStatisticTwo],
        //     )
        //     jest.spyOn(representationStatisticRepository,
        //       'findForRepresentationByDate').mockImplementationOnce(() => [endStatisticTwo],
        //     )
        //     const summaryMail = await summaryEmailService.createSummaryEmail(await userRepository.findByUserId('12345'), new Date(), new Date())
        //     expect(summaryMail).toEqual(expectedSummaryEmail)
        //
        //   })
    })

})