import { Controller, Get, UseGuards } from '@nestjs/common'
import { StatusService } from './status.service'
import { AuthGuard } from '../auth/auth.guard'

@Controller('status')
export class StatusController {
  constructor(private readonly service: StatusService) {}

  @Get('check')
  async getStatus() {
    return await this.service.getStatus()
  }

  @Get('authorization')
  @UseGuards(AuthGuard)
  async getAuthorization() {
    return Promise.resolve()
  }
}
