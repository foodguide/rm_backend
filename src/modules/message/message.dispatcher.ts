import { EmailService } from './email/email.service'
import { Injectable } from '@nestjs/common'
import { MessageValidator } from './message.validator'
import { MessageType } from './message.type'

@Injectable()
export class MessageDispatcher {
  constructor(private readonly emailService: EmailService) {
  }

  async dispatch(message) {
    MessageValidator.messageIsValid(message)
    const payload = message.payload
    const type = message.subscription.type

    switch (type) {
      case MessageType.Email:
        return this.emailService.send(payload)
      default:
        throw new MessageDispatcherUnsupportedTypeError(type)
    }
  }
}

export class MessageDispatcherUnsupportedTypeError extends Error {
  constructor(type: string) {
    super(`Cannot send messages of type "${type}".`)
  }
}
