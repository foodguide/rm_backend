import { TransactionalMail } from '../transactional.mail'
import { MessageSubscription } from '../../../message.model'
import { MessageType } from '../../../message.type'
import { MessageName } from '../../../message.name'
import { emailTemplate } from '../email.template'

export class QuestionnaireEmail extends TransactionalMail {

  constructor(locale: string) {
    super()
    this.subscription = new MessageSubscription()
      .setType(MessageType.Email)
      .setName(MessageName.Basic)

    this.setTemplate(emailTemplate.questionnaire, locale)
  }

}
