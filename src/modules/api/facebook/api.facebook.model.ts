import { IsNotEmpty, IsString } from 'class-validator'

export class BasicFacebookGraphApiObject {
  @IsNotEmpty()
  @IsString()
  id: string
}

export class FacebookPage extends BasicFacebookGraphApiObject{
  @IsNotEmpty()
  @IsString()
  name: string
}

// The full list of error codes can be found here:
// https://developers.facebook.com/docs/graph-api/using-graph-api/error-handling
export const FacebookErrorCodes = {
  API_SESSION_EXPIRED: 102,
  TOO_MANY_REQUESTS: 4,
  API_UNKNOWN: 1,
  API_SERVICE_DOWN: 2,
  TOO_MANY_USER_REQUESTS: 17,
  NO_PERMISSION: 10,
  ACCESSTOKEN_EXPIRED: 190,
  APP_LIMIT_REACHED: 341,
  RULES_VIOLATION: 368,
  DOUBLED_POST: 506,
  POST_LINK: 1609005,
  GRAPH_METHOD_EXCEPTION: 100,
}

export const FacebookErrorSubcodes = {
  APP_NOT_INSTALLED: 458,
  USER_CHECKPOINT: 459,
  PASSWORD_CHANGED: 460,
  EXPIRED: 463,
  NOT_VERIFIED_USER: 464,
  UNVALID_ACCESSTOKEN: 467,
  UNVALID_SESSION: 492,
  NO_QUERY_PERMISSION: 33,
}
