import { Injectable } from '@nestjs/common'
import { BaseRepository } from '../database/database.baseRepository'

@Injectable()
export class DirectTaskRepository extends BaseRepository {
  managedCollection = 'directTask'

  findByReviewId(reviewId: string) {
    return this.commitRead(this.collection.where('reviewId', '==', reviewId).get())
  }

}
