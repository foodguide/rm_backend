import { Body, Controller, Put, UseGuards, ValidationPipe, Res, HttpStatus, Delete } from '@nestjs/common'
import { AdminGuard } from '../auth/admin.guard'
import { DeleteReviewsDto, UpdateReviewsDto } from './review.model'
import { Response } from 'express'
import { ReviewRepository } from './review.repository'
import { ReviewService } from './review.service'
import { AuthGuard } from '../auth/auth.guard'
import { BearerToken } from '../auth/auth.bearerdecorator'
import { DirectReplyDto, ReviewReplyDto } from './review.reply.dto'
import { CurrentUserHelper } from '../helper/helper.currentUserHelper'

/**
 * The controller for everything related to reviews. Path is '/review'.
 */
@Controller('review')
export class ReviewController {

  /**
   * The constructor function for the review controller
   *
   * @param {ReviewRepository} reviewRepository
   * @param {ReviewService} reviewService
   */
  constructor(
        private readonly reviewRepository: ReviewRepository,
        private readonly reviewService: ReviewService,
        private readonly currentUserHelper: CurrentUserHelper,
  ) {}

  /**
   * This function can be used to add some additional infos on a express response object when
   * the validation of a bulk time range has failed.
   *
   * @param {e.Response} res
   * @return {Response}
   */
  static setBadTimeRangeInfo(res: Response) {
    return res.status(HttpStatus.BAD_REQUEST).json({
      success: false,
      message: `No valid date string given. Neither for the field 'before' nor for the field 'after'.`,
    })
  }

  /**
   * This function sets a body for a request that should do something and has ended successfully.
   *
   * @param {e.Response} res
   * @return {Response}
   */
  static setSuccessBody(res: Response) {
    return res.json({
      success: true,
    })
  }

  /**
   * This is the endpoint for an admin if he wants to set the state for multiple reviews with one request.
   * @param {UpdateReviewsDto} dto
   * @param {e.Response} res
   * @return {Promise<void>}
   */
  @UseGuards(AdminGuard)
  @Put()
  async bulkUpdateReviews(@Body(new ValidationPipe()) dto: UpdateReviewsDto, @Res() res: Response) {
    if (!ReviewService.hasValidTimeInformation(dto)) {
      res = ReviewController.setBadTimeRangeInfo(res)
    } else {
      await this.reviewService.updateStateForAllBetweenDates(dto)
      res = ReviewController.setSuccessBody(res)
    }
    res.send()
  }

  /**
   * This is the endpoint for an admin if he wants to delete multiple reviews with one request.
   *
   * @param {DeleteReviewsDto} dto
   * @param {e.Response} res
   * @return {Promise<void>}
   */
  @UseGuards(AdminGuard)
  @Delete()
  async bulkDeleteReviews(@Body(new ValidationPipe()) dto: DeleteReviewsDto, @Res() res: Response) {
    if (!ReviewService.hasValidTimeInformation(dto)) {
      res = ReviewController.setBadTimeRangeInfo(res)
    } else {
      await this.reviewService.deleteAllBetweenDates(dto)
      res = ReviewController.setSuccessBody(res)
    }
    res.send()
  }

  @UseGuards(AuthGuard)
  @Put('reply')
  async reply(@BearerToken() token: string, @Body(new ValidationPipe()) dto: ReviewReplyDto) {
    const user = await this.currentUserHelper.getCurrentUser(token)
    return this.reviewService.sendReply(user, dto.reviewId, dto.text)
  }

  @UseGuards(AdminGuard)
  @Put('direct-reply')
  async directReply(@Body(new ValidationPipe()) dto: DirectReplyDto) {
    const user = await this.currentUserHelper.getUser(dto.userId)
    return this.reviewService.sendReply(user, dto.reviewId, dto.text)
  }
}
