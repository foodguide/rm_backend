import { Body, Controller, Get, Header, Put, UseGuards, ValidationPipe } from '@nestjs/common'
import { ReviewRepository } from '../review/review.repository'
import { UserRepository } from '../user/user.repository'
import { UserBackupRepository } from '../user/user-backup-repository.service'
import { LocationHelper } from '../helper/helper.locationHelper'
import { LocationRepository } from '../location/location.repository'
import { SyncStatisticService } from '../sync/statistic/sync.statistic.service'
import moment from 'moment'
import { AdminGuard } from '../auth/admin.guard'
import { FacebookHelper } from '../helper/helper.facebookHelper'
import { PaymentService } from '../payment/payment.service'
import { ArrayHelper } from '../helper/helper.arrayHelper'
import { RepresentationStatisticRepository } from '../representationStatistic/representationStatistic.repository'
import { NewStatisticsDto } from './health.model'
import { EventRepository } from '../event/event.repository'
import { SlackApi } from '../api/slack/api.slack'
import firebaseAdmin, { firestore } from 'firebase-admin'
import FieldValue = firebaseAdmin.firestore.FieldValue
import { QuickreplyRepository } from '../quickreply/quickreply.repository'
import { ReviewService } from '../review/review.service'
import { CurrentUserHelper } from '../helper/helper.currentUserHelper'
import { HealthService } from './health.service'

@Controller('health')
export class HealthController {
  constructor(
        private readonly userRepository: UserRepository,
        private readonly usersRepository: UserBackupRepository,
        private readonly reviewRepository: ReviewRepository,
        private readonly locationHelper: LocationHelper,
        private readonly locationRepository: LocationRepository,
        private readonly statisticsService: SyncStatisticService,
        private readonly paymentService: PaymentService,
        private readonly reviewService: ReviewService,
        private readonly statsRepository: RepresentationStatisticRepository,
        private readonly eventRepository: EventRepository,
        private readonly quickreplyRepository: QuickreplyRepository,
        private readonly slackApi: SlackApi,
        private readonly currentUserHelper: CurrentUserHelper,
        private readonly healthService: HealthService,
    ) {
  }

  @Get('review/clear')
    /**
     * This endpoint is intended to remove all reviews which fulfill one of these criteria:
     * - No representationId
     * - No userId
     * - UserId belongs to no user
     */
    async clearInvalidReviews() {
    const reviews = await this.reviewRepository.findAll()
    const users = await this.userRepository.findAllWithDatabase()
    const userIds = users.map(user => user.uid)

    const reviewsToPurge = reviews.filter(review =>
          !review.representationId ||
          !review.userId ||
          !userIds.includes(review.userId),
        )
    reviewsToPurge.forEach((review) => {
      this.reviewRepository.delete(review.id)
    })
  }

  @Get('representation/clean')
    /**
     * This endpoint is intended to enrich all representations with their missing locationId.
     */
    async cleanRepresentations() {
    this.locationHelper.forAllUsers(async (locations, userId) => {
      for (const location of locations) {
        const representations = await this.locationHelper.getPlatformRepresentationsForLocation(location)
        for (const representation of representations) {
          if (!representation.locationId) {
            representation.locationId = location.id
            this.locationRepository.updateRepresentation(location.id, representation)
          }
        }
      }
    })
  }

  @UseGuards(AdminGuard)
  @Get('representation/clear-external-review-count')
  async clearExternalReviewCount() {
    await this.locationHelper.forAllUsers(async (locations, userId) => {
      await locations.map(async (location) => {
        if (location.id === 'Jhd4Iye9ekqyZLKdhg22') return
        const representations = await this.locationHelper.getPlatformRepresentationsForLocation(location)
        const tripRepresentation = representations.find(rep => rep.platform === 'tripadvisor')
        if (tripRepresentation && tripRepresentation.externalReviewCount) {
          tripRepresentation.externalReviewCount = FieldValue.delete()
          this.locationRepository.updateRepresentation(location.id, tripRepresentation, false)
        }
      })
    })
  }

  @Get('statistics/new')
    async createNewStatistics() {
    this.locationHelper.forAllUsers(async (locations, userId) => {
      for (const location of locations) {
        const representations = await this.locationHelper.getPlatformRepresentationsForLocation(location)
        for (const representation of representations) {
          this.statisticsService.calculateStatisticsForRepresentation(representation, userId)
        }
      }
    })
  }

  /**
   * Endpoint to create new statistic values after given date.
   * @param dto
   */
  @Get('statistics/new-from-date')
  async createNewStatisticsFromDate(@Body(new ValidationPipe()) dto: NewStatisticsDto) {
    this.locationHelper.forAllUsers(async (locations, userId) => {
      for (const location of locations) {
        const representations = await this.locationHelper.getPlatformRepresentationsForLocation(location)
        for (const representation of representations) {
          this.statisticsService.calculateStatisticsForRepresentation(representation, userId, moment(dto.from))
        }
      }
    })
  }

    // Some useful endpoints for debugging.
    // USE WITH CAUTION DUE TO HIGH AMOUNT OF DATA WRITES/READS.

  @Get('statistics/calculate')
    async calculateStatisticWrites() {
    const statsData = []
    await this.locationHelper.forAllUsers(async (locations, userId) => {
      for (const location of locations) {
        const representations = await this.locationHelper.getPlatformRepresentationsForLocation(location)
        for (const representation of representations) {
          const representationCreationDate = moment(new Date(representation.created))
          const daysInBetween = moment().diff(representationCreationDate, 'days')
          const data = {
            id: representation.id,
            date: representationCreationDate.format('MMM Do YY'),
            days: daysInBetween,
          }
          statsData.push(data)
        }
      }
    })
    statsData.push({
      locationCount: statsData.length,
      count: statsData.map(data => data.days).reduce((count, days) => count + days),
    })
    return statsData
  }

  @Get('user/backup')
    async backupDatabaseUsers() {
    return this.writeUserBackup()
  }

  async writeUserBackup() {
    await this.usersRepository.deleteCollection()
    const dbUsers = await this.userRepository.findAllDatabaseUser()
    dbUsers.forEach((user) => {
      if (!user.userId) user.userId = user.id
      this.usersRepository.create(user)
    })
  }

  @UseGuards(AdminGuard)
  @Get('user/restore-from-backup')
  async restoreDatabaseUsersFromBackup() {
    const backupUsers = await this.usersRepository.findAll()
    const dbUsers = await this.userRepository.findAllDatabaseUser()
    return Promise.all(dbUsers.map((user) => {
      const backupUser = backupUsers.find(({ userId }) => user.userId === userId)
      const concatenatedUser = {
        ...backupUser,
        ...user,
      }
      if (!concatenatedUser.credentials) concatenatedUser.credentials = []
      if (concatenatedUser.credentials.length === 0 && backupUser.credentials) concatenatedUser.credentials = backupUser.credentials

      if (!concatenatedUser.subscriptions) concatenatedUser.subscriptions = []
      else if (concatenatedUser.subscriptions.length === 0 && backupUser.subscriptions) concatenatedUser.subscriptions = backupUser.subscriptions

      return this.userRepository.update(concatenatedUser.userId, concatenatedUser)
    }))

  }

    /**
     * Check if user with multiple accessTokens of one platform exist.
     */
  @Get('token/lookup')
    async lookupDatabaseUserTokens() {
    const dbUsers = await this.userRepository.findAllDatabaseUser()
    dbUsers.forEach((user) => {
      const tokens = user.accessTokens.map(token => token.platform)
      const facebookTokens = user.accessTokens.filter((token => token.platform === 'facebook'))
      const googleTokens = user.accessTokens.filter((token => token.platform === 'google'))

      if (facebookTokens.length > 1 || googleTokens.length > 1)
        console.log(user.userId + ' : ' + JSON.stringify(tokens))
    })
  }

  /**
   * Get all users without pipedrive id
   */
  @Get('pipedrive-check')
  async pipedriveCheck() {
    const users = await this.userRepository.findAllWithDatabase()

    const result = []
    users.forEach((user) => {

      if (!user.pipedriveId) {
        result.push({
          id: user.userId,
          email: user.email,
        })
      }
    })

    return result
  }

  @UseGuards(AdminGuard)
    @Get('review/answered-to-seen')
    async markAnsweredReviewsAsSeen() {
    const query = this.reviewRepository.collection
            .where('response.text', '>=', '')
            .where('state', '==', 'unseen')
    return this.reviewRepository.database.firestore.runTransaction((t) => {
      return t.get(query).then((snapshot) => {
        const reviews = this.reviewRepository.collectDataFromSnapshot(snapshot)
        return Promise.all(reviews.map((review) => {
          return this.reviewRepository.update(review.id, {
            state: 'seen',
          })
        }))
      })
    }).then(() => {
      return {
        success: true,
      }
    })
  }

  @UseGuards(AdminGuard)
    @Get('review/add-facebook-answer-comment')
    async addResponseFromComments() {
    const query = this.reviewRepository.collection
            .where('platform', '==', 'facebook')
    let representationsCache = {}
    return this.reviewRepository.runTransactionWithQuery(query, async (t, data) => {
      const reviews = data.filter(review => !!review.comments && review.comments.length > 0)
                .filter(review => !review.response)
      await Promise.all(reviews.map(async (review) => {
        const { updatedCache, representation } = await this.getRepresentation(representationsCache, review)
        representationsCache = updatedCache
        if (!representation)
          return Promise.resolve()
        const response = FacebookHelper.findResponseInComments(review, representation)
        if (!response)
          return Promise.resolve()
        return this.reviewRepository.update(review.id, {
          response,
          state: 'seen',
        })
      }))
      return {
        success: true,
      }
    })
  }

  @Get('review/tripadvisor-response-url')
  async correctTripadvisorResponseUrl() {
    const query = this.reviewRepository.collection
      .where('platform', '==', 'tripadvisor')
    return this.reviewRepository.runTransactionWithQuery(query, async (t, data) => {
      await Promise.all(data.map(async (review) => {
        const urlParts = review.url.split('?')
        if (urlParts.length >= 2)
          return Promise.resolve()
        const url = `${review.url}?review=${review.externalId}`
        return this.reviewRepository.update(review.id, {
          url,
        })
      }))
      return {
        success: true,
      }
    })
  }

  @UseGuards(AdminGuard)
    @Get('user/accesstoken-to-credential')
    async renameAccessTokensToCredentials() {
    return this.writeUserBackup().then(() => {
      const query = this.userRepository.collection
      return this.userRepository.runTransactionWithQuery(query, async (t, data) => {
        await Promise.all(data.map(async (user) => {
          return this.userRepository.renameAccessTokensToCredentials(user)
        }))
        return {
          success: true,
        }
      })
    })
  }

  async getRepresentation(representationsCache, review) {
    let representation = representationsCache[review.representationId]
    if (!representation) {
      representation = await (this.locationRepository.getRepresentationById(review.locationId, review.representationId))
      representationsCache[review.representationId] = representation
    }
    return {
      representation,
      updatedCache: representationsCache,
    }
  }

  @Get('active-subscriptions-count')
    async countActiveSubscriptions() {
    const userIds = await this.paymentService.getCustomerIdsWithActiveSubscriptions()
    const locations = await Promise.all(userIds.map(id =>
            this.locationRepository.findByUserId(id)),
        ).then(results => ArrayHelper.flatMap(results))
    const representations = await Promise.all(locations.map(({ id }) =>
            this.locationRepository.getRepresentationsByLocationId(id)),
        ).then(results => ArrayHelper.flatMap(results))
    const crawlerRepresentations = representations.filter(r => r.type === 'crawler')
    const data = {
      user_count: userIds.length,
      location_count: locations.length,
      representation_count: representations.length,
      crawler_representation_count: crawlerRepresentations.length,
    }
    this.slackApi.sendMessageToChannel(
      'respondo',
      'Die aktuellen Statistiken für zahlende Nutzer:',
      [this.slackApi.createAttachement('Übersicht', data)])
    return data
  }

  @Get('active-user-count')
  async countActiveUsers() {
    let activeUserCount = 0
    const users = await this.userRepository.findAllWithDatabase()
    users.forEach((user) => {
      if (!!user.lastActiveTime && moment(user.lastActiveTime).isSame(moment(), 'month')) {
        activeUserCount += 1
      }
    })
    this.slackApi.sendMessageToChannel(
      'respondo',
      `Anzahl aktiver Nutzer in diesem Monat: ${activeUserCount}`,
      null)
  }

  @Get('location-count')
  async countLocations() {
    let locationCount = 0
    await this.locationHelper.forAllUsers(async (locations, userId) => {
      locationCount += locations.length
    })
    this.slackApi.sendMessageToChannel(
      'respondo',
      `Anzahl Restaurants in Respondo: ${locationCount}`,
      null)
  }

  @Get('metro-user-statistics')
  @Header('content-type', 'text/comma-separated-values')
  async getMetroUserStatistics() {
    const users = await this.userRepository.findAllWithDatabase()
    const metroUsers = users.filter(user => user.isMetro)
    const result = []

    for (const user of metroUsers) {

      // Locations
      const locationSummary = []
      let representationCount = 0
      const locations = await this.locationHelper.getLocationsByUserId(user.uid)
      for (const location of locations) {
        const representations = await this.locationHelper.getPlatformRepresentationsForLocation(location)
        representationCount = representationCount + representations.length
        locationSummary.push({
          name: location.name,
          representations: representations.map(r => r.platform).join(' '),
        })
      }

      // Events
      const events = await this.eventRepository.findLastAmountForUser(user.uid, 10)
      const eventNames = events.map(event => event.name).join(' ')

      moment.locale('en')

      // Result
      result.push({
        representationCount,
        email: user.email,
        creationDate: moment(user.metadata.creationTime).format('LLL'),
        lastUsage: user.lastActiveTime ? moment(user.lastActiveTime).format('LLL') : 'n/a',
        locationCount: locations.length,
        locations: locationSummary,
        events: eventNames,
      })
    }

    let csvResult = 'Mail address;Created;Last Login;# Restaurants;# Connections; Restaurants (Connections);Last Activity\n'
    for (const r of result) {
      const locationString = r.locations.map(l => `${l.name} (${l.representations})`).join(' ')
      csvResult = csvResult + `${r.email};${r.creationDate};${r.lastUsage};${r.locationCount};${r.representationCount};${locationString};${r.events}\n`
    }

    return csvResult
  }

  @Get('check-stats')
  async checkStats() {
    await this.locationHelper.forAllUsers(async (locations, userId) => {
      for (const location of locations) {
        const representations = await this.locationHelper.getPlatformRepresentationsForLocation(location)
        for (const representation of representations) {
          const stats = await this.statsRepository.findByRepresentationId(representation.id)
          if (stats.length !== 1) {
            console.log(`Missing stats for: ${representation.id}, ${userId}`)
          }
        }
      }
    })
  }

  @Get('upgradeQuickreplies')
  async upgradeQuickreplies() {
    const allQuickreplies = await this.quickreplyRepository.findAll()
    allQuickreplies.forEach((reply) => {
      reply.texts = [reply.text, '', '']
      delete reply.text
      this.quickreplyRepository.update(reply.id, {
        ...{ text: firestore.FieldValue.delete() },
        ... reply,
      })
    })
  }

  @Get('correct-google-review-ids')
  async correctGoogleReviewIds() {
    this.healthService.correctGoogleReviewIds().then(() => {
      console.log('Google Review Id\'s migration successful')
    })
  }

  @Get('upgrade-review-without-texts')
  async upgradeReviewsWithoutText() {
    const reviewsWithoutTexts = await this.reviewRepository.findAllWithText('Zu diesem Review wurde kein Text hinterlegt.')
    reviewsWithoutTexts.forEach((review) => {
      const newReview = { ...review }
      newReview.text = null
      this.reviewRepository.update(review.id, newReview)
    })
  }

  @Put('resend-google-replies')
  async resendGoogleReplies() {
    const timestampOctober21 = 1571616000000
    const events = await this.eventRepository.findSinceTimestamp(timestampOctober21)
    const sendReplyEventsGoogle = events.filter(event => (event.name === 'SendReplyStart' && event.data.reviewId.startsWith('go_')))
    await sendReplyEventsGoogle.map(async (event) => {
      const user = await this.currentUserHelper.getUser(event.userId)
      await this.reviewService.sendReply(user, event.data.reviewId, event.data.text)
    })
  }

  @Get('check-platform-login')
  async checkPlatformLogin() {
    await this.healthService.pushLoginCheckTasks()
  }
}
