import { Module } from '@nestjs/common'
import { EmailService } from './email/email.service'
import { EmailController } from './email/email.controller'
import { PublisherModule } from '../publisher/publisher.module'
import { ApiModule } from '../api/api.module'
import { MessageDispatcher } from './message.dispatcher'
import { SummaryEmailService } from './email/transactional/summary/summary.email.service'
import { RepresentationStatisticModule } from '../representationStatistic/representationStatistic.module'
import { RetentionEmailService } from './email/transactional/reminder/retention.email.service'
import { NewsletterEmailService } from './email/transactional/newsletter/newsletter.email.service'

// @ts-ignore
@Module({
  imports: [PublisherModule, ApiModule, RepresentationStatisticModule],
  providers: [EmailService, MessageDispatcher, SummaryEmailService, RetentionEmailService, NewsletterEmailService],
  controllers: [EmailController],
  exports: [MessageDispatcher, SummaryEmailService, RetentionEmailService, NewsletterEmailService],

})

export class MessageModule {
}
