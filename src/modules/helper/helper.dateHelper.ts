import { Injectable } from '@nestjs/common'

@Injectable()
export class DateHelper{
  static getDateFromInstagramTimestamp = timestamp => new Date(parseInt(timestamp, 10) * 1000)

  static getDateFromTimestamp = (timestamp) => {
    if (typeof timestamp === 'string')
      return new Date(timestamp)
    else return timestamp.toDate()
  }
  static isExpired(date: Date): boolean {
    return date < new Date()
  }

  static getDifference(time1: Date, time2 = new Date()) {
    return Math.abs((time2.getTime() - time1.getTime()) / 1000)
  }
}
