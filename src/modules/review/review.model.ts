import { BaseModel } from '../database/database.baseModel'
import { Transform, Type } from 'class-transformer'
import { IsDateString, IsEnum, IsNotEmpty, IsOptional, IsString } from 'class-validator'

export class ReviewResponse {
  text: string
  @Type(() => Date)
    @Transform((value, obj) => obj.updated.toDate(), { toClassOnly: true })
    updated: Date
  id: string
  responder: string
}

export class ReviewerModel {
  id: string
  name: string
}

export class Review extends BaseModel {
  externalId: string
  locationId: string
  representationId: string
  // The @Type annotation will fail but is needed to start the @Transform annotation
  // The json object provides a firestore timestamp object and we access it in our
  // custom transform annotation (the obj). There we can call the toDate() method
  // from the timestamp class
  @Type(() => Date)
  @Transform((value, obj) => obj.created.toDate(), { toClassOnly: true })
  created: Date
  @Type(() => Date)
  @Transform((value, obj) => obj.updated.toDate(), { toClassOnly: true })
  updated: Date
  url: string
  @Type(() => ReviewerModel)
  reviewer: ReviewerModel
  @Type(() => ReviewResponse)
  response: ReviewResponse
  post: object
  rating: number | null
  text: string
  platform: string
  comments: string[]
  state: string
  externalURI: string
}

enum ReviewState {
    SEEN = 'seen',
    UNSEEN = 'unseen',
}

export class UpdateReviewsDto implements BulkTimeRange {
  @IsNotEmpty()
  @IsString()
  readonly userId: string
  @IsNotEmpty()
  @IsEnum(ReviewState)
  readonly state: ReviewState
  @IsOptional()
  @IsDateString()
  readonly before: string
  @IsOptional()
  @IsDateString()
  readonly after: string
}

export class DeleteReviewsDto implements BulkTimeRange{
  @IsNotEmpty()
  @IsString()
  readonly userId: string
  @IsOptional()
  @IsDateString()
  readonly before: string
  @IsOptional()
  @IsDateString()
  readonly after: string
}

export interface BulkTimeRange{
  readonly before: string
  readonly after: string
}


