export class StringHelper {
  static capitalize(text: string): string {
    if (!text)
      return text
    return text[0].toUpperCase() + text.substr(1).toLowerCase()
  }
}
