import { Module } from '@nestjs/common'
import { SlackErrorFilter } from './slack.error.filter'
import { ApiModule } from '../api/api.module'
import { SlackErrorListener } from './slack.error.listener'

@Module({
  imports: [ApiModule],
  providers: [SlackErrorFilter, SlackErrorListener],
  exports: [SlackErrorFilter, SlackErrorListener],
})

export class ErrorModule {
}
