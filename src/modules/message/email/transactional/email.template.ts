export const emailTemplate = {
  test: {
    de: 591405,
  },
  summary: {
    en: 1040940,
    de: 606375,
  },
  welcome: {
    en: 1042228,
    de: 710087,
  },
  questionnaire: {
    de: 951891,
  },
  retention: {
    de: 728879,
  } ,
}
