import { Injectable } from '@nestjs/common'
import { UserRepository } from '../user/user.repository'
import { LocationRepository } from '../location/location.repository'
import { Location } from '../location/location.model'
import { plainToClass } from 'class-transformer'
import { PaymentService } from '../payment/payment.service'

@Injectable()
export class LocationHelper {
  constructor(private readonly userRepository: UserRepository,
              private readonly locationRepository: LocationRepository,
              private readonly paymentService: PaymentService) {}

  public async forAllUsers(onLocations: (locations: Location[], userId: string) => {}, promises = [], nextPageToken?) {
    const userIds = await this.getUserIds(nextPageToken)
    for (const userId of userIds.userIds) {
      const locations = await this.getLocationsByUserId(userId)
      if (!locations)
        continue
      promises.push(onLocations(locations, userId))
    }
    if (userIds.pageToken)
      await this.forAllUsers(onLocations, promises, userIds.pageToken)
    else
      await Promise.all(promises)
  }

  async getLocationsByUserId(userId): Promise<Location[]> {
    const plainLocations = await this.locationRepository.findByUserId(userId)
    if (!plainLocations || plainLocations.length === 0) {
      console.log('no locations found for user ' + userId)
      return null
    }
    const locations: Location[] = await plainToClass(Location, plainLocations)
    return locations
  }

  async getUserIds(nextPageToken?) {
    const userResult = await this.userRepository.findAll(nextPageToken)
    const users = userResult.users
    return {
      userIds: users.map(user => user.uid),
      pageToken: userResult.pageToken,
    }
  }

  async getPlatformRepresentationsForLocation(location) {
    return await this.locationRepository.getRepresentationsByLocationId(location.id)
  }

  public async findAllUsersWithActiveSubscriptions() {
    const userIds = await this.paymentService.getCustomerIdsWithActiveSubscriptions()
    const dbUsers = await this.userRepository.findAllWithDatabase()
    return dbUsers.filter(user => userIds.indexOf(user.id) >= 0)
  }
}
