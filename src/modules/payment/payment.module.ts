import { forwardRef, Module } from '@nestjs/common'
import { ApiModule } from '../api/api.module'
import { PaymentService } from './payment.service'
import { PaymentController } from './payment.controller'
import { AuthModule } from '../auth/auth.module'
import { UserModule } from '../user/user.module'

@Module({
  imports: [ApiModule, AuthModule, forwardRef(() => UserModule)],
  providers: [PaymentService],
  exports: [PaymentService],
  controllers: [PaymentController],
})

export class PaymentModule {}
