import { Inject, Injectable } from '@nestjs/common'
import { BaseWorker } from './worker.base'
import { Database } from '../database/database.baseProvider'
import { SlackApi } from '../api/slack/api.slack'
import { ConfigService } from '../../config/config.service'

@Injectable()
export class ErrorWorker extends BaseWorker {

  constructor(
    @Inject('Database') private readonly database: Database,
    private readonly slackApi: SlackApi,
    protected config: ConfigService) {
    super(database, config)
  }

  get queueReference() {
    return 'errorQueue'
  }

  process(data, progress, resolve, reject) {
    this.slackApi.sendMessageToChannel(
      'review-manager-errors',
      data.message,
      null,
    )
    resolve()
  }


}
