import flat from 'flat'
import { SlackApi } from '../api/slack/api.slack'
import { Injectable, Logger } from '@nestjs/common'

@Injectable()
export class SlackErrorListener {
  constructor(
    private readonly slackApi: SlackApi,
  ) {
    this.initializeListeners()
  }

  initializeListeners() {
    process.on('unhandledRejection', (event) => this.handleRejection(event))
  }

  private handleRejection(event) {
    if (!(process.env.NODE_ENV === 'testing' || process.env.NODE_ENV === 'development')) {
      const attachment = {
        source: 'Unhandled Rejection',
        userId: event.userId ? event.userId : null,
        reason: typeof event.reason === 'string' ? event.reason : null,
        stack: event.stack ? event.stack : null,
      }

      if (typeof event === 'object') {
        const flattenedReason = flat(event)
        for (const key in flattenedReason) {
          attachment[key] = flattenedReason[key]
        }
      }

      this.slackApi.logErrors([
        this.slackApi.createErrorAttachement(
          attachment,
        ),
      ])
    }
    Logger.error('Unhandled Rejection', event.stack ? event.stack : null)
  }
}
