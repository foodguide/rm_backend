import { Inject, Injectable } from '@nestjs/common'
import { LocationRepository } from '../../location/location.repository'
import { SyncBaseService } from '../sync.base.service'
import { Database } from '../../database/database.baseProvider'
import { SlackApi } from '../../api/slack/api.slack'
import { classToPlain } from 'class-transformer'
import { UserRepository } from '../../user/user.repository'

@Injectable()
export class SyncCrawlerService extends SyncBaseService {

  private yelpCrawlerQueue
  private crawlerQueue

  constructor(protected readonly slackApi: SlackApi,
              protected locationRepository: LocationRepository,
              protected userRepository: UserRepository,
              @Inject('Database') database: Database,
  ) {
    super(null, null, locationRepository, userRepository, database, slackApi)
    this.crawlerQueue = this.database.firebase.ref('crawlerQueue/tasks')
    this.yelpCrawlerQueue = this.database.firebase.ref('yelpCrawlerQueue/tasks')
    this.initListeners()
  }

  private static getFallbackMessage() {
    return 'Old page error. Fallback needed.'
  }

  private get queues() {
    return [
      this.crawlerQueue,
      this.yelpCrawlerQueue,
    ]
  }

  initListeners() {
    this.queues.forEach((queue) => {
      queue.on('child_changed', snapshot => this.onCrawlerTaskChanged(snapshot))
    })
  }

  onCrawlerTaskChanged(snapshot) {
    const task = snapshot.val()
    if (task.state === TaskState.DONE) {
      this.queueStatisticUpdate(task).then(() => {
        console.log(`Finished crawler sync for location ${task.locationId}`)
      })
    } else if (task.platform === 'yelp' && task._state === 'error' && task._error_details.error === SyncCrawlerService.getFallbackMessage()) {
      this.moveTaskToFallbackQueue(task)
    }
  }

  private moveTaskToFallbackQueue(task) {
    task.state = TaskState.WAITING
    this.removeTask(this.yelpCrawlerQueue, task.id).then(() => {
      this.crawlerQueue.child(task.id).set(task).then(() => {
        console.log(`Fallback queueing. Queued crawler task for location ${task.locationId}`)
      })
    })
  }

  private async removeTask(queue, id) {
    return queue.child(id).remove()
  }

  async clearCrawlerQueue() {
    await Promise.all(this.queues.map(queue => queue.remove()))
  }

  async queueStatisticUpdate(task) {
    const representation = await this.locationRepository.getRepresentationById(task.locationId, task.id)
    if (representation) {
      super.queueStatisticUpdate({
        representation,
        locationId: task.locationId,
        userId: task.userId,
      })
    }
  }

  async queueCrawlerTask(options) {
    const crawlerTask = new CrawlerTask(options)
    const queue = this.getQueue(crawlerTask)
    try {
      const task = await new Promise((resolve) => {
        queue.child(crawlerTask.id).once('value', (snapshot) => {
          resolve(snapshot.val())
        })
      }) as any
      if (!!task && task.state === TaskState.RUNNING) {
        console.log(`Crawler task for location ${options.locationId} is already running`)
      } else {
        queue.child(crawlerTask.id).set(classToPlain(crawlerTask)).then(() => {
          console.log(`Queued crawler task for location ${options.locationId}`)
        })
      }
    } catch (e) {
      console.error(e)
    }
  }

  async retryCrawlerTasks(platforms: string[]) {
    const promises = []
    if (platforms.some(plat => plat === 'yelp')) promises.push(this.retryYelpCrawlerTasks())
  }

  getCrawlerTasks(snapshot) {
    const data = snapshot.val()
    const tasks = []
    Reflect.ownKeys(data).forEach((taskId) => {
      tasks.push(data[taskId])
    })
    return tasks
  }

  async getYelpCrawlerTasks() {
    const ref = this.database.firebase.ref('yelpCrawlerQueue/tasks')
    return new Promise((resolve) => {
      ref.once('value', (snapshot) => {
        const tasks = this.getCrawlerTasks(snapshot)
        resolve(tasks)
      })
    })
  }

  async getFailedYelpCrawlerTasks() {
    const tasks: any[] = await this.getYelpCrawlerTasks()
    return tasks.filter(task => task._state === 'error')
  }

  async retryYelpCrawlerTask(task) {
    await this.removeTask(this.yelpCrawlerQueue, task.id)
    // Picking only the necessary data for recreation
    return this.queueCrawlerTask({
      id: task.id,
      platform: task.platform,
      locationId: task.locationId,
      userId: task.userId,
      startUrl: task.startUrl,
      priority: task.priority,
    })
  }

  async retryYelpCrawlerTasks() {
    const tasks = await this.getFailedYelpCrawlerTasks()
    return Promise.all(tasks.map(task => this.retryYelpCrawlerTask(task)))
  }

  private queueCrawlerTasks(userId, locationId, representations, priority) {
    for (const representation of representations) {
      if (representation.url) {
        const options = {
          userId,
          locationId,
          priority,
          startUrl: representation.url,
          platform: representation.platform,
        }
        this.queueCrawlerTask(new CrawlerTask(options))
      }
    }
  }

  async syncReviewsForRepresentation(representation, locationId, userId, priority = TaskPriority.NORMAL) {
    this.queueCrawlerTask({
      userId,
      locationId,
      priority,
      startUrl: representation.url,
      platform: representation.platform,
    }).catch((error) => {
      console.error(error)
    })
  }

  async syncReviewsForRepresentations(representations: any[], locationId, userId) {
    for (const representation of representations) {
      await this.syncReviewsForRepresentation(representation, locationId, userId)
    }
  }

  async syncReviewsForLocations(locations: any[], userId: string, priority: TaskPriority = TaskPriority.NORMAL): Promise<any> {
    return Promise.all(locations.map(location => this.syncReviewsForLocation(location, userId, priority)))
  }

  async syncReviewsForLocation(location, userId, priority: TaskPriority = TaskPriority.NORMAL) {
    const representations = (await this.locationRepository
      .getRepresentationsByLocationId(location.id)).filter(representation => representation.type === 'crawler')
    if (!representations || representations.length <= 0)
      return
    this.queueCrawlerTasks(userId, location.id, representations, priority)
  }

  protected async purgeOutdatedReviews(existingReviews, requestedReviews): Promise<any> {
    return undefined
  }

  async requestPlatformReviewsForRepresentation(representation, location, userId): Promise<any> {
    return undefined
  }

  async getCurrentReviewCount(representation, location, userId, accessToken) {
    return undefined
  }

  async getCurrentLatestReview(representation, location, userId, accessToken) {
    return undefined
  }

  protected shouldSkipPurging(representation): boolean {
    return false
  }

  private getQueue({ platform }) {
    if (platform === 'yelp') return this.yelpCrawlerQueue
    return this.crawlerQueue
  }
}

class CrawlerTask {

  id: string
  startUrl: string
  userId: string
  locationId: string
  platform: string
  state: TaskState
  created: number
  priority: number

  constructor(options) {
    this.startUrl = options.startUrl
    this.userId = options.userId
    this.locationId = options.locationId
    this.platform = options.platform
    this.id = this.getPlatformPrefix() + '_' + this.locationId
    this.state = options.state ? options.state : TaskState.WAITING
    this.created = new Date().getTime()
    this.priority = options.priority
  }

  getPlatformPrefix() {
    switch (this.platform) {
      case 'yelp':
        return PlatformPrefix.YELP
      case 'tripadvisor':
        return PlatformPrefix.TRIPADVISOR
      case 'opentable':
        return PlatformPrefix.OPENTABLE
    }
  }
}

enum PlatformPrefix {
  TRIPADVISOR = 'ta',
  YELP = 'yp',
  OPENTABLE = 'ot',
}

enum TaskState {
  WAITING = 'WAITING',
  RUNNING = 'RUNNING',
  DONE = 'DONE',
  FAILED = 'FAILED',
}

export enum TaskPriority {
  HIGH = 1,
  NORMAL = 2,
}

