import { ConfigService } from '../../config/config.service'
import { LocationRepository } from '../location/location.repository'
import { Injectable } from '@nestjs/common'
import { Database } from '../database/database.baseProvider'
import { ReviewRepository } from '../review/review.repository'
import { ExternalApiError, SyncReviewsError } from '../api/errors'
import { Representation, REPRESENTATION_STATUS } from '../location/location.model'
import { FacebookErrorCodes } from '../api/facebook/api.facebook.model'
import { SlackApi } from '../api/slack/api.slack'
import { UserRepository } from '../user/user.repository'

@Injectable()
export abstract class SyncBaseService {

  private statisticQueue: any
  protected platform: string

  protected constructor(
        protected config: ConfigService,
        protected reviewRepository: ReviewRepository,
        protected locationRepository: LocationRepository,
        protected userRepository: UserRepository,
        protected database: Database,
        protected slackApi: SlackApi,
    ) {
    this.statisticQueue = this.database.firebase.ref('statisticQueue/tasks')
  }

  async syncReviewsForLocations(locations, userId) {
    const user = await this.userRepository.findById(userId)
    for (const location of locations) {
      const currentRepresentation = await this.getPlatformRepresentationForLocation(location)
      if (currentRepresentation) {
        await this.determineStrategyAndRequestReviews(currentRepresentation, location, user)
      }
    }
  }

  async determineStrategyAndRequestReviews(currentRepresentation, location, user) {
    const condition = credential => credential.id === currentRepresentation.credentialId
    const representationCredential = user.credentials.find(condition)

    const latestReviewCount = currentRepresentation.externalReviewCount
    const currentReviewCount = await this.getCurrentReviewCount(currentRepresentation, location, user.id, representationCredential)
    const latestReview = await this.reviewRepository.findLatestByRepresentationId(currentRepresentation.id)
    const currentLatestReview = await this.getCurrentLatestReview(currentRepresentation, location, user.id, representationCredential)

    if (latestReviewCount && currentReviewCount && latestReview.length > 0 && currentLatestReview) {
      if ((latestReviewCount === currentReviewCount) && (latestReview[0].externalId === currentLatestReview.reviewId)) {
        // In this case we expect that reviews didn't change and skip a new requesting
        console.log(`NOT updating ${this.platform}-reviews for location ${location.id} ${location.name} of user ${user.id}`)
        this.queueStatisticUpdateForRepresentations({
          userId: user.id,
          representation: currentRepresentation,
          locationId: location.id,
        })
        return
      }
    }
    if (currentRepresentation.platform === 'google' && !latestReview && !latestReviewCount) {
      const message =  `First sync of a google representation. The currentReviewCount is ${currentReviewCount}.`
      this.slackApi.sendMessageToChannel('review-manager-errors', message, [
        this.slackApi.createErrorAttachement({
          userId: user.id,
          locationId: location.id,
          representationId: currentRepresentation.id,
        }),
      ])
    }

    const requestedReviews =  await this.requestReviews(currentRepresentation, location, user.id, representationCredential)
    if (requestedReviews) {
      // Do not wait for these (very) time-consuming operations
      this.createReviews(requestedReviews, location, user.id).then(() => {
        const promise = this.shouldSkipPurging(currentRepresentation)
          ? Promise.resolve()
          : this.purgeReviews(requestedReviews, location)
        promise.then(() => {
          this.queueStatisticUpdateForRepresentations({
            userId: user.id,
            representation: currentRepresentation,
            locationId: location.id,
          })
        })
      })
    }

    await this.updateExternalReviewCount(location, currentRepresentation, currentReviewCount)
  }

  async requestReviews(currentRepresentation, location, userId, representationCredential): Promise<any> {
    console.log(`updating ${this.platform}-reviews for location ${location.id} ${location.name} of user ${userId}`)
    let requestedReviews = []
    try {
      requestedReviews = await this.requestPlatformReviewsForRepresentation(currentRepresentation, location, userId, representationCredential)
    } catch (e) {
      const code = e.status ? e.status : e.code ? e.code : e.response ? e.response.error ? e.response.error.code : null : null
      if (code === FacebookErrorCodes.ACCESSTOKEN_EXPIRED) {
        await this.setRepresentationExpired(currentRepresentation)
      } else if (code === FacebookErrorCodes.GRAPH_METHOD_EXCEPTION) {
        this.slackApi.logCatchedError('The known facebook error (code: 100) occurred. (Unsupported get request. Object with ID \'XXXXXXXXXXXXXXX_XXXXXXXXXXXXXX\' does not exist)')
      } else {
        this.slackApi.logErrors([this.slackApi.createErrorAttachement(new SyncReviewsError(userId, location, currentRepresentation, e.message, code))])
      }
    }
    return requestedReviews
  }

  async purgeReviews(requestedReviews, location) {
    if (requestedReviews.length > 0) {
      const dbReviews = await this.reviewRepository.findByLocationIdAndPlatform(location.id, this.platform)
      console.log(`Requested reviews: ${requestedReviews.length}, DB reviews: ${dbReviews.length} for location ${location.id} ${location.name} for user ${location.userId}`)
      if (dbReviews.length > 0 && (requestedReviews.length - dbReviews.length >= -4)) {
        await this.purgeOutdatedReviews(dbReviews, requestedReviews)
      } else {
        this.slackApi.logCatchedError(`More than four reviews for ${location.id} on ${this.platform} are gone.`)
      }
    }
  }

  async setRepresentationExpired(representation: Representation) {
    representation.status = REPRESENTATION_STATUS.ACCESSTOKEN_EXPIRED
    return this.locationRepository.updateRepresentation(representation.locationId, representation)
  }

  async createReviews(reviews, location, userId) {
    console.log(`start creating ${reviews.length} reviews for ${this.platform} ${location.id} ${location.name} for user ${userId}`)
    for (const review of reviews) {
      await this.reviewRepository.createOrUpdateReviewForPlatform(review, this.platform, location.id, userId)
    }
  }

  async updateExternalReviewCount(location, representation, externalReviewCount) {
    await this.locationRepository.updateExternalReviewCount(location.id, representation, externalReviewCount)
  }

  abstract async getCurrentReviewCount(representation, location, userId, accessToken)

  abstract async getCurrentLatestReview(representation, location, userId, accessToken)

  async getPlatformRepresentationForLocation(location) {
    const representations = await this.locationRepository.getRepresentationsByLocationId(location.id)
    return representations.find(representation => representation.platform === this.platform)
  }

  abstract async requestPlatformReviewsForRepresentation(representation, location, userId, accessToken): Promise<any>

  protected queueStatisticUpdateForRepresentations(update) {
    return Promise.resolve(this.queueStatisticUpdate(update))
  }

  protected queueStatisticUpdate(update) {
    return this.statisticQueue.push(update)
  }

  protected async abstract purgeOutdatedReviews(existingReviews, requestedReviews)

  protected abstract shouldSkipPurging(representation): boolean
}
