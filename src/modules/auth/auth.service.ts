import { GoogleAuthApi } from '../api/google/api.googleauth'
import { Injectable } from '@nestjs/common'
import { Database } from '../database/database.baseProvider'

@Injectable()
export class AuthService {
  constructor(private readonly googleAuthClient: GoogleAuthApi, private readonly database: Database) {
  }

  retrieveGoogleAccessToken(code: string) {
    return this.googleAuthClient.retrieveAccessToken(code)
  }

    /**
     *
     * Do you want to know more about firebase's custom claims?
     * See here: https://firebase.google.com/docs/auth/admin/custom-claims
     *
     * @param {string} userId The firebase uid of the user that should become an admin
     * @returns {Promise<any>} A promise which resolves when the operation is done.
     */
  setAdminClaim(userId: string): Promise<any> {
    return this.database.auth.setCustomUserClaims(userId, { admin: true })
  }
}
