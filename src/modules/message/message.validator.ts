import { MessageQueueItem } from './message.interface'
import { User } from '../user/user.model'
import { MessageSubscription } from './message.model'
import { MessageType } from './message.type'

export class MessageValidator {

  static messageIsValid(message): boolean {

    if (!message.payload) {
      throw new Error('Missing message payload')
    }

    if (message.subscription.type == MessageType.Email) {
      if (message.payload.Variables) {
        const result = MessageValidator.hasSupportedEmailTypes(message.payload.Variables, 'Variables')
        if (!result.isSupported)
          throw new Error(`Error in email message payload variables: ${result.message}`)
      }
    }

    if (!message.subscription) {
      throw new Error('Missing message subscription')
    }

    if (!message.subscription.type) {
      throw new Error('Missing message subscription type')
    }

    if (!message.subscription.name) {
      throw new Error('Missing message subscription name')
    }
    return true
  }

    /**
     *
     * A function that checks if the given object contains only objects and
     * primitives that don't produce errors on the email service (mailjet).
     * Mailjet cannot work with null values. A list of valid types can be found
     * in the isEmailType function.
     * This function is called recursively when the given object is iterable and not a primitive.
     *
     * @param obj The object that should be checked about email validity.
     *
     * @return A Boolean that indicates wether the given object can be used in the email service.
     */
  static hasSupportedEmailTypes(obj: any, key: string): {isSupported: Boolean, message: string} {
        // Note: the order of these two checks is important, because a string is iterable too.
    if (!MessageValidator.isEmailType(obj)) {
      if (!MessageValidator.isIterable(obj)) {
        const message = !!obj ? `Unsupported type for key '${key}': ${typeof obj}` : `The key '${key}' has a null value`
        return { isSupported: false, message }
      }
      for (const key in obj) {
        const value = obj[key]
        const result = MessageValidator.hasSupportedEmailTypes(value, key)
        if (!result.isSupported)
          return result
      }
    }
    return { isSupported: true, message: null }
  }

  static isIterable(obj: any): Boolean {
    if (!obj)
      return false
    if (typeof obj === 'object')
      return typeof Reflect.ownKeys(obj)[Symbol.iterator] === 'function'
    return typeof obj[Symbol.iterator] === 'function'
  }

  static isEmailType(obj: any): Boolean {
    const supportedTypes = ['string', 'number']
    const type = typeof obj
    return supportedTypes.includes(type)
  }

  static userHasSubscription(user: User, subscription: MessageSubscription): boolean {
    if (subscription.name === 'basic') {
      return true
    }
    if (user.subscriptions) {
      const messageSubscriptions = user.subscriptions
      if (messageSubscriptions.some(
                userSubscription => userSubscription.name === subscription.name
                    && userSubscription.type === subscription.type)
            ) {
        return true
      }
    }
    return false
  }
}

export class NotSubscribedError extends Error {
  constructor(user: User, subscription: MessageSubscription) {
    super(`User ${user.id} has not subscribed to ${subscription.name}-messages via ${subscription.type}`)
  }

}
