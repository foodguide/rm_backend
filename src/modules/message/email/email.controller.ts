import { Body, Controller, Get, Put, Res, UseGuards, ValidationPipe } from '@nestjs/common'
import { MessagePublisher } from '../../publisher/publisher.message'
import { UserRepository } from '../../user/user.repository'
import { SummaryEmailService } from './transactional/summary/summary.email.service'
import { EmailRecipient, NewsletterDto } from './email.model'
import { RetentionEmailService } from './transactional/reminder/retention.email.service'
import { AdminGuard } from '../../auth/admin.guard'
import { Response } from 'express'
import { NewsletterEmailService } from './transactional/newsletter/newsletter.email.service'

@Controller('email')
export class EmailController {
  constructor(
    private readonly userRepository: UserRepository,
    private readonly messagePublisher: MessagePublisher,
    private readonly summaryEmailService: SummaryEmailService,
    private readonly retentionEmailService: RetentionEmailService,
    private readonly newsletterEmailService: NewsletterEmailService,

  ) {
  }

  @Get('retentionmails')
  async sendRetentionMails() {
    await this.retentionEmailService.sendRetentionEmails()
  }

  @Get('summarymails')
  async sendSummaryMails() {
    return await this.summaryEmailService.sendSummaryMails()
  }

  @UseGuards(AdminGuard)
  @Put('newsletter')
  async sendNewsletter(@Body(new ValidationPipe()) dto: NewsletterDto, @Res() res: Response) {
    await this.newsletterEmailService.sendNewsletterMails(dto.templateId)
    EmailController.setSuccessBody(res)
    res.send()
  }

  @UseGuards(AdminGuard)
  @Get('questionnaire')
  async sendQuestionnaire(@Res() res: Response) {
    await this.newsletterEmailService.sendQuestionnaireMails()
    EmailController.setSuccessBody(res)
    res.send()
  }

  @Get('test')
  async test() {
    const user = await this.userRepository.findById('S5CSIeE6zKfQazapATHsBF2TsDE2')
    const recipient = new EmailRecipient().setEmail('matthias@thefoodguide.de')
    const end = new Date('2019-4-16')
    const start = new Date('2019-4-10')
    const testMail = await this.summaryEmailService.createSummaryEmail(user, start, end)
    testMail.setRecipients([recipient])
    this.messagePublisher.push(testMail, user)
  }

  /**
   * This function sets a body for a request that should do something and has ended successfully.
   *
   * @param {e.Response} res
   * @return {Response}
   */
  static setSuccessBody(res: Response) {
    return res.json({
      success: true,
    })
  }
}
