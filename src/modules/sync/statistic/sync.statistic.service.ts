import { RepresentationStatisticRepository } from '../../representationStatistic/representationStatistic.repository'
import { LocationRepository } from '../../location/location.repository'
import { RepresentationStatistic } from '../../representationStatistic/representationStatistic.model'
import { ReviewRepository } from '../../review/review.repository'
import { Injectable } from '@nestjs/common'
import { ExternalApiError } from '../../api/errors'
import moment from 'moment'
import { ReviewHelper } from '../../helper/helper.reviewHelper'
import { DateHelper } from '../../helper/helper.dateHelper'
import { LocationStatisticRepository } from '../../locationStatistic/locationStatistic.repository'
import { LocationStatistic } from '../../locationStatistic/locationStatistic.model'

@Injectable()
export class SyncStatisticService {
  constructor(private readonly representationStatisticRepository: RepresentationStatisticRepository,
              private readonly locationRepository: LocationRepository,
              private readonly reviewRepository: ReviewRepository,
              private readonly locationStatisticRepository: LocationStatisticRepository) {
  }

  async calculateStatisticForRepresentationForYesterday(representation, userId) {
    const yesterday = moment().add(-1, 'day').endOf('day')
    return await this.calculateStatisticForRepresentationForDate(representation, userId, yesterday.toDate(), null, true)
  }

  async calculateStatisticForRepresentationForDate(representation, userId , date = null, reviews = null, writeTotalStats = false) {
    const representationStatistic = await this.createRepresentationStatistic(representation, userId, date, reviews)
    if (representationStatistic) {
      this.representationStatisticRepository.crupdate(representationStatistic)
        .then(() => {
          console.log('updated statistics for '
            + representation.platform
            + '-representation of location '
            + representation.locationId)
          if (writeTotalStats) {
            return this.createLocationTotalStatistics(representation, userId)
          }
        }).catch((error) => {
          throw new ExternalApiError(
          representationStatistic.userId, error.message, error.response ? error.response.status : null)
        })
    }
  }

  async hasStatistics(representation) {
    return this.representationStatisticRepository.hasStatistics(representation.id)
  }

  async calculateStatisticsForRepresentation(representation, userId, thresholdMomentDate = null) {
    let reviews = await this.reviewRepository.findByRepresentationId(representation.id)
    reviews = reviews.filter(review => review.created)
    const oldestReview = ReviewHelper.getOldest(reviews)
    let creationDate = oldestReview
      ? moment(oldestReview.created.toDate())
      : moment(DateHelper.getDateFromTimestamp(representation.created))

    if (!thresholdMomentDate) {
      thresholdMomentDate = moment('2012-01-01')
    }
    if (creationDate.isBefore(thresholdMomentDate)) creationDate = thresholdMomentDate
    creationDate.add(-1, 'day').endOf('day')
    const daysInBetween = moment().diff(creationDate, 'days')
    for (let i = 0; i < daysInBetween; i += 1) {
      const date = creationDate.add(1, 'days')
      const reviewsUntilDate = reviews.filter(review => date.isAfter(moment(review.created.toDate())))
      await this.calculateStatisticForRepresentationForDate(representation, userId, date.toDate(), reviewsUntilDate)
    }
  }

  async createRepresentationStatistic(representation, userId, date = null, reviewsUntilDate = null) {
    const reviews = !!reviewsUntilDate
      ? reviewsUntilDate
      : await this.reviewRepository.findByRepresentationId(representation.id)
    const representationStatistic = new RepresentationStatistic()
    representationStatistic.date = !!date ? date : new Date()
    representationStatistic.representationId = representation.id
    representationStatistic.locationId = representation.locationId
    representationStatistic.userId = userId
    representationStatistic.totalReviews = reviews.length
    representationStatistic.id =
      representationStatistic.date.getFullYear().toString() + '_'
      + (representationStatistic.date.getMonth() + 1).toString() + '_'
      + representationStatistic.date.getDate().toString() + '_'
      + representation.id
    representationStatistic.ratingOne = 0
    representationStatistic.ratingTwo = 0
    representationStatistic.ratingThree = 0
    representationStatistic.ratingFour = 0
    representationStatistic.ratingFive = 0
    representationStatistic.recommendationPositive = 0
    representationStatistic.recommendationNegative = 0
    representationStatistic.answered = 0
    representationStatistic.avgRating = 0

    if (reviews.length === 0)
      return representationStatistic

    let reviewsWithRatingCount = 0
    let ratingSum = 0

    for (const review of reviews) {
      if (!!review.ratingType) {
        const recommendation = review.ratingType
        if (recommendation === 'positive') {
          representationStatistic.recommendationPositive += 1
          ratingSum += 5
          reviewsWithRatingCount += 1
        } else if (recommendation === 'negative') {
          representationStatistic.recommendationNegative += 1
          ratingSum += 1
          reviewsWithRatingCount += 1
        }
      } else if (!!review.rating) {
        switch (review.rating) {
          case 1: {
            representationStatistic.ratingOne += 1
            break
          }
          case 2: {
            representationStatistic.ratingTwo += 1
            break
          }
          case 3: {
            representationStatistic.ratingThree += 1
            break
          }
          case 4: {
            representationStatistic.ratingFour += 1
            break
          }
          case 5: {
            representationStatistic.ratingFive += 1
            break
          }
        }
        ratingSum += review.rating
        reviewsWithRatingCount += 1
      }

      if (review.response || review.state === 'seen')
        representationStatistic.answered += 1
    }

    representationStatistic.avgRating = reviewsWithRatingCount > 0 ? ratingSum / reviewsWithRatingCount : null
    return representationStatistic
  }

  async createLocationTotalStatistics(representation, userId) {
    if (representation.platform === 'instagram') return
    const locationId = representation.locationId
    const excludeInstagramYelp = representation => representation.platform !== 'instagram' && representation.platform !== 'yelp'
    const representations = await this.locationRepository.getRepresentationsByLocationId(locationId).filter(excludeInstagramYelp)
    const yesterday = moment().add(-1, 'day').endOf('day').toDate()
    const averageYesterday = await this.calculateAverageOnGivenDate(representations, yesterday)
    if (averageYesterday) {
      const thirtyDaysAgo = moment().add(-30, 'day').endOf('day').toDate()
      const averageOld = await this.calculateAverageOnGivenDate(representations, thirtyDaysAgo)
      return this.crupdateLocationStatistic(
        userId,
        locationId,
        yesterday,
        averageYesterday.totalCount,
        averageYesterday.average,
        averageOld ? averageOld.average : null)
    }
  }

  async calculateAverageOnGivenDate(representations, date) {
    let allStatsAvailable = true
    let average = 0
    let totalCount = 0
    for (const representation of representations) {
      const oldStat = await this.representationStatisticRepository.findForRepresentationByDate(representation.id, date)
      allStatsAvailable = oldStat.length > 0
      if (!allStatsAvailable) break
      average += oldStat[0].avgRating * oldStat[0].totalReviews
      totalCount += oldStat[0].totalReviews
    }
    return allStatsAvailable ? { totalCount, average: Number((average / totalCount).toFixed(2)) } : null
  }

  async crupdateLocationStatistic(userId, locationId, date, totalCount, average, averageOld) {
    let locationStatistic = await this.locationStatisticRepository.findByLocationId(locationId)
    if (locationStatistic.length === 0) {
      locationStatistic = new LocationStatistic()
      locationStatistic.id = locationId
      locationStatistic.userId = userId
      locationStatistic.locationId = locationId
      const location = await this.locationRepository.findDoc(locationId)
      locationStatistic.locationName = location.name
    } else {
      locationStatistic = locationStatistic[0]
    }
    locationStatistic.date = moment().toDate()
    locationStatistic.totalReviews = totalCount
    locationStatistic.avgRating = average
    locationStatistic.avgRatingOld = averageOld

    return this.locationStatisticRepository.create(locationStatistic).then(() => {
      console.log(`updated location statistics for ${locationId}`)
    })
  }
}
