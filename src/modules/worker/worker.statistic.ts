import { BaseWorker } from './worker.base'
import { Inject, Injectable } from '@nestjs/common'
import { SyncStatisticService } from '../sync/statistic/sync.statistic.service'
import { Database } from '../database/database.baseProvider'
import { ConfigService } from '../../config/config.service'

@Injectable()
export class StatisticWorker extends BaseWorker {

  constructor(
    @Inject('Database') private readonly database: Database,
    private readonly syncStatisticService: SyncStatisticService,
    protected config: ConfigService) {
    super(database, config)
  }

  get queueReference() {
    return 'statisticQueue'
  }

  process(data, progress, resolve, reject) {
    const onResult = (result) => {
      progress(100)
      resolve(result)
    }
    this.syncStatisticService.hasStatistics(data.representation).then((hasStatistics) => {
      if (!hasStatistics) {
        this.syncStatisticService.calculateStatisticsForRepresentation(data.representation, data.userId).then(onResult)
      } else {
        this.syncStatisticService
        .calculateStatisticForRepresentationForYesterday(data.representation, data.userId)
        .then(onResult)
      }
    })
  }
}
