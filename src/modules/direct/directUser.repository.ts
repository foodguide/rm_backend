import { Injectable } from '@nestjs/common'
import { BaseRepository } from '../database/database.baseRepository'

@Injectable()
export class DirectUserRepository extends BaseRepository {
  managedCollection = 'directUser'
}
