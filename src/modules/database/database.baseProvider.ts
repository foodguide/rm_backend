import firebaseAdmin from 'firebase-admin'
import { Injectable } from '@nestjs/common'

/**
 * An abstract class that creates the connection to the database.
 */
@Injectable()
export abstract class Database {
  readonly firestore
  readonly auth
  readonly firebase

  /**
   * The constructor function for the Database class. This function loads the correct firebase json file
   * and uses it to initialize the connection to firebase
   */
  constructor() {
    const serviceAccount = this.getServiceAccount()
    firebaseAdmin.initializeApp({
      credential: firebaseAdmin.credential.cert(serviceAccount),
      databaseURL: Database.getDatabaseURL(serviceAccount),
    })
    this.firestore = firebaseAdmin.firestore()
    this.firestore.settings({ timestampsInSnapshots: true })
    this.firebase = firebaseAdmin.database()
    this.auth = firebaseAdmin.auth()
  }

  /**
   * An abstract function to return the correct firebase json file which depends on the
   * environment (process.env.NODE_ENV)
   *
   * @return {any}
   */
  abstract getServiceAccount(): any

  /**
   * This function creates the url for the database based on the given
   * project id from the current service account.
   *
   * @param serviceAccount
   * @return {string}
   */
  static getDatabaseURL(serviceAccount: any): string {
    return `https://${serviceAccount['project_id']}.firebaseio.com`
  }
}
