#!/bin/bash
firebase auth:export accounts.json --project fg-review-manager
firebase auth:import accounts.json --project review-manager-test
firestore-export --accountCredentials ../prod_fg-admin-cert.json --backupFile ./databasedump.json
firestore-import --accountCredentials ../dev_fg-admin-cert.json --backupFile ./databasedump.json --yes
rm ./databasedump.json
rm ./accounts.json
exit
