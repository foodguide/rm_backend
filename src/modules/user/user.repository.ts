import { Injectable } from '@nestjs/common'
import { BaseRepository } from '../database/database.baseRepository'
import { MessageValidator } from '../message/message.validator'
import { User } from './user.model'
import { MessageSubscription } from '../message/message.model'
import { classToPlain } from 'class-transformer'
import firebaseAdmin, { firestore } from 'firebase-admin'
import ListUsersResult = firebaseAdmin.auth.ListUsersResult

@Injectable()
export class UserRepository extends BaseRepository {
  managedCollection = 'user'

  async findAllDatabaseUser() {
    return await super.findAll()
  }

  async findAll(nextPageToken?): Promise<ListUsersResult> {
    return await this.database.auth.listUsers(1000, nextPageToken)
  }

  async findAllWithDatabase() {
    const users = []
    let result = await this.findAll()
    users.push(...result.users)
    while (!!result.pageToken) {
      result = await this.findAll(result.pageToken)
      users.push(...result.users)
    }
    const databaseUsers = await super.findAll()
    return users.map((user) => {
      const databaseEquivalent = databaseUsers.find(dbUser => dbUser.userId === user.uid)
      return {
        ...user,
        ...databaseEquivalent,
      }
    })
  }

  async subscribeUserToMessage(user: User, subscription: MessageSubscription) {
    if (!MessageValidator.userHasSubscription(user, subscription)) {
      if (!user.subscriptions)
        user.subscriptions = []
      user.subscriptions.push(subscription)
      await this.update(user.id, classToPlain(user))
      return user.subscriptions
    }
  }

  async subscribeUserToMessages(user: User, subscriptions: MessageSubscription[]) {
    if (!user.subscriptions) {
      user.subscriptions = subscriptions
    } else {
      const newSubscriptions = subscriptions.filter(subscription => !MessageValidator.userHasSubscription(user, subscription))
      const plainNewSubscriptions = classToPlain(newSubscriptions)
      user.subscriptions = user.subscriptions.concat(plainNewSubscriptions)
    }
    await this.update(user.id, classToPlain(user))
    return user.subscriptions
  }

  async unsubscribeUserFromMessage(user: User, subscription: MessageSubscription) {

    const reducedSubscriptions = user.subscriptions.filter(
            activeSubscription => activeSubscription.name !== subscription.name
                || activeSubscription.type !== subscription.type)

    if (reducedSubscriptions.length < user.subscriptions.length) {
      user.subscriptions = reducedSubscriptions
      await this.update(user.id, classToPlain(user))
    }
    return user.subscriptions
  }

  async updateMessages(user: User, subscriptions: MessageSubscription[]) {
    user.subscriptions = subscriptions
    await this.update(user.id, classToPlain(user))
  }

  async updatePipedriveId(user, pipedriveId: string) {
    return this.collection.doc(user.userId).update({ pipedriveId })
  }

  async renameAccessTokensToCredentials(user) {
    return this.collection.doc(user.userId).update({ credentials: user.accessTokens, accessTokens: firestore.FieldValue.delete() })
  }

  async updateLastActiveTime(user) {
    return this.collection.doc(user.userId).update({ lastActiveTime: user.lastActiveTime })
  }
}
