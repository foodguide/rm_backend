import chargebee from 'chargebee'
import { PaymentApiInterface } from '../../payment/payment.api.interface'
import { Injectable } from '@nestjs/common'
import { ConfigService } from '../../../config/config.service'
import { ArrayHelper } from '../../helper/helper.arrayHelper'

@Injectable()
export class ChargebeeApi implements PaymentApiInterface {
  constructor(config: ConfigService) {
    chargebee.configure({
      site: config.get('CHARGEBEE_SITE'),
      api_key: config.get('CHARGEBEE_API_KEY'),
    })
  }

  getCheckoutUrlForExistingSubscription(subscriptionId: string) {
    return new Promise((resolve, reject) => {
      chargebee.hosted_page.checkout_existing({
        subscription: {
          id: subscriptionId,
        },
        embed: false,
      }).request((error, result) => {
        error ? reject(error) : resolve(result.hosted_page)
      })
    })
  }

  updateSubscription(subscriptionId: string, planId: string, updateAtEndOfTerm: boolean, numberOfLocations: number) {
    return new Promise((resolve, reject) => {
      chargebee.subscription.update(subscriptionId, {
        plan_id: planId,
        end_of_term: updateAtEndOfTerm,
        plan_quantity: numberOfLocations,
      }).request((error, result) => {
        error ? reject(error) : resolve(result)
      })
    })
  }

  removeScheduledChanges(subscriptionId: string) {
    return new Promise(((resolve, reject) => {
      chargebee.subscription.remove_scheduled_changes(subscriptionId).request((error, result) => {
        error ? reject(error) : resolve(result)
      })
    }))
  }

  retrieveScheduledChanges(subscriptionId: string) {
    return new Promise(((resolve) => {
      chargebee.subscription.retrieve_with_scheduled_changes(subscriptionId).request((error, result) => {
        /*
        Chargebee returns an error if no scheduled changes are available.
        Therefore it is important to resolve the error too and handle it later on.
         */
        error ? resolve(error) : resolve(result)
      })
    }))
  }

  retrieveSubscription(subscriptionId: string) {
    return new Promise(((resolve, reject) => {
      chargebee.subscription.retrieve(subscriptionId).request((error, result) => {
        error ? reject(error) : resolve(result)
      })
    }))
  }

  getCheckoutUrlForNewSubscription(customerId: string, planId: string, userEmail?: string) {
    const customer: any = {
      id: customerId,
      locale: 'de-DE',
    }
    if (!!userEmail)
      customer.email = userEmail
    return new Promise((resolve, reject) => {
      chargebee.hosted_page.checkout_new({
        customer,
        subscription: {
          plan_id: planId,
        },
        embed: false,
      }).request((error, result) => {
        error ? reject(error) : resolve(result.hosted_page)
      })
    })
  }

  getPortalSession(customerId: string) {
    return new Promise((resolve, reject) => {
      chargebee.portal_session.create({
        customer: {
          id: customerId,
          locale: 'de-DE',
        },
        embed: false,
      }).request((error, result) => {
        error ? reject(error) : resolve(result.portal_session)
      })
    })
  }

  getUpdatePaymentUrl(customerId: string) {
    return new Promise((resolve, reject) => {
      chargebee.hosted_page.manage_payment_sources({
        customer: {
          id: customerId,
          locale: 'de-DE',
        },
        embed: false,
      }).request((error, result) => {
        error ? reject(error) : resolve(result.portal_session)
      })
    })
  }

  getSubscriptions(customerId: string): Promise<Array<any>> {
    return new Promise((resolve, reject) => {
      chargebee.subscription.list({
        limit: 10,
        'customer_id[is]': customerId,
        'sort_by[asc]': 'created_at',
      }).request((error, result) => {
        error ? reject(error) : resolve(result.list)
      })
    })
  }

  cancelSubscriptions(customerId: string): Promise<Array<any>> {
    return this.getSubscriptions(customerId).then(subscriptions => subscriptions.filter(entry => {
      return new Promise((resolve, reject) => {
        chargebee.subscription.cancel(entry.subscription.id, { end_of_term: false }).request((error, result) => {
          error ? reject(error) : resolve(result)
        })
      })
    }))
  }

  getValidSubscriptions(customerId: string): Promise<Array<any>> {
        /*
        Possible values are
          future -> The Subscription is scheduled to start in a future date.
          in_trial -> The subscription is in trial.
          active -> The subscription is in active state and will be charged at start of each term based on the recurring items(plan & addons etc.,).
          non_renewing -> The subscription will be cancelled at end of the current term.
          paused -> The subscription is paused. No new recurring actions will be allowed, but any pending payments will be collected.
          cancelled -> The subscription has been cancelled. No new recurring actions will take place, but any pending payments will be collected.
         */
    return this.getSubscriptions(customerId).then(subscriptions => subscriptions.filter((entry) => {
      const status = entry.subscription.status
      return ChargebeeApi.isStatusValid(status)
    }))
  }

  private static isStatusValid(status) {
    const states = ChargebeeApi.getActiveStates()
    return states.includes(status)
  }

  private static getActiveStates() {
    return ['active', 'in_trial', 'non_renewing']
  }

  private getAllActiveSubscriptions(offset = null): Promise<Array<any>> {
    const params: any = {
      limit: 100,
      'status[in]': ChargebeeApi.getActiveStates(),
      'sort_by[asc]': 'created_at',
    }
    if (offset)
      params.offset = offset
    return new Promise((resolve, reject) => {
      chargebee.subscription.list(params).request((error, result) => {
        if (error)
          reject(error)
        else {
          if (result.next_offset) {
            console.log(`next_offset: ${result.next_offset}`)
            this.getAllActiveSubscriptions(result.next_offset).then((listTail) => {
              resolve([...result.list, ...listTail])
            })
          } else {
            resolve(result.list)
          }
        }
      })
    })
  }

  async getCustomerIdsWithActiveSubscriptions(): Promise<Array<string>> {
    let subscriptions = await this.getAllActiveSubscriptions()
    console.log(`counting subscriptions: ${subscriptions.length}`)
    subscriptions = subscriptions.map(subscriptionResult => subscriptionResult.customer.id)
    return ArrayHelper.removeDuplicates(subscriptions)
  }

  getCancelledSubscriptions(customerId: string): Promise<Array<any>> {
    return this.getSubscriptions(customerId).then(subscriptions => subscriptions.filter((entry) => {
      return entry.subscription.status === 'cancelled'
    }))
  }

  hasCancelledSubscriptions(customerId: string): Promise<boolean> {
    return this.getCancelledSubscriptions(customerId).then(list => list.length > 0)
  }

  hasValidSubscriptions(customerId: string): Promise<boolean> {
    return this.getValidSubscriptions(customerId).then(list => list.length > 0)
  }

  getHostedPage(hostedPageId: string): Promise<any> {
    return new Promise((resolve, reject) => {
      chargebee.hosted_page.retrieve(hostedPageId).request((error, result) => {
        error ? reject(error) : resolve(result.hosted_page)
      })
    })
  }
}
