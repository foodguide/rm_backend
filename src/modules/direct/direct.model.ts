import { BaseModel } from '../database/database.baseModel'

export class DirectTask extends BaseModel {
  reviewId: string
  locationName: string
  userName: string
  state = DirectTaskState.NEW
  created: Date = new Date()
  replied: Date = null
}

export enum DirectTaskState {
  NEW = 0,
  USER_NOTIFIED = 1,
  REPLY_RECEIVED = 2,
  REPLY_SENT = 3,
  REPLY_CONFIRMATION = 4,
}
