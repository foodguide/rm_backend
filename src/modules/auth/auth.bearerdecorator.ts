import { createParamDecorator } from '@nestjs/common'

export const BearerToken = createParamDecorator((data, request) => {
  return request.headers.authorization ? request.headers.authorization.split('Bearer ')[1] : null
})
