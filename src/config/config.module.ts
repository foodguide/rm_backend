import { Global, Module } from '@nestjs/common'
import { ConfigService } from './config.service'

const envFiles = process.env.NODE_ENV === 'development' ? [`./base.env`, `./dev.env`] : `./base.env`

@Global()
@Module({
  providers: [
    {
      provide: ConfigService,
      useValue: new ConfigService(envFiles),
    },
  ],
  exports: [ConfigService],
})
export class ConfigModule {}
