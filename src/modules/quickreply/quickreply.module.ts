import { Module } from '@nestjs/common'
import { QuickreplyRepository } from './quickreply.repository'

@Module({
  providers: [QuickreplyRepository],
  exports: [QuickreplyRepository],
})

export class QuickreplyModule{}

