import { IsNotEmpty, IsString } from 'class-validator'

export class WidgetDto {
  @IsNotEmpty()
  @IsString()
  widgetId: string
}
