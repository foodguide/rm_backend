import { Injectable } from '@nestjs/common'
import { GoogleAuthApi } from './api.googleauth'
import { GoogleAccessTokenProvider } from './api.googleaccesstokenprovider.model'
import { ArrayHelper } from '../../helper/helper.arrayHelper'
import { TimedCache } from '../../helper/helper.cache'
import axios from 'axios'

@Injectable()
export class GoogleMyBusinessApi {
  shouldSkipPurging : Map<string, boolean>
  timedCache: TimedCache
  constructor(
    private readonly googleAuth: GoogleAuthApi) {
    this.shouldSkipPurging = new Map<string, boolean>()
    this.timedCache = new TimedCache(45 * 60)
  }

  private getBaseUrl() {
    return 'https://mybusiness.googleapis.com/v4'
  }

  private getAccountsUrl() {
    return `${this.getBaseUrl()}/accounts`
  }

  private getLocationsUrl(accountName: string) {
    return `${this.getBaseUrl()}/${accountName}/locations`
  }

  private getReviewsUrl(accountName: string) {
    return this.resolveUrl(accountName, 'locations:batchGetReviews')
  }


  // Maybe useful when google keeps changing things
  /*
  private getReviewsFallbackUrl(locationName: string, pageToken: string = null) {
    const params: any = { pageSize: 20 }
    if (pageToken) params.pageToken = pageToken
    return this.resolveUrl(locationName, 'reviews', params)
  }*/

  private getReviewCountUrl(location: string) {
    return this.resolveUrl(location, 'reviews', { pageSize:1 })
  }

  private resolveUrl(name: string, endpoint: string = null, parameters: object = null) {
    let url = `${this.getBaseUrl()}/${name}`
    if (endpoint)
      url += `/${endpoint}`

    if (parameters) {
      let isFirstParam = true
      Object.keys(parameters).forEach((key) => {
        const value = parameters[key]
        if (isFirstParam) {
          url += `?${key}=${value}`
          isFirstParam = false
        } else {
          url += `&${key}=${value}`
        }
      })
    }
    return url
  }


  private getReplyUrl(reviewName: string) {
    return this.resolveUrl(reviewName, 'reply')
  }

  async getToken(accessTokenProvider: GoogleAccessTokenProvider) {
    return await this.googleAuth.getValidAccessToken(accessTokenProvider)
  }

  async getAccounts(accessTokenProvider: GoogleAccessTokenProvider) {
    return await this.request(accessTokenProvider, this.getAccountsUrl())
  }

  async getLocations(accessTokenProvider: GoogleAccessTokenProvider, account: string) {
    return await this.request(accessTokenProvider, this.getLocationsUrl(account))
  }

  async getAllLocations(accessTokenProvider: GoogleAccessTokenProvider) {
    const accounts = await this.getAccounts(accessTokenProvider).then(data => data.accounts)
    const promises = accounts.map(async account =>
      this.getLocations(accessTokenProvider, account.name).then(data => data.locations))
    return Promise.all(promises).then((results) => {
      const locations = []
      results.forEach((result) => {
        if (result)
        // @ts-ignore
          result.forEach(loc => locations.push(loc))
      })
      return { locations }
    })
  }

  async getReviewCount(accessTokenProvider: GoogleAccessTokenProvider, location: string) {
    return this.request(accessTokenProvider, this.getReviewCountUrl(location)).then(data => data.totalReviewCount)
  }

  async getLatestReview(accessTokenProvider: GoogleAccessTokenProvider, location: string) {
    return this.request(accessTokenProvider, this.getReviewCountUrl(location)).then(data => data.reviews ? data.reviews[0] : undefined)
  }

  async getReviews({ accessTokenProvider, account, location, nextPageToken = null, reviewList = [] }): Promise<any> {
    this.shouldSkipPurging.set(location, false)
    const requestBody = {
      locationNames: [location],
      pageSize: 200,
      pageToken: nextPageToken,
      ignoreRatingOnlyReviews: false,
    }
    const onCatch = (e) => {
      const status = e.response ? e.response.status : null
      console.log(`Google Get Reviews failed with status ${status} for location ${location} for user ${accessTokenProvider.user.id} at list size ${reviewList.length}`)
      if (status && status === 400) {
        this.shouldSkipPurging.set(location, true)
        return reviewList
      }
      throw e
    }
    const onRecursiveFetch = result => this.getReviews(result)
    return this.request(accessTokenProvider, this.getReviewsUrl(account), 'POST', requestBody).then(responseData =>
      this.handleReviewResponse({
        accessTokenProvider,
        account,
        location,
        reviewList,
        responseData,
        onRecursiveFetch,
      })).catch(onCatch)
  }

  async handleReviewResponse({ accessTokenProvider, account, location, reviewList, responseData, onRecursiveFetch }) {
    if (!responseData)
      return reviewList

    const appendReviews = async (data) => {
      let combinedReviewList = reviewList
      if (ArrayHelper.isIterable(data.locationReviews)) {
        combinedReviewList = [...reviewList, ...data.locationReviews.map(reviewObj => reviewObj.review)]
      } else if (ArrayHelper.isIterable(data.reviews)) {
        combinedReviewList = [...reviewList, ...data.reviews.map(reviewObj => reviewObj.review)]
      }
      if (data.nextPageToken) {
        return onRecursiveFetch({
          accessTokenProvider,
          account,
          location,
          nextPageToken: data.nextPageToken,
          reviewList: combinedReviewList })
      } else {
        return combinedReviewList
      }
    }
    return appendReviews(responseData).then((list) => {
      console.log(`returning google reviews for ${location}. count: ${list.length}`)
      return list
    })
  }

  // This function may be useful when google keeps changing things
  /*
  private async getReviewsFallback({ accessTokenProvider, account, location, nextPageToken = null, reviewList = [] }) {
    const url = this.getReviewsFallbackUrl(location, nextPageToken)
    const onCatch = (e) => {
      console.log(`ooon catch:`)
      console.log(reviewList.length)
      const status = e.response ? e.response.status : null
      if (status && status === 400) {
        //  this.getReviewsFallback({ accessTokenProvider, account, location })
        return new Promise((resolve) => {
          setTimeout(() => {
            resolve()
          },         60000)
        }).then(() => {
          return this.getReviewsFallback({ accessTokenProvider, account, location, nextPageToken, reviewList })
        })
      }
      throw e
    }
    return await this.request(accessTokenProvider, url).then(observable => this.handleReviewResponse({
      accessTokenProvider,
      account,
      location,
      reviewList,
      observable,
      onRecursiveFetch: result => this.getReviewsFallback(result),
    })).catch(onCatch)
  }*/


  async sendReply(accessTokenProvider: GoogleAccessTokenProvider, uri: string, text: string) {
    return await this.request(accessTokenProvider, this.getReplyUrl(uri), 'PUT', {
      comment: text,
    })
  }

  private getHeaders(token) {
    return {
      headers: { Authorization: 'Bearer ' + token },
    }
  }

  private getConfig(token, url, method, data) {
    return data
      ? { url, method: method.toLowerCase(), ...this.getHeaders(token), data }
      : { url, method: method.toLowerCase(), ...this.getHeaders(token) }
  }

  private async request(accessTokenProvider: GoogleAccessTokenProvider, url, method = 'GET', data = null) {
    const token = await this.getTokenFromCacheOrGoogle(accessTokenProvider)
    if (!token) {
      console.log(`couldn't get an accesstoken for google`)
      return null
    }
    console.log(`sending requests: ${url}`)
    console.log(`token: ${token}`)
    const config = this.getConfig(token, url, method, data)
    return axios.request(config).then(response => response.data)
  }

  private async getTokenFromCacheOrGoogle(accessTokenProvider) {
    const identifier = accessTokenProvider.credential && accessTokenProvider.credential.id ? accessTokenProvider.credential.id : accessTokenProvider.credentialId
    return this.timedCache.getEntryOrCreate(identifier, async () => await this.getToken(accessTokenProvider))
  }

}
