import { Module } from '@nestjs/common'
import { MessagePublisher } from './publisher.message'

// @ts-ignore
@Module({
  providers: [MessagePublisher],
  exports: [MessagePublisher],
})

export class PublisherModule {
}
