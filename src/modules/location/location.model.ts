import { BaseModel } from '../database/database.baseModel'
import { Transform, Type } from 'class-transformer'

export class Location extends BaseModel {
  name: string
  @Type(() => Representation)
  representations: Representation[]
  @Type(() => Date)
  @Transform((value, obj) => obj.unseenOldReviewsTimestamp.toDate(), { toClassOnly: true })
  unseenOldReviewsTimestamp: Date
}

export abstract class BaseRepresentation extends BaseModel {
  externalId: string
  locationId: string
  info: any
  platform: string
  type: string
  url: string
  @Type(() => Date)
  @Transform((value, obj) => obj.unseenOldReviewsTimestamp.toDate(), { toClassOnly: true })
  unseenOldReviewsTimestamp: Date
}

export class Representation extends BaseRepresentation {
  accessToken: string
  status: string = REPRESENTATION_STATUS.ACTIVE
}

export const REPRESENTATION_STATUS = {
  ACTIVE: 'active',
  ACCESSTOKEN_EXPIRED: 'accesstoken_expired',
}
