import { NestFactory } from '@nestjs/core'
import { AppModule } from './app.module'
import cors from 'cors'
import { SlackErrorFilter } from './modules/error/slack.error.filter'
// @ts-ignore
Promise = require('bluebird')

declare const module: any

async function bootstrap() {
  const app = await NestFactory.create(AppModule)
  app.useGlobalFilters(app.get(SlackErrorFilter))
  app.use(cors())
  await app.listen(8080)
  if (module.hot) {
    module.hot.accept()
    module.hot.dispose(() => app.close())
  }
}

bootstrap()
