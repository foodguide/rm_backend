import { Controller, Get, Query, ValidationPipe } from '@nestjs/common'
import { FacebookAccesstokenDto } from './facebook.accesstoken.dto'
import { FacebookService } from './facebook.service'

@Controller('facebook')
export class FacebookController {
  constructor(private facebookService: FacebookService) {}

  @Get('accesstoken')
  async getAccessToken(@Query(new ValidationPipe()) dto: FacebookAccesstokenDto) {
    return this.facebookService.facebookApi.getAccessToken(dto.accessToken)
  }

  @Get('account')
  async getAccountInformation(@Query(new ValidationPipe()) dto: FacebookAccesstokenDto) {
    return this.facebookService.facebookApi.getAccountInformation(dto.accessToken)
  }
}
