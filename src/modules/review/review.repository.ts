import { BaseRepository } from '../database/database.baseRepository'
import { Injectable } from '@nestjs/common'
import { GmbReviewModel } from '../googlemybusiness/gmb.review.model'
import * as admin from 'firebase-admin'
import Query = admin.firestore.Query
import moment from 'moment'

/**
 * This is the repository for the review collection
 */
@Injectable()
export class ReviewRepository extends BaseRepository {
  managedCollection = 'review'

  /**
   * This function creates a facebook review with the given data.
   *
   * @param data
   * @param locationId
   * @param userId
   * @return {Promise<any>}
   */
  async createFacebookReview(data, locationId, userId) {
    const reviewData: any = {
      userId,
      locationId,
      id: 'fb_' + locationId + '_' + data.open_graph_story.id,
      externalId: data.open_graph_story.id,
      platform: 'facebook',
      created: data.created_time ? new Date(data.created_time) : null,
      reviewer: data.reviewer ? data.reviewer : null,
      rating: data.open_graph_story.data.rating ? data.open_graph_story.data.rating.value : null,
      ratingType: data.open_graph_story.data.recommendation_type
        ? data.open_graph_story.data.recommendation_type
        : null,
      text: data.open_graph_story.data.review_text
        ? data.open_graph_story.data.review_text
        : null,
      updated: data.open_graph_story.data.review_update_time
        ? data.open_graph_story.data.review_update_time
        : null,
      url: 'http://www.facebook.com' + '/' + data.open_graph_story.id,
      comments: data.comments ? data.comments : null,
      representationId: `fb_${locationId}`,
    }
    if (data.response)
      reviewData.response = data.response
    return this.createOrUpdateReview(reviewData)
  }

  /**
   * Creating an instagram review with the given data
   *
   * @param data
   * @param locationId
   * @param userId
   * @return {any}
   */
  createInstagramReview(data, locationId, userId) {
    const reviewData = {
      userId,
      locationId,
      representationId: `ig_${locationId}`,
      ...data,
    }
    return this.createOrUpdateReview(reviewData)
  }

  /**
   * Creating a google review with the given data
   *
   * @param {GmbReviewModel} gmbReview
   * @param {string} locationId
   * @param {string} userId
   * @return {Promise<any>}
   */
  async createGoogleReview(gmbReview: GmbReviewModel, locationId: string, userId: string) {
    const mappedReview: any = {
      userId,
      locationId,
      id: `go_${locationId}_${gmbReview.reviewId}`,
      created: new Date(gmbReview.createTime),
      updated: new Date(gmbReview.updateTime),
      externalId: gmbReview.reviewId,
      platform: 'google',
      representationId: `go_${locationId}`,
      rating: this.getGoogleRating(gmbReview),
      reviewer: {
        id: null,
        name: gmbReview.reviewer.displayName ? gmbReview.reviewer.displayName : null,
      },
      text: gmbReview.comment ? gmbReview.comment : null,
      url: null,
      externalURI: gmbReview.name,
    }
    if (gmbReview.reviewReply) {
      mappedReview.response = {
        id: `reply_go_${gmbReview.reviewId}`,
        responder: null,
        text: gmbReview.reviewReply.comment,
        updated: new Date(gmbReview.reviewReply.updateTime),
      }
    }
    return this.createOrUpdateReview(mappedReview)
  }

  /**
   * This function transforms the text to a number for a review that has the
   * google my business structure.
   *
   * @param review
   * @return {number}
   */
  getGoogleRating(review): number {
    switch (review.starRating.toLowerCase()) {
      case 'five':
        return 5
      case 'four':
        return 4
      case 'three':
        return 3
      case 'two':
        return 2
      case 'one':
        return 1
      default:
        return null
    }
  }

  /**
   * This function creates a review or updates a review whether it already exists or not.
   *
   * @param data
   * @return {any}
   */
  createOrUpdateReview(data) {
    return this.update(data.id, data)
        .catch((error) => {
          if (error.code === 'not-found' || error.code === 5) {
            const hasResponse = data.response
            const isNew = Math.abs(moment(data.created).diff(moment(), 'months')) < 3
            data.state = !hasResponse && isNew ? 'unseen' : 'seen'
            return this.create(data)
          } else throw new Error(error)
        })
  }

  /**
   * This function checks the platform chooses then the correct function for creating the review.
   *
   * @param data
   * @param platform
   * @param locationId
   * @param userId
   * @return {Promise<void>}
   */
  async createOrUpdateReviewForPlatform(data, platform, locationId?, userId?) {
    switch (platform) {
      case 'facebook':
        await this.createFacebookReview(data, locationId, userId)
        break
      case 'google':
        await this.createGoogleReview(data, locationId, userId)
        break
      case 'instagram':
        await this.createInstagramReview(data, locationId, userId)
        break
      default:
        throw Error('Platform not supported!')
    }
  }

  /**
   * This function finds all reviews for a specific location and platform.
   *
   * @param {string} locationId
   * @param {string} platform
   * @return {any[]} Plain review objects
   */
  findByLocationIdAndPlatform(locationId: string, platform: string): any[] {
    return this.commitRead(this.collection
      .where('locationId', '==', locationId)
      .where('platform', '==', platform).get())
  }

  /**
   * This functions finds and returns all reviews for specific representation.
   *
   * @param {string} representationId
   * @return {any[]}
   */
  findByRepresentationId(representationId: string): any[] {
    return this.commitRead(this.collection.where('representationId', '==', representationId).get())
  }

  /**
   * This functions finds latest review for specific representation.
   *
   * @param {string} representationId
   * @return {any[]}
   */
  findLatestByRepresentationId(representationId: string): any[] {
    return this.commitRead(this.collection
        .where('representationId', '==', representationId)
        .orderBy('created', 'desc')
        .limit(1)
        .get())
  }

  /**
   * This function returns all reviews for a user between two given dates (one is optional).
   *
   * @param {string} userId
   * @param {Date} before
   * @param {Date} after
   * @return {any[]} Plain review objects
   */
  findAllBetweenDates(userId: string, before: Date, after: Date): Promise<any[]> {
    const query = this.getAllBetweenDatesQuery(userId, before, after)
    return this.commitRead(query.get())
  }

  findAllWithText(text: string) {
    return this.commitRead(this.collection.where('text', '==', text).get())
  }

  /**
   * This function returns the query object for getting all reviews for a user between
   * two given dates (one is optional).
   *
   * @param {string} userId
   * @param {Date} before
   * @param {Date} after
   * @return {admin.database.Query}
   */
  getAllBetweenDatesQuery(userId: string, before: Date, after: Date): Query {
    let query = this.collection.where('userId', '==', userId)
    if (before)
      query = query.where('created', '<=', before)
    if (after)
      query = query.where('created', '>=', after)
    return query
  }

  /**
   * This is a function that can be used for updating a specific review.
   *
   * @param {string} id
   * @param data
   * @return {any}
   */
  update(id: string, data) {
    return this.commitWrite(this.collection.doc(id).update(data))
  }

  /**
   * This is a function to store reply for a review.
   *
   * @param id
   * @param reply
   */
  replyToReview(id: string, reply) {
    return this.update(id, {
      state: 'seen',
      response: reply,
    })
  }

}
