import { Module } from '@nestjs/common'
import { DirectService } from './direct.service'
import { DirectController } from './direct.controller'
import { DirectUserRepository } from './directUser.repository'
import { DirectTaskRepository } from './directTask.repository'
import { LocationModule } from '../location/location.module'
import { UserModule } from '../user/user.module'

@Module({
  imports: [LocationModule, UserModule],
  controllers: [DirectController],
  providers: [DirectService, DirectUserRepository, DirectTaskRepository],
  exports: [DirectService, DirectUserRepository],
})

export class DirectModule{
}
