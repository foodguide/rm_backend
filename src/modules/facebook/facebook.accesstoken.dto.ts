import { IsNotEmpty, IsString } from 'class-validator'

export class FacebookAccesstokenDto {
  @IsNotEmpty()
  @IsString()
  readonly accessToken: string
}
