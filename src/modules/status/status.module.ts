import { Module } from '@nestjs/common'
import { ApiModule } from '../api/api.module'
import { StatusService } from './status.service'
import { StatusController } from './status.controller'

@Module({
  imports: [ApiModule],
  providers: [StatusService],
  controllers: [StatusController],
})

export class StatusModule {}
