import { Injectable } from '@nestjs/common'
import { AuthGuard } from './auth.guard'

@Injectable()
export class AdminGuard extends AuthGuard {
  protected validateClaims(claims): boolean {
    if (!super.validateClaims(claims))
      return false
    return claims.admin === true
  }
}
