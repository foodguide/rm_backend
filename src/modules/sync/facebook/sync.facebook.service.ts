import { Inject, Injectable } from '@nestjs/common'
import { FacebookApi } from '../../api/facebook/api.facebook'
import { ReviewRepository } from '../../review/review.repository'
import { LocationRepository } from '../../location/location.repository'
import { SyncBaseService } from '../sync.base.service'
import { Database } from '../../database/database.baseProvider'
import { ConfigService } from '../../../config/config.service'
import { SlackApi } from '../../api/slack/api.slack'
import { SyncReviewsError } from '../../api/errors'
import { UserRepository } from '../../user/user.repository'
import { FacebookHelper } from '../../helper/helper.facebookHelper'

@Injectable()
export class SyncFacebookService extends SyncBaseService {
  constructor(
        private readonly facebookClient: FacebookApi,
        protected config: ConfigService,
        protected readonly slackApi: SlackApi,
        protected locationRepository: LocationRepository,
        protected reviewRepository: ReviewRepository,
        protected userRepository: UserRepository,
        @Inject('Database') protected database: Database,
    ) {
    super(config, reviewRepository, locationRepository, userRepository, database, slackApi)
  }

  get platform() {
    return 'facebook'
  }

  protected async purgeOutdatedReviews(existingReviews, requestedReviews) {
    const requestedReviewIds = requestedReviews.map(requestedReview => requestedReview.open_graph_story.id)
    const reviewsToPurge = existingReviews.filter((fetchedReview) => {
      return !requestedReviewIds.includes(fetchedReview.externalId)
    })
    for (const review of reviewsToPurge) {
      this.reviewRepository.delete(review.id).then((response) => {
        return response
      })
    }
  }

  async requestPlatformReviewsForRepresentation(representation, location, userId, accessToken): Promise<any> {
    const requestedReviews = await this.facebookClient.getReviews(representation.accessToken, representation.externalId)
    return await Promise.all(requestedReviews.map(async (review) => {
      review = await this.getCommentsForFacebookReview(review, representation, userId)
      const response = FacebookHelper.findResponseInComments(review, representation)
      if (response)
        review.response = response
      return review
    }))
  }

  private async getCommentsForFacebookReview(review, representation: any, userId) {
    try {
      const mainComments: any = (await this.facebookClient
            .getComments(representation.accessToken, review.open_graph_story.id, representation.id, userId))

      if (mainComments && mainComments.data.length > 0) {
        review.comments = mainComments.data
        for (const comment of review.comments) {
          if (comment) {
            const subcomments = (await this.facebookClient.getSubcomments(representation.accessToken, comment.id, representation.id, userId))
            if (subcomments && subcomments.data.length > 0) {
              comment.subcomments = subcomments.data
            }
          }
        }
      }
    } catch (e) {
      this.slackApi.logErrors([this.slackApi.createErrorAttachement(new SyncReviewsError(userId, null, representation, e.message))])
    }

    return review
  }

  async getCurrentReviewCount(representation, location, userId, accessToken) {
    return this.facebookClient.getReviewCount(representation.accessToken, representation.externalId)
  }

  async getCurrentLatestReview(representation, location, userId, accessToken) {
    return this.facebookClient.getLatestReview(representation.accessToken, representation.externalId)
  }

  protected shouldSkipPurging(representation): boolean {
    return false
  }
}
