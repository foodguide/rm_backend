import { IsNotEmpty, IsOptional, IsString, IsUrl, ValidateNested } from 'class-validator'
import { BasicFacebookGraphApiObject, FacebookPage } from './api.facebook.model'

export class InstagramMediaObject extends BasicFacebookGraphApiObject{
  @IsNotEmpty()
  @IsString()
  timestamp: string
  @IsNotEmpty()
  @IsUrl()
  media_url: string
  @IsOptional()
  @IsUrl()
  thumbnail_url: string
  @IsNotEmpty()
  @IsString()
  media_type: string
  @IsNotEmpty()
  @IsUrl()
  permalink: string
  @IsOptional()
  @IsString()
  caption: string
}

export class InstagramComment extends BasicFacebookGraphApiObject{
  @IsNotEmpty()
  @IsString()
  timestamp: string
  @IsOptional()
  @IsString()
  username: string
  @IsString()
  @IsNotEmpty()
  text: string
}

export class InstagramCommentReply extends InstagramComment{}
