import { forwardRef, Module } from '@nestjs/common'
import { User } from './user.model'
import { UserRepository } from './user.repository'
import { UserController } from './user.controller'
import { PublisherModule } from '../publisher/publisher.module'
import { EventModule } from '../event/event.module'
import { GmbModule } from '../googlemybusiness/gmb.module'
import { LocationRepository } from '../location/location.repository'
import { UserBackupRepository } from './user-backup-repository.service'
import { FacebookModule } from '../facebook/facebook.module'

@Module({
  imports: [PublisherModule, EventModule, forwardRef(() => GmbModule), FacebookModule, LocationRepository],
  providers: [User, UserRepository, UserBackupRepository],
  controllers: [UserController],
  exports: [UserRepository, UserBackupRepository],
})
export class UserModule {}
