import { Injectable } from '@nestjs/common'
import { FacebookApi } from '../api/facebook/api.facebook'

@Injectable()
export class InstagramService {
  constructor(private readonly _facebookApi: FacebookApi) {}
  get facebookApi() {
    return this._facebookApi
  }

  async sendReply(user, review, representation, text) {
    const credential = user.credentials.find(c => c.id === representation.credentialId)
    return this.facebookApi.send(`/${review.externalId}/replies?message=${text}`, { access_token: credential.accessToken }, 'POST')
  }

}
