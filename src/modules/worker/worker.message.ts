import { Inject, Injectable } from '@nestjs/common'
import { BaseWorker } from './worker.base'
import { Database } from '../database/database.baseProvider'
import { MessageDispatcher } from '../message/message.dispatcher'
import { ConfigService } from '../../config/config.service'

@Injectable()
export class MessageWorker extends BaseWorker {

  constructor(
    @Inject('Database') private readonly database: Database,
    private readonly dispatcher: MessageDispatcher,
    protected config: ConfigService) {
    super(database, config)
  }

  get queueReference() {
    return 'messageQueue'
  }

  process(data, progress, resolve, reject) {
    this.dispatcher.dispatch(data).then(result => {
      progress(100)
      return resolve(result)
    }).catch(reason => {
      console.log(reason)
      reject(reason)
    },
    )
  }
}
