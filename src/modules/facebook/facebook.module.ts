import { FacebookController } from './facebook.controller'
import { Module } from '@nestjs/common'
import { FacebookService } from './facebook.service'
import { InstagramService } from './instagram.service'

@Module({
  providers: [FacebookService, InstagramService],
  controllers: [FacebookController],
  exports: [FacebookService, InstagramService],
})
export class FacebookModule{}
