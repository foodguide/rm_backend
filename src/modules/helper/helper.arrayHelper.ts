export class ArrayHelper {
  static removeDuplicates(array: any[]): any[] {
    return array.filter((v,i) => array.indexOf(v) === i)
  }

  static flatMap(array: any[]): any[] {
    const flat = []
    array.forEach((subArray) => {
      if (ArrayHelper.isIterable(subArray))
        subArray.forEach(element => flat.push(element))
    })
    return flat
  }

  static isIterable = object => object != null && typeof object[Symbol.iterator] === 'function'
}
