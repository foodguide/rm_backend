import { IncomingWebhook } from '@slack/client'
import { WebClient } from '@slack/client'
import { ConfigService } from '../../../config/config.service'
import { Injectable } from '@nestjs/common'

@Injectable()
export class SlackApi {
  private webHook: IncomingWebhook
  private webClient: WebClient

  constructor(config: ConfigService) {
    this.webHook = new IncomingWebhook(config.get('SLACK_WEBHOOK'))
    this.webClient = new WebClient(config.get('SLACK_BOTTOKEN'))
  }

  sendMessageToChannel(channel: string, text: string, attachments: Array<any>) {
    this.webClient.chat.postMessage({
      channel, text, attachments,
    }).catch(error => {
      console.log('Slack Post Message Fail:')
      console.log(error)
      console.log(text)
      console.log(attachments)
    })
  }

  logErrors(errors: Array<any>) {
    this.sendMessageToChannel('review-manager-errors', 'Error', errors)
  }

  logCatchedError(message: string) {
    const slackMessage = {
      text: `Catched an error: ${message}`,
      color: '#e8e417',
    }
    this.logErrors([slackMessage])
  }

  createErrorAttachement(data) {
    return this.createAttachement('A  *BACKEND ERROR* occured!', data)
  }

  createAttachement(title, data) {
    const fields = []
    const longFields = ['message', 'stacktrace', 'browser', 'url', 'stack', 'startUrl', 'payload']

    for (const key in data) {
      if (data[key] && data[key] !== null) {
        fields.push({
          title: key,
          value: typeof data[key] === 'object' ? JSON.stringify(data[key]) : data[key],
          short: !longFields.includes(key),
        })
      }
    }

    return {
      text: title + ' \n',
      color: '#6A1918',
      fields,
    }
  }

}
