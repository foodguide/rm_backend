import { Location, Representation } from '../location/location.model'

export class ExternalApiError extends Error {
  userId
  code

  constructor(userId: string, message?: string, code?: string, stack?: string) {
    super()
    this.message = message
    this.userId = userId
    this.code = code
    this.name = 'ExternalApiError'
    this.stack = stack ? stack : (<any>new Error()).stack
  }
}

export class SyncReviewsError extends ExternalApiError {
  locationId: string
  representationId: string
  location: Location
  representation: Representation

  constructor(userId: string, location: Location, representation: Representation, message?: string, code?: string, stack?: string) {
    super(userId, message, code, stack)
    this.location = location
    this.representation = representation
    this.name = 'SyncReviewsError'
  }
}
