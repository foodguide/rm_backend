import { Database } from './database.baseProvider'
import { Injectable } from '@nestjs/common'

@Injectable()
export class DevDatabase extends Database {

  getServiceAccount() {
    return require('../../../dev_fg-admin-cert.json')
  }

}
