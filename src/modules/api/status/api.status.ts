import { StatusApiInterface } from '../../status/status.api.interface'
import { Injectable } from '@nestjs/common'

@Injectable()
export class StatusApi implements StatusApiInterface {
  constructor() {}

  getStatus() {
    return Promise.resolve('true')
  }
}
