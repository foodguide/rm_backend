
export class ReviewHelper {
  static getOldest(reviews: any[]): any {
    if (!reviews || reviews.length === 0) return null
    if (reviews.length === 1) return reviews[0]
    const oldest = reviews.reduce((left, right) => {
      return ReviewHelper.getOlder(left, right)
    })
    return oldest
  }

  static compare(left: any, right: any): number {
    if (!left.created) {
      return 0
    }

    if (!right.created) {
      return -1
    }

    return left.created.toDate().getTime() - right.created.toDate().getTime()
  }

  static getOlder(left: any, right: any): any {
    const result = ReviewHelper.compare(left, right)
    return result < 0 ? left : right
  }
}
