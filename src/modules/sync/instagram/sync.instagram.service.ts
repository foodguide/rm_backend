import { Inject, Injectable } from '@nestjs/common'
import { SyncBaseService } from '../sync.base.service'
import { FacebookApi } from '../../api/facebook/api.facebook'
import { ConfigService } from '../../../config/config.service'
import { LocationRepository } from '../../location/location.repository'
import { ReviewRepository } from '../../review/review.repository'
import { Database } from '../../database/database.baseProvider'
import { Representation } from '../../location/location.model'
import { InstagramComment, InstagramMediaObject } from '../../api/facebook/api.facebook.instagram.model'
import { InstagramHelper } from '../../helper/helper.instagramHelper'
import { DateHelper } from '../../helper/helper.dateHelper'
import { SlackApi } from '../../api/slack/api.slack'
import { UserRepository } from '../../user/user.repository'

@Injectable()
export class SyncInstagramService extends SyncBaseService {
  constructor(
    private readonly facebookClient: FacebookApi,
    protected config: ConfigService,
    protected locationRepository: LocationRepository,
    protected reviewRepository: ReviewRepository,
    protected userRepository: UserRepository,
    protected readonly slackApi: SlackApi,
    @Inject('Database') protected database: Database,
  ) {
    super(config, reviewRepository, locationRepository, userRepository, database, slackApi)
  }

  get platform() {
    return 'instagram'
  }

  protected async purgeOutdatedReviews(existingReviews, requestedReviews): Promise<any> {
    return await Promise.all(
      existingReviews.map(
        existingReview =>
          !(requestedReviews.find(requestedReview =>
            requestedReview.id === existingReviews.id)) ?
            Promise.resolve() : this.reviewRepository.delete(existingReview.id),
      ))
  }

  async requestPlatformReviewsForRepresentation(representation, location, userId): Promise<any> {
    const castedRepresentation = (representation as Representation)
    const { accessToken, externalId } = castedRepresentation
    const mediaObjects = await this.facebookClient.instagram.getMediaObjects(accessToken, externalId)
    const responses = await Promise.all(mediaObjects.map(media => this.getReviews(accessToken, media, representation, location)))
    let reviews = []
    responses.forEach((response) => {
      reviews = [...reviews, ...response]
    })
    return InstagramHelper.arrange(reviews, castedRepresentation)
  }

  async getReviews(accessToken: string, media: InstagramMediaObject, representation: Representation, location) {
    return await this.facebookClient.instagram.getMediaComments(accessToken, media.id).then(async (comments) => {
      const responses = await Promise.all(comments.map(comment => this.getReview(accessToken, comment, media, representation, location)))
      return responses
    })
  }

  async getReview(accessToken: string, comment: InstagramComment, media: InstagramMediaObject, representation: Representation, location) {
    const replies = await this.facebookClient.instagram.getCommentReplies(accessToken, comment.id)
    return {
      id: `ig_${location.id}_${comment.id}`,
      externalId: comment.id,
      locationId: location.id,
      userId: location.userId,
      platform: 'instagram',
      created: comment.timestamp ? DateHelper.getDateFromTimestamp(comment.timestamp) : null,
      reviewer: comment.username ? { name: comment.username } : {},
      rating: null,
      text: comment.text ? comment.text : null,
      updated: comment.timestamp ? DateHelper.getDateFromTimestamp(comment.timestamp) : null,
      representationId: representation.id,
      url: media.permalink,
      post: {
        created: media.timestamp ? DateHelper.getDateFromTimestamp(media.timestamp) : null,
        id: media.id,
        image_url: media.media_type === 'VIDEO' ? media.thumbnail_url : media.media_url,
        text: media.caption ? media.caption : '',
      },
      comments: replies.map((reply) => {
        return {
          created_time: reply.timestamp ? DateHelper.getDateFromTimestamp(reply.timestamp) : null,
          from: {
            name: reply.username,
          },
          id: reply.id,
          message: reply.text,
        }
      }),
    }
  }

  async getCurrentReviewCount(representation, location, userId, accessToken) {
    // Won't be implemented, because Graph API doesn't offer endpoint to get count of account's comments
    return undefined
  }

  async getCurrentLatestReview(representation, location, userId, accessToken) {
    return undefined
  }

  protected shouldSkipPurging(representation): boolean {
    return false
  }
}
