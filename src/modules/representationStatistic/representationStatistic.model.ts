import { BaseModel } from '../database/database.baseModel'
import { Transform, Type } from 'class-transformer'

export class RepresentationStatistic extends BaseModel{
  locationId: string
  representationId: string
  @Type(() => Date)
  @Transform((value, obj) => obj.date.toDate(), { toClassOnly: true })
  date: Date
  totalReviews: number
  avgRating: number
  answered: number
  ratingOne: number
  ratingTwo: number
  ratingThree: number
  ratingFour: number
  ratingFive: number
  recommendationPositive: number
  recommendationNegative: number
}
