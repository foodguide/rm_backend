import { Module } from '@nestjs/common'
import { SyncController } from './sync.controller'
import { SyncFacebookService } from './facebook/sync.facebook.service'
import { RepresentationStatisticModule } from '../representationStatistic/representationStatistic.module'
import { LocationStatisticModule } from '../locationStatistic/locationStatistic.module'
import { ReviewModule } from '../review/review.module'
import { ApiModule } from '../api/api.module'
import { LocationModule } from '../location/location.module'
import { SyncStatisticService } from './statistic/sync.statistic.service'
import { SyncCrawlerService } from './crawler/sync.crawler.service'
import { SyncGoogleService } from './google/sync.google.service'
import { SyncBaseService } from './sync.base.service'
import { SyncInstagramService } from './instagram/sync.instagram.service'
import { UserModule } from '../user/user.module'
import { PaymentModule } from '../payment/payment.module'
import { DirectModule } from '../direct/direct.module'

// @ts-ignore
@Module({
  imports: [
    RepresentationStatisticModule,
    LocationStatisticModule,
    LocationModule,
    ReviewModule,
    ApiModule,
    UserModule,
    PaymentModule,
    DirectModule,
  ],
  controllers: [SyncController],
  providers: [
    SyncBaseService,
    SyncFacebookService,
    SyncStatisticService,
    SyncCrawlerService,
    SyncGoogleService,
    SyncInstagramService,
  ],
  exports: [SyncStatisticService],
})

export class SyncModule {
}
