import { Injectable } from '@nestjs/common'
import { MailjetApi } from '../../api/mailjet/api.mailjet'

@Injectable()
export class EmailService {
  constructor(
    private readonly mailjetApi: MailjetApi,
  ) {
  }

  async send(email) {
    const response = this.mailjetApi.send([email])
    for (const recipient of email.To) {
      console.log(`Sent "${email.Subject}"-Mail to ${recipient.Email}. Template id: ${email.TemplateId}`)
    }
    return response
  }
}
