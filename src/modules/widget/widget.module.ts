import { Module } from '@nestjs/common'
import { WidgetController } from './widget.controller'
import { WidgetRepository } from './widget.repository'
import { LocationStatisticModule } from '../locationStatistic/locationStatistic.module'

@Module({
  imports: [LocationStatisticModule],
  providers: [WidgetRepository],
  controllers: [WidgetController],
  exports: [],
})
export class WidgetModule {}
