import { Body, Controller, Get, Put, Query, UseGuards, ValidationPipe } from '@nestjs/common'
import { AuthGuard } from '../auth/auth.guard'
import { BearerToken } from '../auth/auth.bearerdecorator'
import {
  ExistingSubscriptionDto,
  HostedPageDto,
  NewSubscriptionDto,
  UpdateSubscriptionDto,
  ExistingCustomerDto, UpgradeSubscriptionDto,
} from './payment.model'
import { PaymentService } from './payment.service'
import { CurrentUserHelper } from '../helper/helper.currentUserHelper'

@Controller('payment')
export class PaymentController {
  constructor(private readonly service: PaymentService, private readonly currentUserHelper: CurrentUserHelper) {
  }

  @UseGuards(AuthGuard)
  @Get('new_subscription_url')
  async getNewSubscriptionUrl(@BearerToken() token: string, @Query(new ValidationPipe()) dto: NewSubscriptionDto) {
    const verificationResponse = await this.currentUserHelper.getUserVerificationResponse(token)
    const userId = verificationResponse.user_id
    const userEmail = verificationResponse.email
    return await this.service.getNewSubscriptionUrl(userId, dto.planId, userEmail)
  }

  @UseGuards(AuthGuard)
  @Get('existing_subscription_url')
  async getExistingSubscriptionUrl(@BearerToken() token: string,
                                   @Query(new ValidationPipe()) dto: ExistingSubscriptionDto) {
    return await this.service.getExistingSubscriptionUrl(dto.subscriptionId)
  }

  @UseGuards(AuthGuard)
  @Get('update_payment_url')
  async getUpdatePaymentUrl(@BearerToken() token: string) {
    const userId = await this.currentUserHelper.getUserIdFromVerification(token)
    return await this.service.getUpdatePaymentUrl(userId)
  }

  @UseGuards(AuthGuard)
  @Get('portal_session')
  async getPortalSession(@BearerToken() token: string) {
    const userId = await this.currentUserHelper.getUserIdFromVerification(token)
    return await this.service.getPortalSession(userId)
  }

  @UseGuards(AuthGuard)
  @Get('has_active_subscriptions')
  async hasActiveSubscriptions(@BearerToken() token: string) {
    const userId = await this.currentUserHelper.getUserIdFromVerification(token)
    const response = await this.service.hasValidSubscriptions(userId)
    return { has_active_subscriptions: response }
  }

  @UseGuards(AuthGuard)
  @Get('has_cancelled_subscriptions')
  async hasCancelledSubscriptions(@BearerToken() token: string) {
    const userId = await this.currentUserHelper.getUserIdFromVerification(token)
    return await this.service.hasCancelledSubscriptions(userId)
  }

  @UseGuards(AuthGuard)
  @Get('active_subscriptions')
  async getActiveSubscriptions(@BearerToken() token: string) {
    const userId = await this.currentUserHelper.getUserIdFromVerification(token)
    return await this.service.getValidSubscriptions(userId)
  }

  @UseGuards(AuthGuard)
  @Put('subscription')
  async updateSubscription(@BearerToken() token: string, @Body(new ValidationPipe()) dto: UpdateSubscriptionDto) {
    return await this.service.updateSubscription(dto.subscriptionId, dto.numberOfLocations)
  }

  @UseGuards(AuthGuard)
  @Put('subscription-upgrade')
  async upgradeToBasicPlan(@BearerToken() token: string, @Body(new ValidationPipe()) dto: UpgradeSubscriptionDto) {
    return await this.service.upgradeSubscriptionToBasicPlan(dto.subscriptionId)
  }

  @UseGuards(AuthGuard)
  @Get('hosted_page')
  async getHostedPage(@Query(new ValidationPipe()) dto: HostedPageDto) {
    return await this.service.getHostedPage(dto.hostedPageId)
  }

  @Put('cancel')
  async cancelSubscription(@Body(new ValidationPipe()) dto: ExistingCustomerDto) {
    return await this.service.cancelSubscriptions(dto.customerId)
  }
}
