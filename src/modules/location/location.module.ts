import { Module } from '@nestjs/common'
import { LocationRepository } from './location.repository'
import { Location } from './location.model'

@Module({
  providers: [Location, LocationRepository],
  exports: [LocationRepository],
})

export class LocationModule{}
