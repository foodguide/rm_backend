import { TransactionalMail } from '../transactional.mail'
import { MessageSubscription } from '../../../message.model'
import { MessageType } from '../../../message.type'
import { MessageName } from '../../../message.name'

export class NewsletterEmail extends TransactionalMail {
  constructor(templateId: string) {
    super()
    this.subscription = new MessageSubscription()
      .setType(MessageType.Email)
      .setName(MessageName.Basic)

    this.setSubject('Neues von Respondo')
      .setTemplate(Number.parseInt(templateId, 0))
  }
}
