import { Injectable } from '@nestjs/common'
import { Database } from './database.baseProvider'

@Injectable()
export class ProdDatabase extends Database {

  getServiceAccount() {
    return require('../../../prod_fg-admin-cert.json')
  }

}
