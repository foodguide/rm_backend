import { IsNotEmpty, IsString } from 'class-validator'

export class GoogleCodeRequestDto {
  @IsNotEmpty()
  @IsString()
  readonly code: string
}
