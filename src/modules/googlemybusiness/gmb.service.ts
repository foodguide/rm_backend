import { Injectable } from '@nestjs/common'
import { GoogleMyBusinessApi } from '../api/google/api.googlemybusiness'
import { GoogleAccessTokenProvider } from '../api/google/api.googleaccesstokenprovider.model'

@Injectable()
export class GmbService {
  constructor(private readonly _gmbApi: GoogleMyBusinessApi) {}
  get gmbApi() {
    return this._gmbApi
  }

  async sendReply(user, review, representation, text) {
    return this.gmbApi.sendReply(new GoogleAccessTokenProvider(user, representation.credentialId, null), review.externalURI, text)
  }
}
