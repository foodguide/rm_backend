import { BaseMessageQueueItem, MessageSubscription } from '../message.model'
import { EmailTemplate } from './transactional/email.template'
import { IsNotEmpty, IsString } from 'class-validator'

export abstract class Email extends BaseMessageQueueItem {

  subscription: MessageSubscription
  payload: EmailPayload = new EmailPayload()

  setSender(sender: EmailSender) {
    this.payload.From = sender
    return this
  }

  setRecipients(recipients: Array<EmailRecipient>) {
    this.payload.To = recipients
    return this
  }

  setBccRecipients(recipients: Array<EmailRecipient>) {
    this.payload.Bcc = recipients
    return this
  }

  setSubject(subject: string) {
    this.payload.Subject = subject
    return this
  }

  setTextPart(textPart: string) {
    if (!!this.payload.TemplateId || this.payload.TemplateLanguage)
      throw new InvalidEmailConfigurationError('Cannot use TextPart when using a template')
    this.payload.TextPart = textPart
    return this
  }

  setHtmlPart(htmlPart: string) {
    if (!!this.payload.TemplateId || this.payload.TemplateLanguage)
      throw new InvalidEmailConfigurationError('Cannot use HtmlPart when using a template')
    this.payload.HtmlPart = htmlPart
    return this
  }

  setTemplate(templateId: Object, locale: string) {
    if (!!this.payload.TextPart || !!this.payload.HtmlPart)
      throw new InvalidEmailConfigurationError('Cannot use TemplateId when Html- or TextPart is used')
    this.payload.TemplateId = templateId[locale] ? templateId[locale] : templateId['de']
    return this
  }

  useTemplateLanguage(useTemplateLanguage: boolean) {
    if (!!this.payload.TextPart || !!this.payload.HtmlPart)
      throw new InvalidEmailConfigurationError('Cannot use TemplateLanguage when Html- or TextPart is used')
    this.payload.TemplateLanguage = useTemplateLanguage
    this.payload.TemplateErrorDeliver = useTemplateLanguage
    this.payload.TemplateErrorReporting = useTemplateLanguage ? new EmailContact().setEmail('mattserudolph@gmail.com') : null
    return this
  }

  setTemplateVariables(templateVariables: object) {
    if (!!this.payload.TextPart || !!this.payload.HtmlPart)
      throw new InvalidEmailConfigurationError('Cannot use TemplateVariables when Html- or TextPart is used')
    this.payload.Variables = templateVariables
    return this
  }

  setTemplatErrorReporting() {
    this.payload.TemplateErrorReporting = {
      Email: 'matthias.rudolph@thefoodguide.com',
      Name: 'Matthias Rudolph',
    }
    this.payload.TemplateErrorDeliver = true

    return this
  }

  setAttachments(attachments: Array<EmailAttachment>) {
    this.payload.Attachments = attachments
    return this
  }
}


export class EmailAttachment {
  ContentType: string
  FileName: string
  Base64Content: string

  setContentType(contentType: string) {
    this.ContentType = contentType
    return this
  }

  setFileName(fileName: string) {
    this.FileName = fileName
    return this
  }

  setBase64Content(base64Content: string) {
    this.Base64Content = base64Content
    return this
  }

}

export class EmailPayload {
  From: EmailSender
  To: Array<EmailRecipient>
  Bcc: Array<EmailRecipient>
  Subject: string
  TextPart: string
  HtmlPart: string
  Attachments: Array<EmailAttachment>
  TemplateId: number
  TemplateLanguage: boolean
  TemplateErrorDeliver: boolean
  TemplateErrorReporting: EmailContact
  Variables: object = {}
  TemplateErrorReporting: object = {}
  TemplateErrorDeliver: boolean
}

export class EmailContact {
  Email: string
  Name: string

  setName(name: string) {
    this.Name = name
    return this
  }

  setEmail(email: string) {
    this.Email = email
    return this
  }
}

export class EmailSender extends EmailContact {
}

export class EmailRecipient extends EmailContact {
}

export class InvalidEmailConfigurationError extends Error {
  constructor(message?: string) {
    super()
    this.message = message
    this.name = 'InvalidEmailConfiguration'
  }
}

export class NewsletterDto {
  @IsNotEmpty()
  @IsString()
  readonly templateId: string
}
