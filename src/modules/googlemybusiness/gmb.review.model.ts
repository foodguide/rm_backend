import { Type } from 'class-transformer'

export class GmbReviewResponse {
  @Type(() => GmbReviewModel)
  reviews: GmbReviewModel[]
  averageRating: number
  totalReviewCount: number
  nextPageToken: string
}

export class GmbReviewerModel {
  displayName: string
  isAnonymous: boolean
  profilePhotoUrl: string
}

export class GmbReviewReply {
  comment: string
  updateTime: string
}

export class GmbReviewModel {
  name: string
  reviewId: string
  @Type(() => GmbReviewerModel)
  reviewer?: GmbReviewerModel
  starRating: string
  comment: string
  createTime: string
  updateTime: string
  @Type(() => GmbReviewReply)
  reviewReply?: GmbReviewReply
}
