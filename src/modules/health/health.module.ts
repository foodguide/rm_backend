import { Module } from '@nestjs/common'
import { UserModule } from '../user/user.module'
import { HealthController } from './health.controller'
import { ReviewModule } from '../review/review.module'
import { SyncModule } from '../sync/sync.module'
import { PaymentModule } from '../payment/payment.module'
import { QuickreplyModule } from '../quickreply/quickreply.module'
import { HealthService } from './health.service'

// @ts-ignore
@Module({
  providers: [HealthService],
  imports: [UserModule, ReviewModule, SyncModule, PaymentModule, QuickreplyModule],
  controllers: [HealthController],
})
export class HealthModule {}
