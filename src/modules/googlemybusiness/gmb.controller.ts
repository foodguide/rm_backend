import { Controller, Get, Put, Query, UseGuards, ValidationPipe } from '@nestjs/common'
import { AuthGuard } from '../auth/auth.guard'
import { BearerToken } from '../auth/auth.bearerdecorator'
import { CurrentUserHelper } from '../helper/helper.currentUserHelper'
import { GmbService } from './gmb.service'
import { GoogleAccessTokenProvider } from '../api/google/api.googleaccesstokenprovider.model'
import { Credential, GoogleRequestDto } from '../user/user.model'

@Controller('gmb')
export class GmbController {
  constructor(private currentUserHelper: CurrentUserHelper, private gmbService: GmbService) {
  }

  @Get('accounts')
  async getAccounts(@Query(new ValidationPipe()) dto: Credential) {
    return this.gmbService.gmbApi.getAccounts(new GoogleAccessTokenProvider(null, null, dto))
  }

  @Get('locations')
  async getLocations(@Query(new ValidationPipe()) dto: Credential) {
    return this.gmbService.gmbApi.getAllLocations(new GoogleAccessTokenProvider(null, null, dto))
  }

  /**
   * If the user has already stored credentials, the server should handle these credentials via userId and credentialId.
   * Otherwise the credentials are submitted directly.
   *
   * Therefore we need two different endpoints for each request.
   */

  @UseGuards(AuthGuard)
  @Get('accounts-for-user')
  async getAccountsForUser(@BearerToken() token: string, @Query(new ValidationPipe()) dto: GoogleRequestDto) {
    const user = await this.currentUserHelper.getCurrentUser(token)
    return this.gmbService.gmbApi.getAccounts(new GoogleAccessTokenProvider(user, dto.credentialId, null))
  }

  @UseGuards(AuthGuard)
  @Get('locations-for-user')
  async getLocationsForUser(@BearerToken() token: string, @Query(new ValidationPipe()) dto: GoogleRequestDto) {
    const user = await this.currentUserHelper.getCurrentUser(token)
    return this.gmbService.gmbApi.getAllLocations(new GoogleAccessTokenProvider(user, dto.credentialId, null))
  }
}
