import { Controller, Get } from '@nestjs/common'
import { EventRepository } from './event.repository'

@Controller('event')
export class EventController {

  constructor(
    private readonly eventRepository: EventRepository,
  ) {}

  @Get('delete')
  async deleteAll() {
    return this.eventRepository.deleteCollection()
  }

}
