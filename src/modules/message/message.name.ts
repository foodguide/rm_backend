export enum MessageName {
  Summary = 'summary',
  Basic = 'basic',
  Reminder = 'reminder',
  Newsletter = 'newsletter',
}
