import { Controller } from '@nestjs/common'
import { DirectService } from './direct.service'

@Controller('direct')
export class DirectController {

  constructor(
    private readonly directService: DirectService,
  ) {
  }

}
