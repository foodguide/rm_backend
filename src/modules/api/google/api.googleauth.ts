import { OAuth2Client } from 'google-auth-library'
import { ConfigService } from '../../../config/config.service'
import { Injectable } from '@nestjs/common'
import { UserRepository } from '../../user/user.repository'
import { DateHelper } from '../../helper/helper.dateHelper'
import { Credential, User } from '../../user/user.model'
import { classToPlain, plainToClass } from 'class-transformer'
import { GoogleAccessTokenProvider } from './api.googleaccesstokenprovider.model'

@Injectable()
export class GoogleAuthApi {
  private _client: OAuth2Client

  constructor(config: ConfigService, readonly userRepo: UserRepository) {
    this._client = new OAuth2Client({
      clientId: config.get('GOOGLE_CLIENT_ID'),
      clientSecret: config.get('GOOGLE_CLIENT_SECRET'),
      redirectUri: 'postmessage',
      authBaseUrl: null,
      tokenUrl: null,
    })
  }

  async retrieveAccessToken(code: string) {
    return await this._client.getToken(code).then(response => response.tokens)
  }

  private static getAccessToken(tokenData) {
    return tokenData.accessToken ? tokenData.accessToken : tokenData.access_token
  }

  async getValidAccessTokenForUser(user: User, credentialId: string) {
    const credential = user.credentials.find(userCredential => userCredential.id === credentialId)
    return await this.getValidAccessTokenForAccessToken(credential, () => {
      // return this.updateCredentialForUser(user, newAccessToken)
      return Promise.resolve()
    })
  }

  async getValidAccessToken(accessTokenProvider: GoogleAccessTokenProvider): Promise<string> {
    if (!!accessTokenProvider.credential) {
      let credential = accessTokenProvider.credential
      if (typeof credential === 'string')
        credential = await plainToClass<Credential, object>(Credential, JSON.parse(credential))
      return await this.getValidAccessTokenForAccessToken(credential, () => {
        // We don't store the new credential because no corresponding user is given
        return Promise.resolve()
      })
    } else if (!!accessTokenProvider.user) {
      return this.getValidAccessTokenForUser(accessTokenProvider.user, accessTokenProvider.credentialId)
    } else {
      throw Error('No valid user or credentials given for Google request')
    }
  }

  private async getValidAccessTokenForAccessToken(credential: Credential, update: (newCredential: Credential) => Promise<any>): Promise<string> {
    if (!credential || !credential.expiry_date)
      return null
    const expiryDate =
      (typeof credential.expiry_date === 'number') ? credential.expiry_date : parseInt(credential.expiry_date, 10)
    if (!DateHelper.isExpired(new Date(expiryDate))) {
      return GoogleAuthApi.getAccessToken(credential)
    }
    const newCredential = await this.renewCredential(credential.refresh_token)
    const newCredentialObject = await plainToClass<Credential, object>(Credential, JSON.parse(JSON.stringify(newCredential.credentials)))
    newCredentialObject.id = credential.id
    await update(newCredentialObject)
    return GoogleAuthApi.getAccessToken(newCredentialObject)
  }

  private async updateCredentialForUser(user: User, newCredential) {
    const index = user.credentials.findIndex(token => token.id === newCredential.id)
    const oldCredential = user.credentials[index]
    newCredential.account = oldCredential.account
    newCredential.platform = oldCredential.platform
    user.credentials[index] = newCredential
    const message = `Updating credential ${newCredential.id} for user ${user.id}. Old values: ${JSON.stringify(oldCredential)}. New Values: ${JSON.stringify(newCredential)}`
    console.log(message)
    return await this.userRepo.update(user.id, classToPlain(user))
  }

  private async renewCredential(refreshToken: string) {
    this._client.setCredentials({
      refresh_token: refreshToken,
    })
    return await this._client.refreshAccessToken()
  }

}
