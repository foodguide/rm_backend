import { IsNotEmpty, IsString } from 'class-validator'

export class ReviewReplyDto {
  @IsNotEmpty()
  @IsString()
  readonly reviewId: string
  @IsNotEmpty()
  @IsString()
  readonly text: string
}

export class DirectReplyDto extends ReviewReplyDto {
  @IsNotEmpty()
  @IsString()
  readonly userId: string
}
