import { Facebook } from 'fb'
import { ConfigService } from '../../../config/config.service'
import { Injectable, Logger } from '@nestjs/common'
import { URL } from 'url'
import { SlackApi } from '../slack/api.slack'
import { ExternalApiError } from '../errors'
import { plainToClass } from 'class-transformer'
import {
  InstagramComment,
  InstagramCommentReply,
  InstagramMediaObject,
} from './api.facebook.instagram.model'

@Injectable()
export class FacebookApi {

  private _client
  private _accessToken: string

  constructor(
    private readonly config: ConfigService,
    private readonly slackApi: SlackApi,
  ) {
    this._client = new Facebook({
      appId: config.get('FACEBOOK_APP_ID'),
      autoLogAppEvents: true,
      xfbml: true,
      version: 'v4.0',
    })
  }

  set accessToken(accessToken: string) {
    this._accessToken = accessToken
    this._client.setAccessToken(accessToken)
  }

  get accessToken(): string {
    return this._accessToken
  }

  get client() {
    return this._client
  }

  /**
   * Returns long-living access token (60 days).
   * https://developers.facebook.com/docs/facebook-login/access-tokens/refreshing
   * @param accessToken - Short-living access token
   */
  getAccessToken(accessToken: string) {
    return this.send('oauth/access_token?grant_type=fb_exchange_token'
      + '&client_id=' + this.config.get('FACEBOOK_APP_ID')
      + '&client_secret=' + this.config.get('FACEBOOK_APP_SECRET')
      + '&fb_exchange_token=' +  accessToken,
                     {}).then((response: any) => {
                       console.log(`long-living-accesstoken-response: ${JSON.stringify(response)}`)
                       return response
                     })
  }

  getAccountInformation(accessToken: string) {
    return this.send('/me', { access_token: accessToken })
  }

  getReviews(accessToken, pageId, after?, reviewList?) {
    if (!reviewList)
      reviewList = []

    const url = '/' + pageId + '/ratings'
    const options: any = {
      access_token: accessToken,
      fields: 'open_graph_story, reviewer, created_time',
    }
    if (after !== undefined)
      options.after = after

    const promise = this.send(url, options)
    return promise.then((response: any) => {
      const hasData = !!response && response.data.length > 0
      const hasNext = !!response && !!response.paging && response.paging.next !== undefined

      if (hasData)
        reviewList.push(...response.data)

      if (hasData && hasNext && new URL(response.paging.next).searchParams.get('after') !== undefined)
        return this.getReviews(accessToken, pageId, new URL(response.paging.next).searchParams.get('after'), reviewList)
      return reviewList
    })
  }

  getReviewCount(accessToken, pageId) {
    const url = '/' + pageId
    const options: any = {
      access_token: accessToken,
      fields: 'rating_count',
    }

    const promise = this.send(url, options)
    return promise.then((response: any) => {
      return response.rating_count
    })
  }

  getLatestReview(accessToken, pageId) {
    const url = '/' + pageId + '/ratings'
    const options: any = {
      access_token: accessToken,
      fields: 'open_graph_story',
      limit: 1,
    }
    const promise = this.send(url, options)
    return promise.then((response: any) => {
      const review = response.data[0]
      // We need property 'reviewId' for easy comparison in sync.base.service
      review.reviewId = review.open_graph_story.id
      return review
    })
  }

  getComments(accessToken, reviewId, representationId, userId) {
    return this.send('/' + reviewId + '/comments', {
      access_token: accessToken,
    },               'GET', { representationId, userId })
  }

  getSubcomments(accessToken, commentId, representationId, userId) {
    return this.send('/' + commentId + '/comments', {
      access_token: accessToken,
    },               'GET', { representationId, userId })
  }

  get instagram() {
    return {
      getMediaObjects: async (accessToken: string, pageId: string): Promise<InstagramMediaObject[]> => {
        const url = `/${pageId}/media`
        const options = {
          access_token: accessToken,
          fields: 'caption,media_url,thumbnail_url,media_type,permalink,timestamp',
        }
        const response = await this.send(url, options)
        return plainToClass(InstagramMediaObject, response.data)
      },
      getMediaComments: async (accessToken, mediaId): Promise<InstagramComment[]> => {
        const url = `/${mediaId}/comments`
        const options = {
          access_token: accessToken,
          fields: 'username,timestamp,text',
        }
        const response = await this.send(url, options)
        return plainToClass(InstagramComment, response.data)
      },
      getCommentReplies: async (accessToken, commentId): Promise<InstagramCommentReply[]> => {
        const url = `/${commentId}/replies`
        const options = {
          access_token: accessToken,
          fields: 'username,text,timestamp',
        }
        const response = await this.send(url, options)
        return plainToClass(InstagramCommentReply, response.data)
      },
    }
  }

  async send(url, options = {}, method = 'GET', data = null) {
    try {
      return await this.client.api(url, method, options)
    } catch (e) {
      Logger.error(e.message, e.stack)
      let message = e.response.error.message + '\n' + url
      if (!!data) {
        message = message + '\n' + data.representationId
      }

      const error = new ExternalApiError(
        !!data ? data.userId : '', message, e.response.error.code, JSON.stringify(e.stack))
      this.slackApi.logErrors(
        [this.slackApi.createErrorAttachement(error)],
      )
      return await Promise.reject(e)
    }
  }
}
