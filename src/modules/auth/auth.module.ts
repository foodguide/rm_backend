import { Module } from '@nestjs/common'
import { AuthController } from './auth.controller'
import { AuthService } from './auth.service'
import { ApiModule } from '../api/api.module'
import { DatabaseModule } from '../database/database.module'
import { AuthGuard } from './auth.guard'

@Module({
  imports: [ApiModule, DatabaseModule],
  controllers: [AuthController],
  providers: [AuthService, AuthGuard],
})

export class AuthModule {}
