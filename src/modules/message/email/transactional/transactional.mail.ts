import { Email, EmailSender } from '../email.model'

export class TransactionalMail extends Email {
  constructor() {
    super()
    this.setSender(new EmailSender()
            .setName('Respondo')
            .setEmail('info@respondo.app'))
            .useTemplateLanguage(true)
  }

  protected setTemplateVariable(key: string, value: any) {
    this.payload.Variables[key] = value
    return this
  }

  public getTemplateVariable(key: string) {
    return this.payload.Variables[key]
  }
}

export interface Unsubscribable {
  setUnsubscribeUrlByUserId(userId: string, baseUrl: string)
}

export class UnsubscribableTransactionalMail extends TransactionalMail implements Unsubscribable {
  setUnsubscribeUrlByUserId(userId: string, baseUrl: string) {
    this.setTemplateVariable('unsubscribe_url', `${baseUrl}/#/unsubscribe/${userId}`)
    return this
  }
}
