import { Injectable } from '@nestjs/common'
import { Database } from '../database/database.baseProvider'
import moment from 'moment'
import { DirectTask } from './direct.model'
import { DirectTaskRepository } from './directTask.repository'
import { DirectUserRepository } from './directUser.repository'
import { DateHelper } from '../helper/helper.dateHelper'
import { SlackApi } from '../api/slack/api.slack'
import { LocationRepository } from '../location/location.repository'
import { CurrentUserHelper } from '../helper/helper.currentUserHelper'

@Injectable()
export class DirectService {

  constructor(
    protected database: Database,
    private readonly directUserRepository: DirectUserRepository,
    private readonly directTaskRepository: DirectTaskRepository,
    private readonly locationRespository: LocationRepository,
    private readonly userHelper: CurrentUserHelper,
    private readonly slackApi: SlackApi,
  ) {
    this.initReviewListener()
  }

  async initReviewListener() {
    const directUsers = await this.directUserRepository.findAll()
    directUsers.forEach(({ userId }) => {
      this.database.firestore.collection('review').where('userId', '==', userId)
        .onSnapshot((querySnapshot) => {
          querySnapshot.docChanges().forEach((change) => {
            if (change.type === 'added') {
              this.handleAddedReview(change.doc.data())
            }
          })
        })
    })
  }

  async handleAddedReview(review) {
    const isReviewAddedInLastHour = moment().diff(moment(DateHelper.getDateFromTimestamp(review.created)), 'minutes') < 65
    if (!isReviewAddedInLastHour) return

    const taskForReviewId = await this.directTaskRepository.findByReviewId(review.id)
    const existsTaskForReview = taskForReviewId && taskForReviewId.length > 0
    if (existsTaskForReview) return

    await this.createNewTask(review)
  }

  async createNewTask(data) {
    const location = await this.locationRespository.findDoc(data.locationId)
    const user = await this.userHelper.getUser(data.userId)

    const newTask = new DirectTask()
    newTask.id = data.id
    newTask.reviewId = data.id
    newTask.userId = data.userId
    newTask.locationName = location.name
    newTask.userName = user.email

    return this.directTaskRepository.create(newTask).then(() => {
      console.log(`New direct task for review: ${newTask.reviewId}`)
      return this.sendSlackNotification(data, newTask)
    })
  }

  async sendSlackNotification(data, task) {
    const slackData = {
      text: data.text,
      platform: data.platform,
      date: DateHelper.getDateFromTimestamp(data.created),
      location: task.locationName,
      user: task.user,
    }
    return this.slackApi.sendMessageToChannel(
      'respondo-direct',
      `New review`,
      [this.slackApi.createAttachement('Respondo Direct', slackData)])
  }

}
