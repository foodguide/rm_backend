import { Injectable } from '@nestjs/common'
import { BaseRepository } from '../database/database.baseRepository'

@Injectable()
export class EventRepository extends BaseRepository {
  managedCollection = 'event'

  async findLastForUser(userId: string) {
    return this.commitRead(this.collection.where('userId', '==', userId).orderBy('timestamp', 'desc').limit(1).get())
  }

  async findLastAmountForUser(userId: string, amount: number) {
    return this.commitRead(this.collection.where('userId', '==', userId).orderBy('timestamp', 'desc').limit(amount).get())
  }

  async findSinceTimestamp(timestamp: number) {
    return this.commitRead(this.collection.where('timestamp', '>=', timestamp).orderBy('timestamp', 'asc').get())
  }

  async deleteAll() {
    return super.deleteAll()
  }
}
