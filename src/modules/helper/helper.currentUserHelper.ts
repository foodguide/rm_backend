import { Inject, Injectable } from '@nestjs/common'
import { Database } from '../database/database.baseProvider'
import { UserRepository } from '../user/user.repository'
import { User } from '../user/user.model'
import { plainToClass } from 'class-transformer'
import { ConfigService } from '../../config/config.service'

@Injectable()
export class CurrentUserHelper {
  constructor(@Inject('Database') private readonly database: Database,
              private readonly userRepo: UserRepository,
              private readonly configService: ConfigService) {}

  public async getUserVerificationResponse(token: string) {
    return  await this.database.auth.verifyIdToken(token)
  }

  public async getUserIdFromVerification(token: string) {
    const verificationResponse = await this.database.auth.verifyIdToken(token)
    return verificationResponse.user_id
  }

  public async getEmailVerificationLink(email: string, userId: string) {
    const actionCodeSettings = {
      url: `${this.configService.get('BASE_URL')}/#/verified/${userId}?newUser=true`,
    }
    return await this.database.auth.generateEmailVerificationLink(email, actionCodeSettings)
  }

  public async getPipedriveBcc(userId: string) {
    const dbUser = await this.userRepo.findByUserId(userId)
    return this.createPipedriveBcc(dbUser[0].pipedriveId)
  }

  public createPipedriveBcc(pipedriveId) {
    return !!pipedriveId ? `foodguide+deal${pipedriveId}@pipedrivemail.com` : null
  }

  public async getFirebaseUser(userId: string) {
    return await this.database.auth.getUser(userId)
  }

  /**
   * Get user from auth and append DB data.
   */
  public async getUser(userId) {
    const user = await this.getFirebaseUser(userId)
    const dbUser = await this.userRepo.findByUserId(userId)
    return {
      ...user,
      ...dbUser[0],
    }
  }

  public async getCurrentUser(token: string | null): Promise<User> {
    const userId = await this.getUserIdFromVerification(token)
    try {
      const userJson: object = await this.userRepo.findById(userId)
      if (!userJson) {
        return {
          userId,
          id: userId,
        }
      }
      return await plainToClass(User, userJson)
    } catch (e) {
      return null
    }
  }
}
