import { Injectable } from '@nestjs/common'
import { BaseRepository } from '../database/database.baseRepository'

@Injectable()
export class LocationStatisticRepository extends BaseRepository {

  managedCollection = 'locationStatistics'

  findByLocationId(locationId: string) {
    return this.commitRead(
      this.collection.where('locationId', '==', locationId).limit(1).get(),
    )
  }

}
