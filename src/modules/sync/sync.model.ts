import { IsArray, IsNotEmpty, IsOptional, IsString } from 'class-validator'

export class SyncPlatformDto {
  @IsNotEmpty()
  @IsString()
  platform
}

export class PlatformsDto {
  @IsArray()
  @IsNotEmpty()
  platforms: string[]
}

export class SyncLocationDto {
  @IsString()
  @IsNotEmpty()
  locationId: string
  @IsArray()
  @IsNotEmpty()
  platforms: string[]
}

export class SyncLocationCrawlerDto {
  @IsString()
  @IsNotEmpty()
  locationId: string
}

export class SyncRepresentationCrawlerDto {
  @IsString()
  @IsNotEmpty()
  representationId: string
  locationId: string
}

export class SyncAdminRepresentationCrawlerDto {
  @IsString()
  @IsNotEmpty()
  representationId: string
  @IsString()
  @IsNotEmpty()
  locationId: string
  @IsString()
  @IsNotEmpty()
  userId: string
}

export class PlatformNotSupportedException extends Error {
  constructor(platform: string) {
    super()
    this.message = 'Platform ' + platform + ' is not supported.'
    this.name = 'PlatformNotSupported'
    this.stack = (<any> new Error()).stack
  }
}
