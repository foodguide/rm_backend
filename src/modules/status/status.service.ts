import { Inject, Injectable } from '@nestjs/common'
import { StatusApiInterface } from './status.api.interface'

@Injectable()
export class StatusService {
  constructor(@Inject('StatusApiInterface') private statusApi: StatusApiInterface) {
  }

  getStatus() {
    return this.statusApi.getStatus()
  }

}
